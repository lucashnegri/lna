module(..., package.seeall)

local lnacomplex = require('lna.complex')
local lcomplex   = require('complex')     -- reference implementation

local ffi = require('ffi')

function setup()
    tol = 1e-9
end

function test_basic()
    local r1 = (((5-9i)*10-20)/(4-2i))^1.2
    local r2 = ((lcomplex.new(5,-9)*10-20)/lcomplex.new(4,-2))^1.2
    
    assert_equal(r1:real(), r2:real(), tol)
    assert_equal(r1:imag(), r2:imag(), tol)
end

function test_functions()
    local funcs = {"sqrt", "abs", "log", "exp", "cos", "sin", "acos", "asin",
        "cosh", "acosh", "sinh", "asinh", "tan", "atan", "tanh", "atanh", "arg",
        "conj"}
    
    for _, fname in ipairs(funcs) do
        local r, i = (math.random()-0.5)*5, (math.random()-0.5)*5
        local c1 = lnacomplex.new(r, i)
        local c2 = lcomplex.new(r, i)
        
        local r1 = c1[fname](c1)
        local r2 = c2[fname](c2)
        
        if type(r1) == 'number' then
            assert_equal(r1, r2, tol)
        else
            assert_equal(r1:real(), r2:real(), tol)
            assert_equal(r1:imag(), r2:imag(), tol)
        end
    end
    
    assert_true((13-6i):log10():equal(1.15587693052787-0.187792310862i, tol))
end

function test_is()
    local c1 = ffi.new("complex", 5, 6)
    local c2 = lnacomplex.new(5, 6)
    local c3 = ffi.new("complex[5]")
    c3[0] = lnacomplex.new(5,6)
    
    assert_true(c1:equal(c2))
    assert_true(c2:equal(c1))
    assert_true(c1:equal(c3[0]))
    assert_true(c2:equal(c3[0]))
    assert_true(lnacomplex.is(c1))
    assert_true(lnacomplex.is(c2))
    assert_true(lnacomplex.is(c3[0]))
end

function test_lt()
    assert_true(5+2i < 4+10i)
    assert_true(3+0i < (2-10i) )
    assert_true(10-2i > 4+0i)
    assert_true(10-2i < 40+0i)
end
