--- Helpers for quick experimentation on the REPL or quick programs.
--
-- Sometimes, importing each module and writting a proper gnuplot plot
-- description takes more time than the task to be performed. This helper
-- module imports all other modules to the global table and provides a quick
-- function to plot very simple 2D curves.

matrix  = require('lna.matrix' )
fft     = require('lna.fft'    )
stats   = require('lna.stats'  )
numeric = require('lna.numeric')
complex = require('lna.complex')
gp      = require('gnuplot'    )

---
-- Shows a simple two-dimensional plot, where _x_ and _y_ are Lua tables or
-- row/columns vectors.
-- Returns the generated gnuplot object. Labels can be specified by _xlabel_ and _ylabel_,
-- and a title can be set with _title). _term_ can be used to force a specific
-- terminal (wxt, qt).
--
-- @example plot( matrix.linspace(1, 100, 10) )
-- @example plot(x, y)
function plot(x, y, xlabel, ylabel, title, path)
    path = path or 'tmp.qt'
    if not path:match("%.") then path = "tmp." .. path end
    
    if matrix.is(x) then x = x:totable() end
    if matrix.is(y) then y = y:totable() end
    
    -- fix gnuplot weird font sizes
    local mult = 1
    if path:match("%.svg$") then mult = 1.4 end
    if path:match("%.pdf$") then mult = 1.3 end
    
    local p = gp{
        grid   = 'back',
        xlabel = xlabel,
        ylabel = ylabel,
        title  = title,
        width  = "600",
        height = "400",
        fsize  = 12 * mult,
        
        data = {
            gp.array{
                {x, y},
                with  = #x > 20 and 'lines' or 'linespoints',
                width = 2,
            }
        }
    }
    p:plot(path)
    return p
end
