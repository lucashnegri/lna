--[[
    2D visualizations of the wine[1] dataset.
    [1] http://archive.ics.uci.edu/ml/machine-learning-databases/wine
--]]

-- import libraries
local matrix = require('lna.matrix')
local stats  = require('lna.stats' )
local gp     = require('gnuplot'   )

-- load the dataset
local data  = matrix.load('wine.data', ',')
local y     = data:col(1)
local X     = stats.zscorem( data:view(1, 2, data.m, data.n-1) )

-- project with both PCA and LDA
for _, algo in ipairs{"pca", "lda"} do
    local w, V, mu
    if algo == "pca" then
        w, V, mu = stats.pca(X)
    else
        w, V, mu = stats.lda(X, y)
    end
    
    local Xproj = stats.project(X, V:view(1, 1, V.m, 2), mu)
    
    local c1 = Xproj:filter(function(_,i) return y:get(i) == 1 end)
    local c2 = Xproj:filter(function(_,i) return y:get(i) == 2 end)
    local c3 = Xproj:filter(function(_,i) return y:get(i) == 3 end)
    
    gp{
        title = "Projection using " .. algo,
        width = 600   , height = 500,
        grid  = "back", title = algo .. ' projection',
        data  = {
            gp.custom{c1, with = 'points', title = 'Class 1' },
            gp.custom{c2, with = 'points', title = 'Class 2' },
            gp.custom{c3, with = 'points', title = 'Class 3' },
        }
    }:plot('wine_' .. algo .. '.qt')
end
