module(..., package.seeall)

local matrix  = require('lna.matrix')
local complex = require('lna.complex')
local fft     = require('lna.fft')

function setup()
    tol = 1e-9
end

function test_fft_real()
    for t = 1, 30 do
        local s1, s2 = 1, 1
        if math.random() < 0.5 then
            s1 = math.random(1, 2048)
        else
            s2 = math.random(1, 2048)
        end
        
        local a = matrix.new(s1, s2)
        a:fill(math.random)
        local A  = fft.fft(a)
        local a2 = fft.ifft(A):real()
        
        assert_true(a:equal(a2, tol))
    end
end

function test_fft_imag()
    for t = 1, 30 do
        local s1, s2 = 1, 1
        if math.random() < 0.5 then
            s1 = math.random(1, 2048)
        else
            s2 = math.random(1, 2048)
        end
        
        local a = matrix.new(s1, s2, true)
        
        a:fill(function() return complex.new(math.random(), math.random()) end)
        local A  = fft.fft(a)
        local a2 = fft.ifft(A)
        assert_true(a:equal(a2, tol))
    end
end

function test_shift()
    local x1 = matrix.fromtable{1,2,3,4,5}
    local x2 = matrix.fromtable{1,2,3,4}
    local x3 = matrix.fromtable{1}

    for i = 1, 100 do
        assert_true( fft.shift(x1):equal(matrix.fromtable{4,5,1,2,3}) )
        assert_true( fft.shift(x2):equal(matrix.fromtable{3,4,1,2}) )
        assert_true( fft.shift(x3):equal(matrix.fromtable{1}) )
    end
    
    local t = matrix.rand(20,1)
    local T = fft.shift( fft.fft(t) )
    assert_equal(T.m, t.m)
    assert_equal(T.n, t.n)
    assert_true(matrix.iscomplex(T))
end

function test_unwrap()
    local A = matrix.linspace(0, 100, 3)
    local B = fft.unwrap(A)
    assert_equal(A.m, B.m)
    assert_equal(A.n, B.n)
end
