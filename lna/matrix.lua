--- Matrices and linear algebra module.
--
-- Implementation of real and complex matrices. Some operations are implemented in Lua, while
-- linear algebra functions that must be fast and precise, like svd(), eig(), solve(),
-- are handled by LAPACK via FFI.
--
-- There is no special wrapper for column vectors or row vectors (a column vector is just a matrix
-- with one column), everything is a matrix. Also, due to speed and to avoid mistakes, one can't
-- index a matrix by using A[i][j]. The recommended way is to use A:get(i,j) and A:set(i,j).
-- Use A.m and A.n to access the number of rows and cols.
--
-- All memory are stored contiguously. To access a given 'flat' index (memory position, 1-indexed),
-- one can use the A(pos) operator. For column/row vectors, one can read elements from the
-- vector with the A(pos) notation.
--
-- Due to LuaJIT, almost all operations implemented in Lua are almost at the C performance level.
-- Iterating element-per-element using A:get(i,j) is not slow at all.
--
-- @module lna.matrix

-- ** libraries and declarations **
local complex = require('lna.complex')
local sort    = require('lna.sort')
local ffi     = require('ffi')

-- load blas and lapack interfaces
ffi.cdef( require('lna.headers') )
local cblas       = ffi.load('cblas')
local ok, lapacke = pcall(ffi.load, 'lapacke')

if not ok then
    ok, lapacke = pcall(ffi.load, 'openblas')
end

local matrix   = {}
matrix.__index = matrix

---
-- Returns the content of memory position _i_. For non-view vectors (matrices with 1 column or 1
-- Row) it can be used as a faster way to _get_ the _i_-th element; note that _V.n_ must be equal
-- to _V.ld_, which is true for all matrices that are not views.
function matrix.__call(V,i)
    assert(V.n == V.ld, "V(idx) should not be used when V.n ~= V.ld")
    return V.data[i-1]
end

local RowMajor, ColMajor = 101, 102

-- ** implementation **

---
-- Returns the memory position of index (i,j) of _A_.
function matrix.idx(A, i, j)
    return (i-1)*A.ld + (j-1)
end
local idx = matrix.idx

---
-- Returns the value at index (i,j) of matrix _A_ (_A[i][j]_).
function matrix.get(A, i, j)
    return A.data[idx(A,i, j or 1)]
end
local get = matrix.get

---
-- Assigns _v_ to matrix _A_ at index (i,j) (_A[i][j] = v_).
function matrix.set(A, i, j, v)
    A.data[idx(A,i, j or 1)] = v
end
local set = matrix.set

---
-- Returns a new _m_ x _n_ matrix of real numbers (or complex numbers if _iscomplex_ is true).
-- If ommited, _n_ will be equal to 1, i.e., a column vector will be allocated.
function matrix.new(m, n, iscomplex)
    if not n then n = 1 end
    assert(m and n and m > 0 and n > 0, "invalid parameters")

    local self = {}
    self.m     = math.floor(m)
    self.n     = math.floor(n)
    self.ld    = self.n
    local size = self.m*self.n
    
    if iscomplex then
        self.data        = ffi.new("complex[?]", size)
        self.type        = 'complex'
    else
        self.data        = ffi.new("double[?]", size)
        self.type        = 'double'
    end
    
    setmetatable(self, matrix)
    
    return self
end

---
-- Returns true if _A_ is a matrix, false otherwise.
function matrix.is(A)
    return getmetatable(A) == matrix
end

---
-- Returns true is _A_ is a complex matrix or a complex number, false otherwise.
function matrix.iscomplex(A)
    return matrix.is(A) and A.type == 'complex' or complex.is(A)
end

---
-- Returns an _m_ x _n_ identity matrix (if _iscomplex_ is true, then it will be a complex matrix).
function matrix.eye(m, n, iscomplex)
    n = n or m
    local a = matrix.new(m, n, iscomplex)
    a:fill(0)
    for i = 1, math.min(m,n) do a:set(i,i, 1) end
    return a
end

---
-- Returns an _m_ x _n_ real matrix with pseudo-random values between [0,1].
-- If _iscomplex_ is true, then a complex matrix will be returned, with the
-- real values and the imaginary values between [0,1].
function matrix.rand(m, n, iscomplex)
    if iscomplex then
        return matrix.rand(m,n) + matrix.rand(m,n)*complex.i
    else
        return matrix.new(m,n):fill(math.random)
    end
end

---
-- Returns an _m_ x _n_ real matrix filled with zeros.
-- If _iscomplex_ is true, then a complex matrix with zeros will be returned.
function matrix.zeros(m, n, iscomplex)
    return matrix.new(m, n, iscomplex):fill(0)
end

---
-- Returns an _m_ x _n_ real matrix filled with ones
-- If _iscomplex_ is true, then a complex matrix with 1+0i will be returned.
function matrix.ones(m, n, iscomplex)
    return matrix.new(m, n, iscomplex):fill(1)
end

---
-- Returns true if every element of _A_ is within a tolerance of _tol_ (default 1e-12) from the
-- corresponding element in _B_, false otherwise.
function matrix.equal(A,B,tol)
    if(A.m ~= B.m or A.n ~= B.n or A.type ~= B.type) then return false end;
    
    local tol = tol or 1e-12
    
    for i = 1, A.m do
        for j = 1, A.n do
            local dif = get(A,i,j) - get(B,i,j)
            
            if A:iscomplex() then
                local r, i = dif:both()
                if not (math.abs(r) <= tol)   then return false end
                if not (math.abs(i) <= tol)   then return false end
            else
                if not (math.abs(dif) <= tol) then return false end
            end
        end
    end
    
    return true
end

matrix.__eq = matrix.equal

---
-- Returns a new matrix from the contents of the Lua table _t_. The returned matrix will be
-- complex only if the first element of _t_ is a complex number or if _iscomplex_ is true.
-- @usage m = matrix.fromtable{ {1,2}, {3,4} }  -- 2x2 matrix
function matrix.fromtable(t, iscomplex)
    local m   = #t
    assert(m > 0, "invalid parameters")
    local vec = type(t[1]) == 'number' or complex.is(t[1])
    local n   = vec and 1 or #t[1]
    
    local cpx = iscomplex or matrix.iscomplex(vec and t[1] or t[1][1])
    local A   = matrix.new(m, n, cpx)
    
    if vec then
        return A:foreach(function(A,i) set(A,i,1, t[i]) end)
    else
        return A:foreach(function(A,i,j) set(A,i,j, t[i][j]) end)
    end
end

---
-- Returns a Lua table that corresponds to _A_. If _A_ is a matrix (_A.m_ and _A.n_ are both greater than 1),
-- then a '2D Lua array' will be returned. If _A_ is a column vector, a single table is returned. If _A_ is a
-- row vector, a table containing a single table is returned. This way, _matrix.totable_ and _matrix.fromtable_
-- can be used together.
-- @see fromtable
function matrix.totable(A)
    local t = {}
    
    if A.m > 1 and A.n > 1 then
        for i = 1, A.m do t[i] = {} end
        A:foreach(function(A,i,j) t[i][j] = get(A,i,j) end)
    else
        A:foreach(function(A,i,j) table.insert(t, get(A,i,j)) end)
        if A.m == 1 and A.n > 1 then t = {t} end
    end
    
    return t
end

---
-- Returns a reshaped (now a _m2_ x _n2_ matrix) view of the matrix _A_. If A is already a view,
-- or if _force_ is true, then a new matrix (not a view) will be returned.
function matrix.reshape(A, m2, n2, force)
    assert(A:len() == m2*n2, "dimensions do not match")
    
    if A.isview or force then
        -- copy the matrix contents
        local B = matrix.new(m2, n2, A:iscomplex())
        local p = 0
        for i = 1, A.m do
            for j = 1, A.n do
                B.data[p] = get(A,i, j)
                p = p + 1
            end
        end
        
        return B
    else
        -- Return a view with new dimensions
        return A:view(1, 1, m2, n2)
    end
end

---
-- Returns a new view of matrix _A_. The new initial element will be at index (_i_,_j_),
-- with _m_ rows and _n_ columns; A view can be used as a normal matrix, and it is not read-only.
-- Notice that the returned view stores a reference to the original matrix, as the memory is shared.
-- Some functions can not operate on _views_ and will issue a copy() before operating.
-- @see copy
function matrix.view(A, i, j, m, n)
    assert(i+m-1 <= A.m and j+n-1 <= A.n and i >= 1 and j >= 1, "dimensions do not match")

    local self     = {}
    self.m         = m
    self.n         = n
    self.orig      = A
    self.ld        = A.ld
    self.data      = A.data + idx(A,i,j)
    self.type      = A.type
    self.isview    = true
    setmetatable(self, matrix)
    
    return self
end

---
-- Returns _slice_ (copy of a sub-matrix) of matrix _A_. Similar to creating a
-- view and then calling @{copy} on it. See @{view} for a description of the
-- parameters.
-- @see view
function matrix.slice(A, i, j, m, n)
    return A:view(i,j,m,n):copy()
end

---
-- Returns a copy of matrix _A_. one can force the copy to be a complex matrix with _tocomplex_
-- equal to true. Only the shape and contents of _A_ are copied, i.e, fields set by the user
-- are not copied. A copy of a view will copy the respective contents from the original matrix, and
-- the returned matrix will not be a view.
function matrix.copy(A, tocomplex)
    local m, n = A.m, A.n
    local B    = matrix.new(A.m, A.n, A:iscomplex() or tocomplex)
    for i = 1, m do
        for j = 1, n do
            set(B,i,j, get(A,i,j) )
        end
    end
    
    return B
end

---
-- Returns a view to column _j_ of matrix _A_.
-- @see view
function matrix.col(A, j)
    return matrix.view(A, 1, j, A.m, 1)
end

---
-- Returns a view to the _k_ (default: 0) diagonal of _A_.
-- When _k_ = 0, the main diagonal is returned.
function matrix.diag(A,k)
    k = k or 0
    local m = math.min(A.m, A.n - k)
    local V = A:view(1, k+1, m, 1)
    V.ld = A.n + 1
    return V
end

---
-- Returns a view to row _j_ of matrix _A_.
-- @see view
function matrix.row(A, i)
    return matrix.view(A, i, 1, 1, A.n)
end

---
-- Returns the dot product between two column vectors _A_ and _B_. _A_ and _B_ must have the same
-- number of rows.
function matrix.dot(A, B)
    assert(A.n == 1 and B.n == 1, "column vector expected")
    assert(A.m == B.m, "dimensions do not match")
    local sum = 0
    
    for i = 1, A.m do
        sum = sum + get(A,i)*get(B,i)
    end
    
    return sum
end

---
-- Returns the Frobenius norm of the column vector _A_.
function matrix.norm(A)
    assert(A.n == 1, "column vector expected")
    local sum = 0
    local c   = A:iscomplex()
    
    for i = 1, A.m do
        local v = get(A,i,1)
        if c then v = complex.abs(v) end
        sum = sum + v*v
    end
    
    return math.sqrt(sum)
end

---
-- Returns the squared Frobenius norm of the column vector _A_.
function matrix.sqnorm(A)
    assert(A.n == 1, "column vector expected")
    local sum = 0
    local c   = A:iscomplex()
    
    for i = 1, A.m do
        local v = get(A,i,1)
        if c then v = complex.abs(v) end
        sum = sum + v*v
    end
    
    return sum
end

---
-- Returns the orthogonal projection of _A_ on _B_, both being column vectors with the
-- same number of rows.
function matrix.proj(A,B) -- asserts already done by dot()
    return ( A:dot(B)/B:dot(B) ) * B
end

---
-- Returns the cross-product (a column vector) between two 3D vectors _A_ and _B_.
function matrix.cross(A, B)
    assert(A.n == 1 and B.n == 1 and A.m == 3 and B.m == 3, "3D vectors expected")
    local C = matrix.new(3, 1, A:iscomplex())
    set(C, 1, 1, get(A,2)*get(B,3)-get(A,3)*get(B,2) )
    set(C, 2, 1, get(A,3)*get(B,1)-get(A,1)*get(B,3) )
    set(C, 3, 1, get(A,1)*get(B,2)-get(A,2)*get(B,1) )
    return C
end

---
-- Returns the transpose of matrix _A_ (not a view). If _hermitian_ is true,
-- the conjugate transpose of _A_ is returned instead.
function matrix.transpose(A, hermitian)
    local B = matrix.new(A.n, A.m, A:iscomplex())
    local h = hermitian and A:iscomplex()
    
    for i = 1, A.m do
        for j = 1, A.n do
            local val = get(A,i, j)
            set(B,j, i, h and val:conj() or val)
        end
    end
    
    return B
end

-- If both matrices have real entries, return then. Else, return both as
-- complex matrices.
local function check_complex(A, B)
    if A:iscomplex() then
        if not B:iscomplex() then
            B = B:copy(true)
        end
    else
        if B:iscomplex() then
            A = A:copy(true)
        end
    end
    
    return A, B
end

---
-- General matrix-matrix multiplication (C <- alpha ta(A)\*ta(B) + beta C). For common matrix
-- multiplication, one can use the multiplication operator. This function can be used when one
-- wants to reuse a matrix _C_ to store the results, when _A_ and/or _B_ are to be transposed
-- before the multiplication (controlled by _ta_ and _tb_) or when one wants to multiply by a scalar
-- the result of the product A\*B (_alpha_) or to change the initial contribution of _C_ (_beta_).
-- @usage C = A*B
-- @usage matrix.mmul(A, B, C, 1, 2, true, false) -- store A:t()*B+2*C in C
function matrix.mmul(A, B, C, alpha, beta, ta, tb)
    assert(A.n == B.m, "dimensions do not match")
    A, B = check_complex(A, B)
    
    local m = A.m
    local n = B.n
    local k = A.n
    
    C     = C     or matrix.new(m, n, A:iscomplex())
    alpha = alpha or 1
    beta  = beta  or 0
    
    ta = ta and cblas.CblasTrans or cblas.CblasNoTrans
    tb = tb and cblas.CblasTrans or cblas.CblasNoTrans
    
    if A:iscomplex() then
        alpha = ffi.new('complex[1]', {alpha} )
        beta  = ffi.new('complex[1]', {beta} )
        
        cblas.cblas_zgemm(RowMajor, ta, tb, m, n, k, alpha,
            A.data, A.ld, B.data, B.ld, beta, C.data, C.ld);
    else
        cblas.cblas_dgemm(RowMajor, ta, tb, m, n, k, alpha,
            A.data, A.ld, B.data, B.ld, beta, C.data, C.ld);
    end
        
    return C
end

---
-- Performs a LU decomposition of matrix _A_. Returns the lower triangular
-- matrix L with shape _A.m_ x min(_A.m_,_A.n_), the upper triangular matrix U
-- with shape min(_A.m_,_A.n_) x _A.n_ and the permutation matrix P with shape
-- _A.m_ x _A.n_, such that PA = LU, plus operation info. If _inline_ is
-- true, this function will modify the contents of _A_. If _merged_ is true,
-- this function will return matrix _B_ with the same shape as _A_, with
-- matrices L and U merged in the same space, the pivots _pinv_ as an
-- array of integers (LuaJIT type), and the operation info.
--
-- @usage L,U,P,info = A:lu()
-- @usage B,piv,info = A:lu(true,true)
function matrix.lu(A, inline, merged)
    local minz = math.min(A.m, A.n)
    local piv  = ffi.new("int[?]", minz)
    if not inline then A = A:copy() end
    
    local info
    if A:iscomplex() then
        info = lapacke.LAPACKE_zgetrf(RowMajor, A.m, A.n, A.data, A.ld, piv)
    else
        info = lapacke.LAPACKE_dgetrf(RowMajor, A.m, A.n, A.data, A.ld, piv)
    end

    if merged then return A, piv, info end
    
    -- not merged, hard/boring work ahead
        
    -- lower triangular
    local L = matrix.zeros(A.m, minz, A:iscomplex() )
    for i = 1, A.m do
        for j = 1, math.min(i, A.n) do
            if i == j then
                set( L, i, j, 1 )
            else
                set( L, i, j, get(A,i,j) )
            end
        end
    end
    
    -- upper triangular
    local U = matrix.zeros(minz, A.n, A:iscomplex() )
    for i = 1, minz do
        for j = i, A.n do
            set( U, i, j, get(A,i,j) )
        end
    end
    
    -- permutation
    local v = matrix.linspace(1, A.m, A.m)
    for i = 1, minz do
        local aux = v(i)
        set(v, i, 1, v(piv[i-1]))
        set(v, piv[i-1], 1, aux)
    end
    
    local P = matrix.zeros(A.m, A.m)
    
    for i = 1, A.m do
        set(P, i, v(i), 1)
    end
    
    return L, U, P, info
end

---
-- Performs a QR decomposition / factorization of matrix _A_. _A.m_ must
-- be greater or equal than _A.n_. Returns matrices _Q_ and _R_, where
-- _A_ = _Q_ * _R_, and _Q_ is an orthogonal matrix and _R_ is an upper
-- triangular matrix. If _inline_ is true, this function will modify the
-- contents of _A_.
function matrix.qr(A, inline)
    assert(A.m >= A.n, "dimensions do not match")

    if not inline then A = A:copy() end
    local nr  = math.min(A.m, A.n)
    local info1, info2, R
    
    if A:iscomplex() then
        local TAU = matrix.new(nr, 1, true)
        info1 = lapacke.LAPACKE_zgeqrf(RowMajor, A.m, A.n, A.data, A.ld, TAU.data)
        R = matrix.zeros(nr, A.n, true)
        
        for i = 1, nr do for j = i, A.n do
                R:set(i,j, A:get(i,j))
        end end
        
        info2 = lapacke.LAPACKE_zungqr(RowMajor, A.m, A.n, nr, A.data, A.ld, TAU.data)
    else
        local TAU = matrix.new(nr, 1 )
        info1 = lapacke.LAPACKE_dgeqrf(RowMajor, A.m, A.n, A.data, A.ld, TAU.data)
        R = matrix.zeros(nr, A.n)
        
        for i = 1, nr do for j = i, A.n do
                R:set(i,j, A:get(i,j))
        end end
        
        info2 = lapacke.LAPACKE_dorgqr(RowMajor, A.m, A.n, nr, A.data, A.ld, TAU.data)
    end
            
    return A, R, info1, info2
end

---
-- Returns the inverse of matrix _A_. If _inline_ is true, the inversion will modify the values of
-- _A_ instead of creating a copy of _A_ (returning _A_ instead of a new matrix).
function matrix.inv(A, inline)
    assert(A.m == A.n, "dimensions do not match")
    local LU, piv, info = A:lu(inline, true)
    if info ~= 0 then return nil, info end
    
    local info
    if A:iscomplex() then
        info = lapacke.LAPACKE_zgetri(RowMajor, A.m, LU.data, A.ld, piv)
    else
        info = lapacke.LAPACKE_dgetri(RowMajor, A.m, LU.data, A.ld, piv)
    end
    
    return LU, info
end

---
-- Returns the pseudoinverse of _A_. It is computed by means of svd(),
-- using _tol_ (default: 1e-12) to decide for zeros.
-- @see svd
function matrix.pinv(A, tol)
    tol = tol or 1e-12

    local U,S,V = A:svd()
    local PS    = matrix.zeros(A.n, A.m)
    for i = 1, S.m do -- S.m = will be min(A.m,A.n)
        local val = get(S,i,1)
        if math.abs(val) > tol then
            set(PS, i,i, 1/val)
        end
    end
    
    return V:t(true)*PS*U:t(true)
end

---
-- Solves the generalized least squares problem.
-- Returns matrix _x_, with _A.m_ rows and _b.n_ cols. The least squares
-- problem is solved by using the QR decomposition. _W_ is an optional covariance / weight
-- matrix for the fitting.
-- @see qr
-- @see solve
function matrix.ls(A, b, W)
    if W then
        A = W*A
        b = W*b
    end
    
    local Q, R = A:qr()
    local c = Q:t(true)*b
    return R:solve(c, false, true)
end

---
-- Estimates the rank of matrix _A_. This estimation is based on the
-- singular value decomposition, using _tol_ (default: 1e-12) to decide
-- if a value is zero.
-- @see svd
function matrix.rank(A, tol)
    tol = tol or 1e-12
    local U, S, VT = A:svd()
    local rank = 0
    
    for i = 1, S.m do
        if math.abs( S(i) ) >= tol then
            rank = rank + 1
        end
    end
    
    return rank
end

--- Alias for matrix.transpose().
-- @see transpose
-- @function t
-- @param A
matrix.t = matrix.transpose

---
-- Computes the eigenvectors and eigenvalues of matrix _A_. If w (default 'R') is 'R', then the
-- right eigenvectors are computed. If it is 'L', then the left ones are. Returns a column matrix
-- with the eigenvalues, a matrix with the eigenvectors in the columns, and information about the results;
-- note that the eigenvalues/eigenvectors are not sorted by eigenvalue.
function matrix.eig(A, w, nocopy)
    assert(A.m == A.n, "dimensions do not match")
    w = w or 'R'
    if not notcopy then A = A:copy() end
    
    local jobvl = ffi.new("char[1]", w == 'L' and 'V' or 'N')
    local jobvr = ffi.new("char[1]", w == 'R' and 'V' or 'N')
    local V     = matrix.new(A.n, A.n, A:iscomplex())
        
    if A:iscomplex() then
        local w    = matrix.new(A.n, 1, true)
        local info = lapacke.LAPACKE_zgeev(RowMajor, jobvl[0], jobvr[0], A.n,
            A.data, A.ld, w.data, V.data, V.ld, V.data, V.ld)
        
        return w, V, info
    else
        local wr = matrix.new(A.n)
        local wi = matrix.new(A.n)
        
        local info = lapacke.LAPACKE_dgeev(RowMajor, jobvl[0], jobvr[0], A.n,
            A.data, A.ld, wr.data, wi.data, V.data, V.ld, V.data, V.ld)
            
        return wr + wi:copy(true), V, info
    end
end

---
-- Returns the determinant of _A_. The determinant is computed by using
-- the LU decomposition. If _inline_ is true, matrix _A_ will be modified.
-- @see rank
-- @see lu
function matrix.det(A, inline)
    assert(A.m == A.n, "dimensions do not match")
    local B, pinv = A:lu(inline, true)
    local s = 0
    
    for i = 1, A.m do
        if pinv[i-1] ~= i then s = s + 1 end
    end

    local det  = B:diag():prod()
    return s % 2 == 0 and det or -det
end

---
-- Computes the singular value decomposition (SVD) of matrix _A_. The SVD of
-- _A_ is in the form _A_ = U S Vt, where U is a _A.m_ x _A.m_ matrix, S is a
-- min(_A.m_,_A.n_)x1 matrix and VT is a _A.n_ x _A.n_ matrix. Returns U, S, VT
-- and information about the result.
-- @usage U, S, VT, info = A:svd()
function matrix.svd(A, nocopy)
    if not nocopy then A = A:copy() end
    local cpx = A:iscomplex()
    
    local job  = ffi.new("char[1]", "A")
    local U    = matrix.new(A.m, A.m, cpx)
    local S    = matrix.new(math.min(A.m, A.n))
    local VT   = matrix.new(A.n, A.n, cpx)
    local sup  = ffi.new("double[?]", math.min(A.m,A.n))
    local info
    
    if cpx then
        info = lapacke.LAPACKE_zgesvd(RowMajor, job[0], job[0], A.m, A.n,
            A.data, A.ld, S.data, U.data, U.ld, VT.data, VT.ld, sup);
    else
        info = lapacke.LAPACKE_dgesvd(RowMajor, job[0], job[0], A.m, A.n,
            A.data, A.ld, S.data, U.data, U.ld, VT.data, VT.ld, sup);
    end
        
    return U, S, VT, info
end

---
-- Returns matrix _X_ where _X_ is a solution to the system  _A\*X = B_. If _trans_ is true, then
-- the transpose of _A_ will be used. _A_ is an _N_ x _N_ matrix, while _X_ and _B_ are _N_ x _B.n_
-- matrices (_B_ can have more than one column). If _nocopy_ is true then matrices _A_ and _B_
-- will be modified. This functions also returns error bonds and condition estimates.
-- @usage x, rcond, ferr, berr, rpivot, info = A:solve(B)
function matrix.solve(A, B, trans, nocopy)
    assert(A.m == A.n and A.m == B.m, "dimensions do not match")
    A, B = check_complex(A, B)
    
    if not nocopy then
        A = A:copy()
        B = B:copy()
    end
    
    local fact   = ffi.new("char[1]", 'E')
    local trans  = ffi.new("char[1]", trans and 'T' or 'N')
    local af     = matrix.new(A.m, A.n, A:iscomplex())
    local piv    = ffi.new("int[?]", A.n)
    local equed  = ffi.new("char[1]")
    local r      = matrix.new(A.m)
    local c      = matrix.new(A.n)
    local x      = matrix.new(A.m, B.n, A:iscomplex())
    local rcond  = matrix.new(B.n)
    local ferr   = matrix.new(B.n)
    local berr   = matrix.new(B.n)
    local rpivot = matrix.new(B.n)
    
    local info
    if A:iscomplex() then
        info = lapacke.LAPACKE_zgesvx(RowMajor, fact[0], trans[0], A.n, B.n,
            A.data, A.ld, af.data, af.ld, piv, equed, r.data, c.data, B.data,
            B.ld, x.data, x.ld, rcond.data, ferr.data, berr.data, rpivot.data)
    else
        info = lapacke.LAPACKE_dgesvx(RowMajor, fact[0], trans[0], A.n, B.n,
            A.data, A.ld, af.data, af.ld, piv, equed, r.data, c.data, B.data,
            B.ld, x.data, x.ld, rcond.data, ferr.data, berr.data, rpivot.data)
    end
    
    return x, rcond, ferr, berr, rpivot, info
end

local function pretty(v)
    if math.abs(v) < 1e-15 then
        return 0
    else
        return v
    end
end

---
-- Returns a _string_ representation of matrix _A_. Values smaller than 1e-15 are shown as zeros.
-- Custom formats can be specified by passing _fmt_ (element formater), the element separator _elsep_ and the
-- line separator __linesep__.
function matrix.__tostring(A, fmt, elsep, linesep)
    local view   = A.isview and '(view) ' or ''
    local header = string.format('%s%s[%d][%d](%p)', view, A.type, A.m, A.n, A.data)
    local lines  = {}
     
    for i = 1, A.m do
        local line = {}
        
        for j = 1, A.n do
            local val = A:get(i,j)
            
            if A:iscomplex() then
                local r, i = val:both()
                local a = pretty(r)
                local b = pretty(i)
                local s = b >= 0 and '+' or ''
                table.insert(line, string.format(fmt or "%10.7g%s%.7gi", a, s, b))
            else
                val = pretty(val)
                table.insert(line, string.format(fmt or "%10.7g", val))
            end
        end
        
        table.insert(lines, table.concat(line, elsep or ' ') )
    end
    
    return header .. '\n' .. table.concat(lines, linesep or '\n')
end

---
-- @function tostring
-- @see __tostring
matrix.tostring = matrix.__tostring

---
-- Generates an operation between the elements of two matrices by using function _op_
-- @usage multiply_and_add_two = matrix.mmop(function(a,b) return a*b + 2 end)
function matrix.mmop(op)
    -- support operations between matrices and scalars
    
    local func
    func = function(A,B, inline)
        local mA, mB = matrix.is(A), matrix.is(B)
        assert(mA or mB, "only support operations involving at least one matrix")
        
        if mA and mB then
            assert(A.m == B.m and A.n == B.n, "dimensions do not match")
        end
        
        local M = mA and A or B
        
        local C = inline and M or matrix.new(M.m, M.n, matrix.iscomplex(A) or matrix.iscomplex(B))
        
        for i = 1, M.m do
            for j = 1, M.n do
                local val = op(mA and get(A,i,j) or A, mB and get(B,i,j) or B)
                set(C, i, j, val)
            end
        end
        
        return C
    end
    
    return func
end

---
-- Calls function _f(A,i,j)_, ignoring the return value, for each (i,j) index of matrix _A_,
-- returning matrix _A_.
-- @usage A:foreach(A,i,j) print(A:get(i,j)) end
function matrix.foreach(A, f)
    for i = 1, A.m do
        for j = 1, A.n do
            f(A,i,j)
        end
    end
    
    return A
end

---
-- Calls function _A[i][j] = _f_(A[i][j])_ for each (i,j) index of matrix _A_. The return of _f_ is
-- stores in the (i,j) index; if _inline_ is true, then it will operate on _A_ and return _A_,
-- and if _inline_ is false the function will operate on a (and return) copy of _A_.
-- @usage A:map(function(v) return 3*v end, true) -- mult. every cell by 3, modifying _A_
function matrix.map(A, f, inline)
    if not inline then A = A:copy() end
    
    for i = 1, A.m do
        for j = 1, A.n do
            local val = f( get(A,i,j) )
            set(A, i, j, val)
        end
    end
    
    return A
end

-- operations with scalars

---
-- Returns the sum of _a_ and _b_, where _a_ is a matrix and _b_ is a matrix
-- or a real / complex number. If _inline_ is true, the result will be stored in _a_.
-- @function add
-- @param a
-- @param b
-- @param inline
matrix.add = matrix.mmop(function(a,b) return a+b end)

---
-- Returns the subtraction of _a_ and _b_, where _a_ is a matrix and _b_ is a matrix
-- or a real / complex number. If _inline_ is true, the result will be stored in _a_.
-- @function sub
-- @param a
-- @param b
-- @param inline
matrix.sub = matrix.mmop(function(a,b) return a-b end)

---
-- Returns the scalar multiplication of _a_ and _b_, where _a_ is a matrix and _b_ is a matrix
-- or a real / complex number. If _inline_ is true, the result will be stored in _a_.
-- @function mul
-- @param a
-- @param b
-- @param inline
matrix.mul = matrix.mmop(function(a,b) return a*b end)

---
-- Returns the scalar division of _a_ and _b_ (a/b), where _a_ is a matrix and _b_ is a matrix
-- or a real / complex number. If _inline_ is true, the result will be stored in _a_.
-- @function div
-- @param a
-- @param b
-- @param inline
matrix.div = matrix.mmop(function(a,b) return a/b end)

-- Warning: pmax and pmin works because getting a value from a FFI complex
-- array returns a 'native' complex value, before converting it to a Lua
-- table!

---
-- Returns the pairwise maximum of _a_ and _b_, where _a_ is a matrix and _b_ is a matrix
-- or a real / complex number (complex numbers are compared by absolute value).
-- If _inline_ is true, the result will be stored in _a_.
-- @function pmax
-- @param a
-- @param b
-- @param inline
matrix.pmax = matrix.mmop(function(a,b) return a > b and a or b end)

---
-- Returns the pairwise minimum of _a_ and _b_, where _a_ is a matrix and _b_ is a matrix
-- or a real / complex number (complex numbers are compared by absolute value).
-- If _inline_ is true, the result will be stored in _a_.
-- @function pmin
-- @param a
-- @param b
-- @param inline
matrix.pmin = matrix.mmop(function(a,b) return a < b and a or b end)

---
-- Returns a matrix with the comparation result of _a_ < _b_. A matrix with
-- the same shape as _a_ is returned, where each cell _(i,j)_ will have the value 1
-- if _a(i,j)_ < _b(i,j)_ or 0 otherwise. If _b_ is a number, b will be used
-- instead of _b(i,j)_. If _inline_ is true, the result will be stored in _a_.
-- Complex numbers are compared by absolute value. When _a_ is complex,
-- the returned matrix is also complex.
-- @function lt
-- @param a
-- @param b
-- @param inline
matrix.lt = matrix.mmop(function(a,b) return a < b and 1 or 0 end)

---
-- Returns a matrix with the comparation result of _a_ xor _b_. A matrix with
-- the same shape as _a_ is returned, where each cell _(i,j)_ will have the value 
-- 1 if _a(i,j)_ ~= _b(i,j)_, 0 otherwise. If _b_ is a number, b will be used
-- instead of _b(i,j)_. If _inline_ is true, the result will be stored in _a_.
-- Complex numbers are compared by absolute value. When _a_ is complex,
-- the returned matrix is also complex.
-- @function xor
-- @param a
-- @param b
-- @param inline
matrix.xor = matrix.mmop(function(a,b)
    if complex.is(a) then
        return a:equal(b) and 0 or 1
    else
        return a ~= b and 1 or 0
    end
end)

---
-- Returns a matrix with the comparation result of _a_ > _b_. A matrix with
-- the same shape as _a_ is returned, where each cell _(i,j)_ will have the value 1
-- if _a(i,j)_ > _b(i,j)_ or 0 otherwise. If _b_ is a number, b will be used
-- instead of _b(i,j)_. If _inline_ is true, the result will be stored in _a_.
-- Complex numbers are compared by absolute value. When _a_ is complex,
-- the returned matrix is also complex.
-- @function gt
-- @param a
-- @param b
-- @param inline
matrix.gt = matrix.mmop(function(a,b) return a > b and 1 or 0 end)

---
-- Returns _-A_, where _A_ is a matrix.
-- @function unm
-- @param a
-- @param b
-- @param inline
matrix.unm = function(A,il) return A:map(function(e) return -e end, il) end
matrix.__add = matrix.add
matrix.__sub = matrix.sub
matrix.__unm = matrix.unm
matrix.__div = matrix.div
matrix.__mul = function(A,B) -- handle scalar/matrix and matrix/matrix op
    if matrix.is(B) and matrix.is(A) then
        return A:mmul(B)
    else
        return matrix.mul(A,B)
    end
end

local function mpow(A, n)
    if n == 0     then return matrix.eye(A.m, A:iscomplex()) end
    if n == 1     then return A end
    if n % 2 == 0 then return mpow(A*A, n / 2) end
    return A*mpow(A*A, (n - 1) / 2)
end

---
-- Fast exponentiation. Returns _A_ to the n-th power, where _n_ is an integer greater or equal to
-- -1.
function matrix.pow(A, n)
    assert(n == math.floor(n), "integer powers only")
    if n == -1 then return A:inv() end
    assert(n >= 0, "power must be equal to -1 or positive")
    return mpow(A, n)
end

matrix.__pow = matrix.pow

---
-- Fills matrix _A_ with values. If _e_ is a function, _e_ will be called
-- without parameters for each cell. If _e_ is a matrix, then _A[i][j]_ will be set to _e[i][j]_.
-- Returns matrix _A_.
-- @usage A:fill(42)
-- @usage A:fill(-5+2i)
-- @usage A:fill(math.random)
-- @usage A:fill(function() return 2*math.random() - 5 end)
function matrix.fill(A, e)
    local mtx = matrix.is(e)
    if mtx then
        assert(e.m >= A.m and e.n >= A.n, "dimensions do not match")
    end
    
    for i = 1, A.m do
        for j = 1, A.n do
            if mtx then
                set(A,i,j, e:get(i,j) )
            elseif type(e) == 'function' then
                set(A,i,j, e()) 
            else
                set(A,i,j, e)
            end
        end
    end
    
    return A
end

---
-- Serializes matrix _A_ to a string. Separates rows by a newline ('\n') and values by _sep_
-- (default ' '), returning the string.
function matrix.serialize(A, sep)
    local t = {}
    sep = sep or ' '
    
    for i = 1, A.m do
        for j = 1, A.n do
            if j > 1 then table.insert(t, sep) end
            table.insert(t, tostring(A:get(i,j)) )
        end
        table.insert(t, '\n')
    end
    
    return table.concat(t, '')
end

local function split_str(str, pattern)
    local tbl = {}
    str:gsub(pattern, function(f)
        local val = tonumber(f)
        
        if not val then
            local _, _, r, i = f:find("(-?%d+)([+-]%d+)")
            val = complex.cnew( tonumber(r), tonumber(i) )
        end
        
        tbl[#tbl+1] = assert(val, "input is not a number")
    end)
    return tbl
end

---
-- Loads a serialized matrix from the file at _path_. Will use _sep_ (default 'space') as
-- value separator and '\n' as row separator. The first _skip_ (default: 0) rows will be ignored.
-- Returns the loaded matrix.
function matrix.load(path, sep, skip, cols)
    local f       = assert(io.open(path), "could not open file")
    local ltable  = {}
    local pattern = ("([^%s ]+)"):format(sep or ' ')
    skip = skip or 0
    
    while true do
        local line = f:read('*l')
        if not line then break end
        
        if skip > 0 then
            skip = skip - 1
        else
            local t = split_str(line, pattern)
            if #t == 0 then break end
            ltable[#ltable+1] = t 
        end
    end
    
    f:close()
    return matrix.fromtable(ltable)
end

---
-- Saves the matrix _A_ to a file at _path_. Will use _sep_ as value separator ('\n' for row separator).
function matrix.save(A, path, sep)
    local f = assert(io.open(path, 'w'), "could not open file")
    f:write(A:serialize(sep))
    f:close()
end

---
-- Returns the real part of matrix _A_. Returns a new real number matrix.
function matrix.real(A)
    if not A:iscomplex() then return A:copy() end
    local R = matrix.new(A.m, A.n)
    return R:foreach(function(R, i, j) R:set(i, j, A:get(i,j).re) end)
end

---
-- Returns the imaginary part of matrix _A_. Returns a new real number matrix.
function matrix.imag(A)
    if not A:iscomplex() then return matrix.zeros(A.m,A.n) end
    return matrix.new(A.m,A.n):foreach(function(R, i, j) R:set(i,j, A:get(i,j).im) end)
end

---
-- Returns the absolute value of matrix _A_. Returns a new real number matrix.
function matrix.abs(A)
    if not A:iscomplex() then return A:copy() end
    return matrix.new(A.m, A.n):foreach(function(R, i, j) R:set(i,j, A:get(i,j):abs() ) end)
end

---
-- Returns the complex argument (phase angle) of matrix _A_. Returns a new real number matrix.
function matrix.arg(A)
    if not A:iscomplex() then return matrix.zeros(A.m,A.n) end
    return matrix.new(A.m, A.n):foreach(function(R, i, j) R:set(i,j, A:get(i,j):arg() ) end)
end

---
-- Sorts matrix _A_ as it was a flat vector (row1 .. row2 .. rowN). Tries to sort inplace (heap sort),
-- but may have to copy the matrix (_force = true_ to force the copy). Returns the sorted matrix
-- and a column vector with the memory position reordering that was used to sort the matrix. The sort
-- procedure follows the ordering given by function _lt(a,b)_, that should return true if _a_ is
-- less than _b_, false otherwise (uses the operator __lt by default).
-- @see reorder
-- @usage A, order = A:sort(function(a,b) return a:abs() > b:abs() end)
function matrix.sort(A, force, lt)
    if A.isview or force then A = A:copy() end
    return sort.heapsort(A, lt)
end

---
-- Reorders matrix _A_ with the ordering vector _order_ as it was a flat vector (row1 row2 row3).
-- Tries to work inplace, but may have to copy the matrix (a copy can be force with _force = true_).
-- @see sort
-- @usage A, order = A:sort();  B = B:reorder(order)
function matrix.reorder(A, order, force)
    assert(A:len() == order.m, "dimensions do not match")
    if A.isview or force then A = A:copy()    end
    if order.isview then order = order:copy() end -- just in case
    return sort.reorder(A, order)
end

---
-- Returns a new matrix created by rearranging the rows of _A_. If _col_ is true, then will rearrange
-- columns instead. The arrange is given by vector _order_. A row/column can be
-- used multiple times.
-- @usage B = A:arrange( matrix.fromtable{1,1,2,2} ) -- B now have 4 rows: A[1],A[1],A[2],A[2]
function matrix.arrange(A, order, col)
    if not col then
        return sort.arrangerows(A, order)
    else
        return sort.arrangecols(A, order)
    end
end

---
-- Returns a real column vector with _n_ (default 100) equally-spaced points from _v1_ to _v2_.
function matrix.linspace(v1, v2, n)
    if n then
        n = math.ceil(n)
    else
        n = 100
    end
    
    assert(v2 >= v1 and n > 0, "invalid parameters")
    local dt = (v2 - v1) / (n > 1 and n-1 or 1)
    return matrix.new(n, 1):foreach(function(A,i) A:set(i,1, v1 + dt*(i-1)) end)
end

---
-- Returns the maximum entry of matrix _A_ and its (i,j) index. For a complex matrix, it
-- compares by the absolute value.
function matrix.max(A)
    local mi, mj = 1, 1
    for i = 1, A.m do
        for j = 1, A.n do
            if A:get(i,j) > A:get(mi,mj) then
                mi, mj = i, j
            end
        end
    end
    return A:get(mi,mj), mi, mj
end

---
-- Returns the minimum entry of matrix _A_ and its (i,j) index. For a complex matrix, it
-- compares by the absolute value.
function matrix.min(A)
    local mi, mj = 1, 1
    for i = 1, A.m do
        for j = 1, A.n do
            if A:get(i,j) < A:get(mi,mj) then
                mi, mj = i, j
            end
        end
    end
    return A:get(mi,mj), mi, mj
end

---
-- Return the total number of entries in the matrix _A_.
function matrix.len(A)
    return A.m*A.n
end

---
-- Returns the sum of all elements in matrix _A_.
function matrix.sum(A)
    local total = 0
    A:foreach(function(A,i,j) total = total + A:get(i,j) end)
    return total
end

---
-- Returns a row vector with the mean of each column of _A_. If _row_
-- is true, a column vector with the mean of each row will be returned
-- instead.
function matrix.summ(A, row)
    if row then
        return matrix.new(A.m, 1, A:iscomplex() ):foreach(function(B,i,_)
            B:set(i, 1, A:row(i):sum() )
        end)
    else
        return matrix.new(1, A.n, A:iscomplex() ):foreach(function(B,_,j)
            B:set(1,j, A:col(j):sum() )
        end)
    end
end

---
-- Returns the product of all elements in matrix _A_.
function matrix.prod(A)
    local total = 1
    A:foreach(function(A,i,j) total = total * A:get(i,j) end)
    return total
end

---
-- Broadcasting. Calls function _f(A[r], B, true)_ for every row A[r] of matrix _A_.
-- @usage A:bsx(vec, matrix.add)
function matrix.bsx(A, B, f)
    for r = 1, A.m do
        f(A:row(r), B, true)
    end
    
    return A
end

---
-- Returns a new matrix with only the _i_ rows of matrix _A_ in which _cond(A,i)_ returns true.
-- @usage B = A:filter(function(A,i) return A(i) <= 5 end)
function matrix.filter(A, cond)
    local idx = {}
    local qt  = 0
    local rows, cols = A.m, A.n
    
    for i = 1, rows do
        if cond(A,i) then
            table.insert(idx, i)
            qt = qt + 1
        end
    end
    
    local out = matrix.new(qt, cols)
    
    for i = 1, qt do
        out:row(i):fill(A:row(idx[i]))
    end
    
    return out
end

---
-- Returns a column vector with the unique values from the column or row vector _A_.
function matrix.unique(A)
    assert(A.m == 1 or A.n == 1)
    local set, vals = {}, {}
    local n = 0
    
    for i = 1, A.m do
        for j = 1, A.n do
            local v = A:get(i,j)
            local key
            if complex.is(v) then
                local r, i = v:both()
                key = r .. ':' .. i
            else
                key = v
            end
            
            if not set[key] then
                set[key] = true
                table.insert(vals, v)
                n = n + 1
            end
        end
    end
    
    return matrix.fromtable(vals)
end

---
-- Returns the conjugate transpose of _A_. This return is a new matrix of the
-- same shape of _A_.
function matrix.conj(A)
    return A:map(complex.conj)
end

---
-- Returns the trace (sum of the elements in the main diagonal) of
-- matrix _A_.
function matrix.trace(A)
    return matrix.diag(A):sum()
end

---
-- Limits the values of matrix _A_ to the range [a,b]. Values smaller
-- than a will be changed to a, and values higher than b will be changed
-- to b. Complex numbers are compared by absolute value. If _inline_ is
-- true, this function modifies and returns _A_ instead of a new matrix.
-- @usage B = A:clip(0, 10)
-- @usage A:clip(-1,1,true)
function matrix.clip(A, a, b, inline)
    if not inline then A = A:copy() end
    
    for i = 1, A.m do
        for j = 1, A.n do
            local val = get(A,i,j)
            if val < a then val = a end
            if val > b then val = b end
            set(A,i,j,val)
        end
    end
    
    return A
end

local function make_helper(op1,op2)
    return function(A)
        if not matrix.iscomplex(A) then
            return A:map(op1)
        else
            return A:map(op2)
        end
    end
end

---
-- Returns the sine of each element in _A_.
-- @param A
-- @function sin
matrix.sin  = make_helper(math.sin , complex.sin )

---
-- Returns the cosine of each element in _A_.
-- @param A
-- @function cos
matrix.cos  = make_helper(math.cos , complex.cos )

---
-- Returns the arcsine of each element in _A_.
-- @param A
-- @function asin
matrix.asin  = make_helper(math.asin , complex.asin )

---
-- Returns the arccosine of each element in _A_.
-- @param A
-- @function acos
matrix.acos  = make_helper(math.acos , complex.acos )

---
-- Returns the natural logarithm of each element.
-- @function log
matrix.log  = make_helper(math.log , complex.log )

---
-- Returns the base-10 logarithm of each element.
-- @function log10
matrix.log10 = make_helper(math.log10 , complex.log10 )

---
-- Returns e^x for each element in _A_.
-- @param A
-- @function exp
matrix.exp  = make_helper(math.exp , complex.exp )

---
-- Returns the square root of each element in _A_. This does not computes the square root of the
-- matrix.
-- @param A
-- @function sqrt
matrix.sqrt = make_helper(math.sqrt, complex.sqrt)

---
-- Raises each element of _A_ to the $n-th$ power, returning the results as a matrix.
-- If _inline_ is true, _A_ will have its contents modified.
function matrix.epow(A, n, inline)
    if not inline then A = A:copy() end
    local f = A:iscomplex() and complex.pow or math.pow
    
    for i = 1, A.m do
        for j = 1, A.n do
            local val = f( get(A,i,j), n )
            set(A, i, j, val)
        end
    end
    
    return A
end

return matrix
