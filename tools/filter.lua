--[[
    Small hack to remove unused single-precision headers.
--]]
local headers = require('headers')

local filters = {
    "void cblas_s.-(.-);",
    "float cblas_s.-(.-);",
    "int cblas_s.-(.-);",
    "void LAPACKE_s.-(.-);",
    "float LAPACKE_s.-(.-);",
    "int LAPACKE_s.-(.-);",
    "void cblas_c.-(.-);",
    "float cblas_c.-(.-);",
    "int cblas_c.-(.-);",
    "void LAPACKE_c.-(.-);",
    "float LAPACKE_c.-(.-);",
    "int LAPACKE_c.-(.-);",
    "void catlas_.-(.-);",
    "int catlas_.-(.-);",
    "float catlas_.-(.-);",
    "double catlas_.-(.-);",
}

for _, filter in ipairs(filters) do
    headers = headers:gsub(filter, '')
end

headers = headers:gsub("\n+", "\n")
print(headers)
