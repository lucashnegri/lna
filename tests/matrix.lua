module(..., package.seeall)

local matrix  = require('lna.matrix' )
local complex = require('lna.complex')

function setup()
    tol = 1e-9
end

function test_get_set_real()
    for i = 1, 100 do
        local A = matrix.eye(3)
        for i = 1, 3 do assert_equal(1, A:get(i,i), tol) end
        for i = 1, 3 do A:set(i, i, 3) end
        for i = 1, 3 do assert_equal(3, A:get(i,i), tol) end
    end
end

function test_get_set_complex()
    for i = 1, 100 do
        local A = matrix.eye(3, 3, true)
        for i = 1, 3 do assert_equal(1, A:get(i,i):abs(), tol) end
        for i = 1, 3 do A:set(i,i, complex.new(3,4) ) end
        for i = 1, 3 do assert_equal(5, A:get(i,i):abs(), tol) end
        for i = 1, 3 do assert_equal(3, A:get(i,i):real(), tol) end
        for i = 1, 3 do assert_equal(4, A:get(i,i):imag(), tol) end
        A:set(2,3, 5-2i)
        assert_true( A:get(2,3):equal(5-2i) )
        assert_true( A:get(2,3):equal(complex.new(5,-2) ) )
    
        A:set(3,1, complex.new(-2, 5))
        assert_true( A:get(3,1):equal(-2+5i) )
        assert_true( A:get(3,1):equal(complex.new(-2, 5) ) )
    end
end

function test_basic()
    local A = matrix.fromtable{ {1,2,3}, {4,5,6}, {7,8,9} }
    local B = matrix.new(3,3):fill(math.random)
    
    local C = B + A + B + B
    local D = C - B - B
    assert_true( A:equal(D-B, tol) )
end

function test_creation()
    local A = matrix.fromtable{ {0,0,0}, {0,0,0}, {0,0,0} }
    local B = matrix.fromtable{ {1,0,0}, {0,1,0} }
    local C = matrix.rand(4,2)
    
    assert_true ( matrix.zeros(3,3):equal(A) )
    assert_true ( matrix.eye(2,3):equal(B)   )
    assert_false(A:equal(B,tol)              )
    assert_equal(4, C.m)
    assert_equal(2, C.n)
end

function test_basic_scalar()
    local A = matrix.fromtable{ {1,2,3}, {4,5,6}, {7,8,9} }
    local B = matrix.new(3,3):fill(math.random)
    
    local C = A + B*3
    local D = C * 2 + 5
    local E = ((D - B*6) -5) / 2
    assert_true( A:equal(E, tol) )
end

function test_basic_complex()
    local A = matrix.fromtable{ {1,2,3}, {4,5,6}, {7,8,9} }
    local B = matrix.new(3,3):fill(math.random)
    
    local C = A + B*(3+5i)
    local D = C * (-2-2i) + (3+2i)
    local E = (D - (3+2i)) / (-2-2i) - B*(3+5i)
    assert_true( A:equal(E:real(), tol) )
end

function test_mul_pow()
    for i = 1, 10 do
        local A = matrix.fromtable{ {1,2,3,4}, {-1,-2,-3,-4}, {0,1,0,1}, {1,1,1,1} }
        local B = matrix.fromtable({ {1}, {-1}, {-2}, {-1+3i} }, true)
        local C = A*B
    
        assert_true( C:equal(matrix.fromtable{
            {-11+12i}, {11-12i}, {-2+3i}, {-3+3i}
        }, tol))
    
        assert_false( C:equal(matrix.fromtable{
            {-11+12i}, {11-12i}, {-2+3i}, { 3-3i}
        }, tol))
    end
end

function test_solve()
    for i = 1, 100 do
        local A = matrix.fromtable{ {5+0i, 6}, {0-3i, 5} }
        local B = matrix.fromtable{ {1, 2}, {-1, -2} }
        local x = A:solve(B)
        assert_equal(B, (A*x):real(), tol)
    end
end

function test_view()
    local A = matrix.new(50,50)
    local V = A:view(20, 25, 15, 15)
    
    for t = 1, 100 do
        local i, j = math.random(1,15), math.random(1,15)
        A:set(i+19, j+24, t)
        assert_equal(t, V:get(i, j) )
    end
    
    for t = 1, 100 do
        local i, j = math.random(1,15), math.random(1,15)
        V:set(i, j, t)
        assert_equal(t, A:get(i+19, j+24) )
    end
    
    local V1 = A:view(18, 27, 15, 15)
    local V2 = A:view(18, 27, 15, 15)
    assert_false( A:equal(V1) )
    assert_false( A:equal(V2) )
    assert_false( V:equal(V1) )
    assert_false( V:equal(V2) )
    assert_false( A:equal(V) )
    assert_true ( V1:equal(V2) )
    assert_true ( V2:equal(V1) )
end

function test_slice()
    local A = matrix.new(4,4):fill(2)
    local B = A:slice(2,2,2,2)
    B:fill(5)
    assert_equal(2, A:get(2,2))
    assert_equal(2, A:get(2,3))
    assert_equal(2, A:get(3,2))
    assert_equal(2, A:get(3,3))
    assert_true(B:equal(matrix.fromtable{ {5,5}, {5,5} }))
end

function test_transpose()
    local A = matrix.fromtable{ {1, 2, 3, 4, 5}, {6, 5, 4, 3, 2}}
    local B = matrix.fromtable{ {1,6}, {2,5}, {3,4}, {4,3}, {5,2} }
    assert_true(A:equal(B:t()))
    assert_true(B:equal(A:t()))
end

function test_dot()
    local a = matrix.fromtable{1,2,3,5}
    local b = matrix.fromtable{-1,2,-3,4}
    assert_equal(14, a:dot(b), tol)
    assert_equal(14, b:dot(a), tol)
    assert_not_equal(14, a:dot(a), tol)
    assert_not_equal(14, b:dot(b), tol)
end

function test_cross()
    local a = matrix.fromtable{5,0,3}
    local b = matrix.fromtable{3,0,-2}
    local c = a:cross(b)
    assert_equal(0 , c:get(1), tol)
    assert_equal(19, c:get(2), tol)
    assert_equal(0 , c:get(3), tol)
    local c = b:cross(a)
    assert_equal(0 , c:get(1), tol)
    assert_equal(-19, c:get(2), tol)
    assert_equal(0 , c:get(3), tol)
end

function test_inv_real()
    local A = matrix.fromtable{ {1,2}, {3,4} }
    local B = A:inv()
    local C = matrix.eye(2)
    assert_true( C:equal( A*B, tol ) )
    assert_true( C:equal( B*A, tol ) )
end

function test_inv_complex()
    local A = matrix.fromtable{ {5+2i,0-5i}, {10-5i, 3} }
    local B = A:inv()
    local C = matrix.eye(2, 2, true)
    assert_true( C:equal( A*B, tol ) )
    assert_true( C:equal( B*A, tol ) )
end

function test_inline()
    local A = matrix.fromtable{ {1,2}, {3,4} }
    local B = A*2
    A:map(function(a) return a * 2 end, true )
    assert_true( B:equal(A, tol) )
    A:map(function(a) return a * 5 end, false)
    assert_true( B:equal(A, tol) )
    A:foreach(function(A,i,j) A:set(i,j,5) end)
    assert_true( matrix.new(2,2):fill(5):equal(A,tol) )
end

function test_sort()
    for i = 1, 5 do
        do
            local a1 = matrix.new(math.random(2,1e5), 1):fill(math.random)
            local a2 = a1:copy()
            local a1, o1 = a1:sort()
            a2 = a2:reorder(o1)
            assert_true(a1:equal(a2))
            assert_true( a1(1) <= a1(2) )
        end
        
        do
            local a1 = matrix.new(math.random(1,1e5), 1):fill(math.random)
            local a2 = a1:copy()
            local a1, o1 = a1:sort(false, function(a,b) return a > b end)
            a2 = a2:reorder(o1)
            assert_true(a1:equal(a2))
            assert_true( a1(1) >= a1(2) )
        end
    end
end

function test_linspace()
    local s = matrix.linspace(10, 10, 1)
    assert_equal(s.m, 1)
    assert_equal(s.n, 1)
    assert_equal(10, s:get(1))
    local s2 = matrix.linspace(-2,2,6)
    assert_equal(6, s2.m)
    assert_equal(1, s2.n)
    local r = matrix.fromtable{-2, -1.2, -0.4, 0.4, 1.2, 2}
    assert_true(s2:equal(r))
    
    local s3 = matrix.linspace(1, 5, 5)
    assert_true(s3:equal(matrix.fromtable{1,2,3,4,5}))

    local s4 = matrix.linspace(1, 10)
    assert_equal(1 , s4(1)    )
    assert_equal(10, s4(s4.m) )
    assert_true(s4:get(50) > 1 and s4:get(50) < 10)
end

function test_min_max()
    local A     = matrix.fromtable{1,2,3,4,8,4,3,2,1} -- column vector
    local v,i,j = A:max()
    assert_equal(8, v)
    assert_equal(i, 5)
    assert_equal(j, 1)
    
    local B     = matrix.fromtable{ {1,2,3}, {9,2,20},{50,1,-2} }
    local v,i,j = B:max()
    assert_equal(50, v)
    assert_equal(i, 3)
    assert_equal(j, 1)
    
    local C     = matrix.fromtable{ {1-2i,2+5i}, {2,-1+50i} }
    local v,i,j = C:max()
    assert_true(v:equal(-1+50i))
    assert_equal(i, 2)
    assert_equal(j, 2)
end

function test_fill()
    local A = matrix.new(2,2)
    A:fill(-4)
    assert_true(A:equal(matrix.fromtable{{-4,-4},{-4,-4}}))
    
    A:fill(function() return 8 end)
    assert_true(A:equal(matrix.fromtable{{8,8},{8,8}}))
    
    local B = matrix.fromtable{{-9,20},{2,5}}
    A:fill(B)
    assert_true(A:equal(B))
end

function test_vector()
    local v = {1,2,3,2,1,0,-1}
    local A = matrix.fromtable{v} -- row vector
    local B = matrix.fromtable(v) -- column vector
    
    for i = 1, #v do
        assert_equal(v[i], A(i))
        assert_equal(v[i], B(i))
    end
end

function test_len()
    local A = matrix.new(30,40)
    local B = matrix.new(10,1)
    local C = matrix.new(1,5)
    
    assert_equal(30*40, A:len() )
    assert_equal(10   , B:len() )
    assert_equal(5    , C:len() )
end

function test_sum()
    local v1 = matrix.fromtable{1,2,3,2,1,0,-1}
    local v2 = matrix.fromtable{-9,-10,-11}
    assert_equal(8  , v1:sum())
    assert_equal(-30, v2:sum())
    
    local v3 = matrix.fromtable{ {5+10i, 3-2i}, {0-2i, -4+9i} }
    assert_true( v3:sum():equal(4+15i) )
end

function test_summ_real()
    local A = matrix.fromtable{
        {1, 2, 3, 4},
        {5, 6, 7, 8},
        {9, 8, 7, 6},
    }
    
    assert_true(A:summ():equal(matrix.fromtable{ {15, 16, 17, 18} }, tol))
    assert_true(A:summ(true):equal(matrix.fromtable{10, 26, 30}, tol))
end

function test_summ_complex()
    local A = matrix.fromtable{
        {1+10i, 2, 3, 4+3i},
        {5, 6-5i, 7, 8},
        {9, 8, 7, 6+2i},
    }
    
    assert_true(A:summ():equal(matrix.fromtable{ {15+10i, 16-5i, 17, 18+5i} }, tol))
    assert_true(A:summ(true):equal(matrix.fromtable{10+13i, 26-5i, 30+2i}, tol))
end

function test_prod()
    local v1 = matrix.fromtable{-1,-1,5,3,2}
    local v2 = matrix.fromtable{2,3,1,5,3}
    local v3 = matrix.fromtable{1+2i,5+2i,3}
    
    assert_equal( 30, v1:prod() )
    assert_equal( 90, v2:prod() )
    assert_true (v3:prod():equal(3+36i, tol) )
end

function test_equal()
    local m = matrix.new(5,5):fill(math.sqrt(-1))
    assert_false(m:equal(matrix.new(5,5)))
end

function test_arrange()
    local m = matrix.fromtable{
        {1,2,3},
        {4,5,6},
        {7,8,9},
        {5,5,5}
    }
    
    local order = matrix.fromtable{4,4,3,2,1}
    assert_true(m:arrange(order):equal(matrix.fromtable{
        {5,5,5},{5,5,5},{7,8,9},{4,5,6},{1,2,3}
    }))
    
    order = matrix.fromtable{2,1,1}
    
    assert_true(m:arrange(order,true):equal(matrix.fromtable{
        {2,1,1},{5,4,4},{8,7,7},{5,5,5}
    }))
end

function test_bsx()
    local m = matrix.fromtable{
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9}
    }
    local r = matrix.fromtable{ {7, 8, 9} }
    m:bsx(r, matrix.sub)
    assert_true(m:equal(matrix.fromtable{
        {-6, -6, -6},
        {-3, -3, -3},
        {0 ,  0,  0}
    }))
    
    m:bsx(10, matrix.add)
    assert_true(m:equal(matrix.fromtable{
        {4 , 4, 4},
        {7 , 7, 7},
        {10,10,10}
    }))
end

function test_filter()
    local A1 = matrix.fromtable{1,2,3,4,5,6,7,8,9}
    local A2 = matrix.fromtable{4,5,6}
    assert_true( A2:equal(A1:filter(function(A,i) return A(i) > 3 and A(i) < 7 end)) )
end

function test_unique()
    local A1 = matrix.fromtable{1,2,3,1,2,3,1,2,3,3,2,1}
    local A2 = matrix.fromtable{ {-5,-6,-2,-6,-2,-5,-6} }
    local A3 = matrix.fromtable{5+2i,5+3i,4+2i,4+2i,5+2i,5+3i}
    
    assert_true(A1:unique():equal(matrix.fromtable{1,2,3}))
    assert_true(A2:unique():equal(matrix.fromtable{-5,-6,-2}))
    local a = matrix.fromtable{5+2i,5+3i,4+2i}
    assert_true(A3:unique():equal(a))
end

function test_complex()
    local A = matrix.new(10,6):fill(math.random)
    local B = A * complex.i
    local Z = matrix.new(10,6):fill(0)
    
    assert_true(A:abs():equal(A, tol))
    assert_true(A:real():equal(A, tol))
    assert_true(A:imag():equal(Z, tol))
    assert_true(B:real():equal(Z, tol))
    assert_true(B:imag():equal(A, tol))
    assert_true(A:arg():equal(Z,tol))
    assert_false(B:arg():equal(Z,tol))
    
    assert_false(B:abs():iscomplex()  )
    assert_false(B:real():iscomplex() )
    assert_false(B:imag():iscomplex() )
    
    local C = matrix.fromtable{2-3i,6+8i}
    local D = matrix.transpose(C, true)
    local E = matrix.conj(C)
    
    assert_true(D:equal(matrix.fromtable{{2+3i, 6-8i}} ))
    assert_true(E:equal(matrix.fromtable{2+3i, 6-8i}    ))
end

function test_svd_real()
    for i = 1, 20 do
        local A = matrix.fromtable{
            {1,0,0,0,2},
            {0,0,3,0,0},
            {0,0,0,0,0},
            {0,4,0,0,0},
        }
        A = A + 0.1*matrix.rand(4,5)
        local U, S, VT, i = A:svd()
        local S2 = matrix.zeros(4,5)
        for i = 1, 4 do S2:set(i,i, S(i)) end
        assert_true(A:equal(U*S2*VT, tol))
    end
end

function test_svd_complex()
    for i = 1, 20 do
        local A = matrix.fromtable({
            {1,0,0,0,2},
            {0,0,3,0,0},
            {0,0,0,0,0},
            {0,4,0,0,0},
        }, true)
        A = A + 0.1*matrix.rand(4,5)
        local U, S, VT, i = A:svd()
        local S2 = matrix.zeros(4,5)
        for i = 1, 4 do S2:set(i,i, S(i)) end
        assert_true(A:equal(U*S2*VT, tol))
    end
end

function test_qr_real()
    for i = 1, 20 do
        local m, n = math.random(1, 100), math.random(1,100)
        local A    = matrix.rand(m, n)
        local Q,R  = A:qr()
        assert_true( matrix.eye(m,n):equal( Q*Q:t(), tol ) )
        assert_true( A:equal(Q*R), tol)
    end
end

function test_qr_real()
    for i = 1, 20 do
        local m   = math.random(1, 100)
        local n   = math.random(1, m)
        local A   = matrix.rand(m, n)
        local Q,R = A:qr()
        assert_true( A:equal(Q*R), tol)
    end
end

function test_qr_complex()
    for i = 1, 20 do
        local m   = math.random(1, 100)
        local n   = math.random(1, m)
        local A   = matrix.rand(m, n, true)
        local Q,R = A:qr()
        assert_true( A:equal(Q*R, tol) )
    end
end

function test_lu_real()
    for i = 1, 20 do
        local m     = math.random(1, 100)
        local n     = math.random(1, 100)
        local A     = matrix.rand(m, n)
        local L,U,P = A:lu()
        assert_true( A:equal(P:solve(L)*U, tol) )
    end
end

function test_lu_complex()
    for i = 1, 20 do
        local m     = math.random(1, 100)
        local n     = math.random(1, 100)
        local A     = matrix.rand(m, n, true)
        local L,U,P = A:lu()
        assert_true ( L:iscomplex() )
        assert_true ( U:iscomplex() )
        assert_false( P:iscomplex() )
        assert_true ( A:equal(P:solve(L)*U, tol) )
    end
end

function test_det_real()
    local A = matrix.fromtable{
        {0.51053, 0.82927, 0.57484},
        {0.57913, 0.68499, 0.28574},
        {0.19752, 0.28155, 0.20787}
    }
    
    local B = matrix.fromtable{
        {0.080600, 0.778555, 0.934582, 0.523292},
        {0.323165, 0.689882, 0.436553, 0.983529},
        {0.783472, 0.543841, 0.081253, 0.061624},
        {0.124055, 0.908652, 0.175192, 0.297689}
    }
    
    local C = matrix.fromtable{
        {0.696283, 0.231452, 0.930534, 0.511339, 0.238509, 0.839269},
        {0.306105, 0.789216, 0.857732, 0.045985, 0.280428, 0.017391},
        {0.100922, 0.182498, 0.060621, 0.958858, 0.767226, 0.447990},
        {0.877147, 0.410187, 0.585583, 0.942881, 0.170096, 0.353872},
        {0.685323, 0.106897, 0.786401, 0.658066, 0.013379, 0.591616},
        {0.084908, 0.864794, 0.595514, 0.062021, 0.987110, 0.343032}
    }
    
    assert_equal(-0.0054523, A:det(), 1e-5)
    assert_equal(0.34860   , B:det(), 1e-5)
    assert_equal(0.0073243 , C:det(), 1e-5)
end

function test_det_complex()
    local A = matrix.fromtable{
        {0.696283 + 0.640396i, 0.231452 + 0.764166i, 0.930534 + 0.593339i, 0.511339 + 0.931422i, 0.238509 + 0.865506i, 0.839269 + 0.334814i},
        {0.306105 + 0.697530i, 0.789216 + 0.825545i, 0.857732 + 0.137181i, 0.045985 + 0.098657i, 0.280428 + 0.274938i, 0.017391 + 0.723746i},
        {0.100922 + 0.951772i, 0.182498 + 0.355689i, 0.060621 + 0.242155i, 0.958858 + 0.857247i, 0.767226 + 0.480821i, 0.447990 + 0.775297i},
        {0.877147 + 0.079859i, 0.410187 + 0.267398i, 0.585583 + 0.380798i, 0.942881 + 0.899654i, 0.170096 + 0.411696i, 0.353872 + 0.832594i},
        {0.685323 + 0.320093i, 0.106897 + 0.229024i, 0.786401 + 0.954347i, 0.658066 + 0.411147i, 0.013379 + 0.189981i, 0.591616 + 0.906750i},
        {0.084908 + 0.102617i, 0.864794 + 0.930756i, 0.595514 + 0.203229i, 0.062021 + 0.524324i, 0.987110 + 0.005907i, 0.343032 + 0.841519i}
    }
    
    assert_true( (1.43095-0.34148i):equal(A:det(), 1e-5) )
end

function test_norm()
    assert_equal(7 , matrix.fromtable{1,4,4,4}:norm()         , tol)
    assert_equal(10, matrix.fromtable{1,4,9,1,1}:norm()       , tol)
    assert_equal(20.9996803785201, matrix.fromtable{3+4i,5-19i,5.476}:norm(), tol)
end

function test_proj()
    local A = matrix.fromtable{2, 2}
    local B = matrix.fromtable{2, 0}
    assert_true( A:proj(B):equal(B, tol) )
    assert_true( B:proj(A):equal(matrix.fromtable{1,1}, tol) )
end

function test_rand()
    local A = matrix.rand(5,8)
    assert_equal(5, A.m)
    assert_equal(8, A.n)
    assert_false(A:iscomplex())
    
    local B = matrix.rand(6, 2, true)
    assert_equal(6, B.m)
    assert_equal(2, B.n)
    assert_true(B:iscomplex())
end

function test_zeros()
    local A = matrix.zeros(4, 3)
    assert_equal(4, A.m)
    assert_equal(3, A.n)
    assert_true(A:equal(matrix.fromtable{
        {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}
    }))
    assert_false( A:iscomplex() )
    
    local B = matrix.zeros(3, 4, true)
    assert_equal(3, B.m)
    assert_equal(4, B.n)
    assert_true(B:equal(matrix.fromtable{
        {0i, 0i, 0i, 0i}, {0i, 0i, 0i, 0i}, {0i, 0i, 0i, 0i}
    }))
    assert_true( B:iscomplex() )
end

function test_ones()
    local A = matrix.ones(4, 3)
    assert_equal(4, A.m)
    assert_equal(3, A.n)
    assert_true(A:equal(matrix.fromtable{
        {1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}
    }))
    assert_false( A:iscomplex() )
    
    local B = matrix.ones(3, 4, true)
    assert_equal(3, B.m)
    assert_equal(4, B.n)
    assert_true(B:equal(matrix.fromtable{
        {1+0i, 1+0i, 1+0i, 1+0i}, {1+0i, 1+0i, 1+0i, 1+0i}, {1+0i, 1+0i, 1+0i, 1+0i}
    }))
    assert_true( B:iscomplex() )
end

-- a more useful test of trace() is contained in test_eig()
function test_trace_simple()
    local A = matrix.fromtable{
        {1, 2, 3, 4}, {-1, -1, -1, -1}, {5, 10, 20, 30}, {5, 3, 2, 1}
    }
    assert_equal(21, A:trace() )
end

function test_eig()
    for i = 1, 20 do
        local s = math.random(1, 100)
        local A = matrix.rand(s,s)
        local w, V = A:eig()
        assert_equal(A:trace(), complex.abs(w:sum()), tol)
    end
end

function test_pinv()
    local A = matrix.fromtable{
        {1, 2, 3},
        {4, 5, 6}
    }
    
    local p = A:pinv()
    assert_true( (A*p):equal( matrix.eye(2,2), tol ) )
    
    local B = matrix.fromtable{
        {5, 8},
        {3,-5}
    }
    assert_true( B:pinv():equal(B:inv(), tol) )
end

function test_ls()
    local A = matrix.fromtable{
        {1,2}, {1,3}, {9,4}
    }
    
    local b = matrix.fromtable{
        {1,-1}, {2,-2}, {3,-3}
    }
    
    local x = A:ls(b)
    
    -- known case: octave
    assert_true(x:equal(matrix.fromtable{
        {0.068871, -0.068871},
        {0.592287, -0.592287},
    }, 1e-5))
    
    -- test with the pseudoinverse
    local x2 = A:pinv()*b
    assert_true(x2:equal(x), tol)
    
    -- test for complex matrices
    A = A -1i
    b = b +2i
    local x3 = A:ls(b)
    
    -- known case: octave
    assert_true(x3:equal(matrix.fromtable{
        {0.128079 - 0.162562i,  -0.039409 - 0.083744i},
        {0.251232 + 0.965517i,  -0.763547 + 0.492611i}
    }, 1e-5))
end

function test_diag()
    local A  = matrix.zeros(3,4)
    A:diag(0):fill(3)
    A:diag(1):fill(4)
    A:diag(2):fill(5)
    A:diag(3):fill(6)
    assert_true(A:equal(matrix.fromtable{
        {3, 4, 5, 6},
        {0, 3, 4, 5},
        {0, 0, 3, 4}
    }) )
end

function test_rank()
    local A = matrix.fromtable{ {1,2,3}, {4,5,6}, {7,8,9} }
    assert_equal(2, A:rank())
    
    local B = matrix.fromtable{ {1,2}, {2,4} }
    assert_equal(1, B:rank())
    
    local C = matrix.fromtable{ {1,2,3}, {4,5,6}, {3,3,3} }
    assert_equal(2, C:rank())
    
    local D = matrix.fromtable{
        {1,-1,2,7},
        {9,2,3,3},
        {2,2,2,2},
        {9,8,7,6}
    }
    assert_equal(4, D:rank())
end

function test_pmax_pmin()
    local A = matrix.fromtable{
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9},
        {1, 2, 3}
    }
    local B = matrix.fromtable{
        {3, 2, 1},
        {5, 0, -1},
        {1, 1, 1},
        {3, 0, 9}
    }
    
    assert_true(A:pmax(B):equal(matrix.fromtable{
        {3, 2, 3},
        {5, 5, 6},
        {7, 8, 9},
        {3, 2, 9}
    }))
    
    assert_true(A:pmin(B):equal(matrix.fromtable{
        {1, 2, 1},
        {4, 0, -1},
        {1, 1, 1},
        {1, 0, 3}
    }))
    
    local C = matrix.fromtable{4,3,2,1}
    assert_true( C:pmax(2):equal(matrix.fromtable{4,3,2,2}) )
    
    local D = matrix.fromtable{5+2i, 0+20i, 4-2i}
    assert_true( D:pmax(8):equal(matrix.fromtable{8+0i, 0+20i, 8} ) )
    assert_true( D:pmax(-999):iscomplex() )
end

function test_lt_gt_real()
    local A = matrix.fromtable{
        {1, 2, 3},
        {4, 5, 6},
    }
    local B = matrix.fromtable{
        {5, 3, 3},
        {4, 7, 8}
    }
    
    assert_true(A:lt(B):equal(matrix.fromtable{
        {1, 1, 0},
        {0, 1, 1},
    }) )
    
    assert_true(B:lt(A):equal(matrix.fromtable{
        {0, 0, 0},
        {0, 0, 0},
    }) )
    
    assert_true(B:gt(A):equal(matrix.fromtable{
        {1, 1, 0},
        {0, 1, 1},
    }) )
    
    assert_true(B:lt(A):equal(matrix.fromtable{
        {0, 0, 0},
        {0, 0, 0},
    }) )
    
    assert_true(A:lt(4):equal(matrix.fromtable{
        {1, 1, 1},
        {0, 0, 0},
    }) )
    
    assert_true(B:gt(5):equal(matrix.fromtable{
        {0, 0, 0},
        {0, 1, 1},
    }) )
end

function test_lt_gt_complex()
    local A = matrix.fromtable{
        {1+2i, 2-3i, 3+5i},
        {4-1i, 5+10i, 6+5i},
    }
    
    local B = matrix.fromtable{
        {3+2i, 0-3i, 2+5i},
        {4+0i, 5+20i, 6+5i},
    }
    
    assert_true(A:lt(B):equal(matrix.fromtable{
        {1+0i, 0, 0},
        {0, 1, 0}
    }) )
end

function test_xor_real()
    local A = matrix.fromtable{
        {1,0,1,1,0,0},
        {0,1,0,0,1,1},
    }
    
    local B = matrix.fromtable{
        {1,1,1,0,0,1},
        {1,1,1,0,0,1},
    }
    
    assert_true( A:xor(B):equal( matrix.fromtable{
        {0,1,0,1,0,1},
        {1,0,1,0,1,0},
    }) )
    
    assert_true( B:xor(A):equal( matrix.fromtable{
        {0,1,0,1,0,1},
        {1,0,1,0,1,0},
    }) )
end

function test_xor_complex()
    local A = matrix.fromtable{
        {1+1i,0,1,1,0,0},
        {0,1,0,0,1,1},
    }
    
    local B = matrix.fromtable{
        {1+1i,1,1,0,0,1},
        {0+2i,1,1,0,0,1},
    }
    
    assert_true( A:xor(B):equal( matrix.fromtable{
        {0+0i,1,0,1,0,1},
        {1,0,1,0,1,0},
    }) )
    
    assert_true( B:xor(A):equal( matrix.fromtable{
        {0+0i,1,0,1,0,1},
        {1,0,1,0,1,0},
    }) )
end

function test_clip()
    local A = matrix.fromtable{
        {1,2,3,4,5,6},
        {7,8,9,10,11,12},
    }
    
    local B = A:clip(5,7)
    assert_equal(8, A:get(2,2))
    assert_true(B:equal(matrix.fromtable{
        {5,5,5,5,5,6},
        {7,7,7,7,7,7},
    }))
    
    A:clip(-1,-1,true)
    assert_equal(-1, A:get(1,1))
    
    local C = matrix.fromtable{
        {1+20i,2,3,4,5,6},
        {7+2i,8,9,10,11,12},
    }
    
    assert_true(C:clip(2+3i,10-2i):equal(matrix.fromtable{
        {10-2i,2+3i,2+3i,4,5,6},
        {7+2i,8,9,10,10-2i,10-2i},
    }, tol))
end

function test_epow()
    for _, iscomplex in ipairs{false} do
        for i = 1, 100 do
            local m = math.random(1, 50)
            local n = math.random(1, 50)
            local p = math.random(-10, 10)
            
            local A = matrix.rand(m, n, iscomplex)
            local B = matrix.new(m, n, iscomplex)
                
            for i = 1, m do
                for j = 1, n do
                    local v = A:get(i,j) 
                    B:set(i,j, v^p)
                end
            end
            
            assert_true( A:epow(p):equal(B, tol) )
        end
    end
    
end

function test_scalar_op()
    local A = matrix.fromtable{ {1, 2}, {3, 4} }
    local b = 5
    
    assert_true( (A*b):equal(matrix.fromtable{ { 5,10}  , {15,20}   }, tol) )
    assert_true( (A/b):equal(matrix.fromtable{ {0.2,0.4}, {0.6,0.8} }, tol) )
    assert_true( (A+b):equal(matrix.fromtable{ { 6, 7}  , { 8, 9}   }, tol) )
    assert_true( (A-b):equal(matrix.fromtable{ {-4,-3}  , {-2,-1}   }, tol) )

    assert_true( (b*A):equal(matrix.fromtable{ { 5,10}  , {15,20}   }, tol) )
    assert_true( (b/A):equal(matrix.fromtable{ {5,5/2}  , {5/3,5/4} }, tol) )
    assert_true( (b+A):equal(matrix.fromtable{ { 6, 7}  , { 8, 9}   }, tol) )
    assert_true( (b-A):equal(matrix.fromtable{ {4,  3}  , { 2, 1}   }, tol) )
end

function test_totable()
    local A = matrix.fromtable{ {1, 2, 3}, {4, 5, 6} }
    assert_true( matrix.fromtable(A:totable()):equal(A, tol) )

    local B = matrix.fromtable{ {5+2i, 2-5i, 3}, {4-2i, 5, 6} }
    assert_true( matrix.fromtable(B:totable()):equal(B, tol) )

    local C = matrix.fromtable{1,2,3,4,5}
    assert_true( matrix.fromtable(C:totable()):equal(C, tol) )

    local D = matrix.fromtable{1}
    assert_true( matrix.fromtable(C:totable()):equal(C, tol) )
end

function test_load()
    local TMPFILE = os.tmpname()
    local f = io.open(TMPFILE, 'w')
    f:write([[
col1 col2 col3
1, 2, 3
4, 5, 6
]])
    f:close()
       
    local a = matrix.load(TMPFILE, ',', 1)
    assert_true(a:equal(matrix.fromtable{ {1, 2, 3}, {4, 5, 6} }, tol))
       
    f = io.open(TMPFILE, 'w')
    f:write([[
1+2i | 2 | 3
4 | 5 | 6-3i
1 | 0 | -2+99i
]])
    f:close()
       
    local b = matrix.load(TMPFILE, '|')
    assert_true(b:equal(matrix.fromtable{ {1+2i, 2, 3}, {4, 5, 6-3i}, {1, 0, -2+99i} }, tol))
    
    f = io.open(TMPFILE, 'w')
    f:write([[
1+2i , 2 , 3
4 , 5 , 6-3i
1 , 0 , -2+99i
]])
    f:close()
       
    local c = matrix.load(TMPFILE, ';')
    assert_false(c:equal(matrix.fromtable{ {1+2i, 2, 3}, {4, 5, 6-3i}, {1, 0, -2+99i} }, tol))

    local d = matrix.load(TMPFILE, ',')
    assert_true(d:equal(matrix.fromtable{ {1+2i, 2, 3}, {4, 5, 6-3i}, {1, 0, -2+99i} }, tol))
    d:save(TMPFILE, ';')
    
    local e = matrix.load(TMPFILE, ';')
    assert_true(e:equal(matrix.fromtable{ {1+2i, 2, 3}, {4, 5, 6-3i}, {1, 0, -2+99i} }, tol))
    
end
