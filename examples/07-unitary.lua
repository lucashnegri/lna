--[[
    Unitary DFT. Here, the DFT is expressed as a Vandermonde matrix.
--]]

local matrix  = require('lna.matrix')
local complex = require('lna.complex')

-- input signal x
local N      = 20
local sin, w = matrix.sin, math.pi*2
local t      = matrix.linspace(0,2,N)
local x      = sin(w*15*t) + 3*sin(w*25*t) + 2*sin(w*40*t)

-- DFT matrix F
local F = matrix.new(N, N, true)
local w = complex.exp(-2i*math.pi/N)

F:foreach(function(F,i,j) 
    F:set(i,j, (w^( (i-1) * (j-1) )) / math.sqrt(N) ) -- normalized
end)

-- transform it
local X = F*x

-- get the signal back
local nx = F:t(true)*X

-- due to the higher number of operations [O(N²)], it results in a higher error than
-- when using the fft
print('error:', (x - nx):abs():sum())
