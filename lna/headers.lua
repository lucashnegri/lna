return [[
typedef int (*LAPACK_D_SELECT2) ( const double*, const double* );
typedef int (*LAPACK_D_SELECT3) ( const double*, const double*, const double* );
typedef int (*LAPACK_Z_SELECT1) ( const double _Complex* );
typedef int (*LAPACK_Z_SELECT2) ( const double _Complex*, const double _Complex* );
int LAPACKE_dbdsdc( int matrix_order, char uplo, char compq,
                           int n, double* d, double* e, double* u,
                           int ldu, double* vt, int ldvt,
                           double* q, int* iq );
int LAPACKE_dbdsqr( int matrix_order, char uplo, int n,
                           int ncvt, int nru, int ncc,
                           double* d, double* e, double* vt, int ldvt,
                           double* u, int ldu, double* c,
                           int ldc );
int LAPACKE_zbdsqr( int matrix_order, char uplo, int n,
                           int ncvt, int nru, int ncc,
                           double* d, double* e, double _Complex* vt,
                           int ldvt, double _Complex* u,
                           int ldu, double _Complex* c,
                           int ldc );
int LAPACKE_ddisna( char job, int m, int n,
                           const double* d, double* sep );
int LAPACKE_dgbbrd( int matrix_order, char vect, int m,
                           int n, int ncc, int kl,
                           int ku, double* ab, int ldab,
                           double* d, double* e, double* q, int ldq,
                           double* pt, int ldpt, double* c,
                           int ldc );
int LAPACKE_zgbbrd( int matrix_order, char vect, int m,
                           int n, int ncc, int kl,
                           int ku, double _Complex* ab,
                           int ldab, double* d, double* e,
                           double _Complex* q, int ldq,
                           double _Complex* pt, int ldpt,
                           double _Complex* c, int ldc );
int LAPACKE_dgbcon( int matrix_order, char norm, int n,
                           int kl, int ku, const double* ab,
                           int ldab, const int* ipiv,
                           double anorm, double* rcond );
int LAPACKE_zgbcon( int matrix_order, char norm, int n,
                           int kl, int ku,
                           const double _Complex* ab, int ldab,
                           const int* ipiv, double anorm,
                           double* rcond );
int LAPACKE_dgbequ( int matrix_order, int m, int n,
                           int kl, int ku, const double* ab,
                           int ldab, double* r, double* c,
                           double* rowcnd, double* colcnd, double* amax );
int LAPACKE_zgbequ( int matrix_order, int m, int n,
                           int kl, int ku,
                           const double _Complex* ab, int ldab,
                           double* r, double* c, double* rowcnd, double* colcnd,
                           double* amax );
int LAPACKE_dgbequb( int matrix_order, int m, int n,
                            int kl, int ku, const double* ab,
                            int ldab, double* r, double* c,
                            double* rowcnd, double* colcnd, double* amax );
int LAPACKE_zgbequb( int matrix_order, int m, int n,
                            int kl, int ku,
                            const double _Complex* ab, int ldab,
                            double* r, double* c, double* rowcnd,
                            double* colcnd, double* amax );
int LAPACKE_dgbrfs( int matrix_order, char trans, int n,
                           int kl, int ku, int nrhs,
                           const double* ab, int ldab, const double* afb,
                           int ldafb, const int* ipiv,
                           const double* b, int ldb, double* x,
                           int ldx, double* ferr, double* berr );
int LAPACKE_zgbrfs( int matrix_order, char trans, int n,
                           int kl, int ku, int nrhs,
                           const double _Complex* ab, int ldab,
                           const double _Complex* afb, int ldafb,
                           const int* ipiv,
                           const double _Complex* b, int ldb,
                           double _Complex* x, int ldx,
                           double* ferr, double* berr );
int LAPACKE_dgbrfsx( int matrix_order, char trans, char equed,
                            int n, int kl, int ku,
                            int nrhs, const double* ab, int ldab,
                            const double* afb, int ldafb,
                            const int* ipiv, const double* r,
                            const double* c, const double* b, int ldb,
                            double* x, int ldx, double* rcond,
                            double* berr, int n_err_bnds,
                            double* err_bnds_norm, double* err_bnds_comp,
                            int nparams, double* params );
int LAPACKE_zgbrfsx( int matrix_order, char trans, char equed,
                            int n, int kl, int ku,
                            int nrhs, const double _Complex* ab,
                            int ldab, const double _Complex* afb,
                            int ldafb, const int* ipiv,
                            const double* r, const double* c,
                            const double _Complex* b, int ldb,
                            double _Complex* x, int ldx,
                            double* rcond, double* berr, int n_err_bnds,
                            double* err_bnds_norm, double* err_bnds_comp,
                            int nparams, double* params );
int LAPACKE_dgbsv( int matrix_order, int n, int kl,
                          int ku, int nrhs, double* ab,
                          int ldab, int* ipiv, double* b,
                          int ldb );
int LAPACKE_zgbsv( int matrix_order, int n, int kl,
                          int ku, int nrhs,
                          double _Complex* ab, int ldab,
                          int* ipiv, double _Complex* b,
                          int ldb );
int LAPACKE_dgbsvx( int matrix_order, char fact, char trans,
                           int n, int kl, int ku,
                           int nrhs, double* ab, int ldab,
                           double* afb, int ldafb, int* ipiv,
                           char* equed, double* r, double* c, double* b,
                           int ldb, double* x, int ldx,
                           double* rcond, double* ferr, double* berr,
                           double* rpivot );
int LAPACKE_zgbsvx( int matrix_order, char fact, char trans,
                           int n, int kl, int ku,
                           int nrhs, double _Complex* ab,
                           int ldab, double _Complex* afb,
                           int ldafb, int* ipiv, char* equed,
                           double* r, double* c, double _Complex* b,
                           int ldb, double _Complex* x,
                           int ldx, double* rcond, double* ferr,
                           double* berr, double* rpivot );
int LAPACKE_dgbsvxx( int matrix_order, char fact, char trans,
                            int n, int kl, int ku,
                            int nrhs, double* ab, int ldab,
                            double* afb, int ldafb, int* ipiv,
                            char* equed, double* r, double* c, double* b,
                            int ldb, double* x, int ldx,
                            double* rcond, double* rpvgrw, double* berr,
                            int n_err_bnds, double* err_bnds_norm,
                            double* err_bnds_comp, int nparams,
                            double* params );
int LAPACKE_zgbsvxx( int matrix_order, char fact, char trans,
                            int n, int kl, int ku,
                            int nrhs, double _Complex* ab,
                            int ldab, double _Complex* afb,
                            int ldafb, int* ipiv, char* equed,
                            double* r, double* c, double _Complex* b,
                            int ldb, double _Complex* x,
                            int ldx, double* rcond, double* rpvgrw,
                            double* berr, int n_err_bnds,
                            double* err_bnds_norm, double* err_bnds_comp,
                            int nparams, double* params );
int LAPACKE_dgbtrf( int matrix_order, int m, int n,
                           int kl, int ku, double* ab,
                           int ldab, int* ipiv );
int LAPACKE_zgbtrf( int matrix_order, int m, int n,
                           int kl, int ku,
                           double _Complex* ab, int ldab,
                           int* ipiv );
int LAPACKE_dgbtrs( int matrix_order, char trans, int n,
                           int kl, int ku, int nrhs,
                           const double* ab, int ldab,
                           const int* ipiv, double* b, int ldb );
int LAPACKE_zgbtrs( int matrix_order, char trans, int n,
                           int kl, int ku, int nrhs,
                           const double _Complex* ab, int ldab,
                           const int* ipiv, double _Complex* b,
                           int ldb );
int LAPACKE_dgebak( int matrix_order, char job, char side, int n,
                           int ilo, int ihi, const double* scale,
                           int m, double* v, int ldv );
int LAPACKE_zgebak( int matrix_order, char job, char side, int n,
                           int ilo, int ihi, const double* scale,
                           int m, double _Complex* v,
                           int ldv );
int LAPACKE_dgebal( int matrix_order, char job, int n, double* a,
                           int lda, int* ilo, int* ihi,
                           double* scale );
int LAPACKE_zgebal( int matrix_order, char job, int n,
                           double _Complex* a, int lda,
                           int* ilo, int* ihi, double* scale );
int LAPACKE_dgebrd( int matrix_order, int m, int n,
                           double* a, int lda, double* d, double* e,
                           double* tauq, double* taup );
int LAPACKE_zgebrd( int matrix_order, int m, int n,
                           double _Complex* a, int lda, double* d,
                           double* e, double _Complex* tauq,
                           double _Complex* taup );
int LAPACKE_dgecon( int matrix_order, char norm, int n,
                           const double* a, int lda, double anorm,
                           double* rcond );
int LAPACKE_zgecon( int matrix_order, char norm, int n,
                           const double _Complex* a, int lda,
                           double anorm, double* rcond );
int LAPACKE_dgeequ( int matrix_order, int m, int n,
                           const double* a, int lda, double* r,
                           double* c, double* rowcnd, double* colcnd,
                           double* amax );
int LAPACKE_zgeequ( int matrix_order, int m, int n,
                           const double _Complex* a, int lda,
                           double* r, double* c, double* rowcnd, double* colcnd,
                           double* amax );
int LAPACKE_dgeequb( int matrix_order, int m, int n,
                            const double* a, int lda, double* r,
                            double* c, double* rowcnd, double* colcnd,
                            double* amax );
int LAPACKE_zgeequb( int matrix_order, int m, int n,
                            const double _Complex* a, int lda,
                            double* r, double* c, double* rowcnd,
                            double* colcnd, double* amax );
int LAPACKE_dgees( int matrix_order, char jobvs, char sort,
                          LAPACK_D_SELECT2 select, int n, double* a,
                          int lda, int* sdim, double* wr,
                          double* wi, double* vs, int ldvs );
int LAPACKE_zgees( int matrix_order, char jobvs, char sort,
                          LAPACK_Z_SELECT1 select, int n,
                          double _Complex* a, int lda,
                          int* sdim, double _Complex* w,
                          double _Complex* vs, int ldvs );
int LAPACKE_dgeesx( int matrix_order, char jobvs, char sort,
                           LAPACK_D_SELECT2 select, char sense, int n,
                           double* a, int lda, int* sdim,
                           double* wr, double* wi, double* vs, int ldvs,
                           double* rconde, double* rcondv );
int LAPACKE_zgeesx( int matrix_order, char jobvs, char sort,
                           LAPACK_Z_SELECT1 select, char sense, int n,
                           double _Complex* a, int lda,
                           int* sdim, double _Complex* w,
                           double _Complex* vs, int ldvs,
                           double* rconde, double* rcondv );
int LAPACKE_dgeev( int matrix_order, char jobvl, char jobvr,
                          int n, double* a, int lda, double* wr,
                          double* wi, double* vl, int ldvl, double* vr,
                          int ldvr );
int LAPACKE_zgeev( int matrix_order, char jobvl, char jobvr,
                          int n, double _Complex* a,
                          int lda, double _Complex* w,
                          double _Complex* vl, int ldvl,
                          double _Complex* vr, int ldvr );
int LAPACKE_dgeevx( int matrix_order, char balanc, char jobvl,
                           char jobvr, char sense, int n, double* a,
                           int lda, double* wr, double* wi, double* vl,
                           int ldvl, double* vr, int ldvr,
                           int* ilo, int* ihi, double* scale,
                           double* abnrm, double* rconde, double* rcondv );
int LAPACKE_zgeevx( int matrix_order, char balanc, char jobvl,
                           char jobvr, char sense, int n,
                           double _Complex* a, int lda,
                           double _Complex* w, double _Complex* vl,
                           int ldvl, double _Complex* vr,
                           int ldvr, int* ilo, int* ihi,
                           double* scale, double* abnrm, double* rconde,
                           double* rcondv );
int LAPACKE_dgehrd( int matrix_order, int n, int ilo,
                           int ihi, double* a, int lda,
                           double* tau );
int LAPACKE_zgehrd( int matrix_order, int n, int ilo,
                           int ihi, double _Complex* a,
                           int lda, double _Complex* tau );
int LAPACKE_dgejsv( int matrix_order, char joba, char jobu, char jobv,
                           char jobr, char jobt, char jobp, int m,
                           int n, double* a, int lda, double* sva,
                           double* u, int ldu, double* v, int ldv,
                           double* stat, int* istat );
int LAPACKE_dgelqf( int matrix_order, int m, int n,
                           double* a, int lda, double* tau );
int LAPACKE_zgelqf( int matrix_order, int m, int n,
                           double _Complex* a, int lda,
                           double _Complex* tau );
int LAPACKE_dgels( int matrix_order, char trans, int m,
                          int n, int nrhs, double* a,
                          int lda, double* b, int ldb );
int LAPACKE_zgels( int matrix_order, char trans, int m,
                          int n, int nrhs,
                          double _Complex* a, int lda,
                          double _Complex* b, int ldb );
int LAPACKE_dgelsd( int matrix_order, int m, int n,
                           int nrhs, double* a, int lda,
                           double* b, int ldb, double* s, double rcond,
                           int* rank );
int LAPACKE_zgelsd( int matrix_order, int m, int n,
                           int nrhs, double _Complex* a,
                           int lda, double _Complex* b,
                           int ldb, double* s, double rcond,
                           int* rank );
int LAPACKE_dgelss( int matrix_order, int m, int n,
                           int nrhs, double* a, int lda,
                           double* b, int ldb, double* s, double rcond,
                           int* rank );
int LAPACKE_zgelss( int matrix_order, int m, int n,
                           int nrhs, double _Complex* a,
                           int lda, double _Complex* b,
                           int ldb, double* s, double rcond,
                           int* rank );
int LAPACKE_dgelsy( int matrix_order, int m, int n,
                           int nrhs, double* a, int lda,
                           double* b, int ldb, int* jpvt,
                           double rcond, int* rank );
int LAPACKE_zgelsy( int matrix_order, int m, int n,
                           int nrhs, double _Complex* a,
                           int lda, double _Complex* b,
                           int ldb, int* jpvt, double rcond,
                           int* rank );
int LAPACKE_dgeqlf( int matrix_order, int m, int n,
                           double* a, int lda, double* tau );
int LAPACKE_zgeqlf( int matrix_order, int m, int n,
                           double _Complex* a, int lda,
                           double _Complex* tau );
int LAPACKE_dgeqp3( int matrix_order, int m, int n,
                           double* a, int lda, int* jpvt,
                           double* tau );
int LAPACKE_zgeqp3( int matrix_order, int m, int n,
                           double _Complex* a, int lda,
                           int* jpvt, double _Complex* tau );
int LAPACKE_dgeqpf( int matrix_order, int m, int n,
                           double* a, int lda, int* jpvt,
                           double* tau );
int LAPACKE_zgeqpf( int matrix_order, int m, int n,
                           double _Complex* a, int lda,
                           int* jpvt, double _Complex* tau );
int LAPACKE_dgeqrf( int matrix_order, int m, int n,
                           double* a, int lda, double* tau );
int LAPACKE_zgeqrf( int matrix_order, int m, int n,
                           double _Complex* a, int lda,
                           double _Complex* tau );
int LAPACKE_dgeqrfp( int matrix_order, int m, int n,
                            double* a, int lda, double* tau );
int LAPACKE_zgeqrfp( int matrix_order, int m, int n,
                            double _Complex* a, int lda,
                            double _Complex* tau );
int LAPACKE_dgerfs( int matrix_order, char trans, int n,
                           int nrhs, const double* a, int lda,
                           const double* af, int ldaf,
                           const int* ipiv, const double* b,
                           int ldb, double* x, int ldx,
                           double* ferr, double* berr );
int LAPACKE_zgerfs( int matrix_order, char trans, int n,
                           int nrhs, const double _Complex* a,
                           int lda, const double _Complex* af,
                           int ldaf, const int* ipiv,
                           const double _Complex* b, int ldb,
                           double _Complex* x, int ldx,
                           double* ferr, double* berr );
int LAPACKE_dgerfsx( int matrix_order, char trans, char equed,
                            int n, int nrhs, const double* a,
                            int lda, const double* af, int ldaf,
                            const int* ipiv, const double* r,
                            const double* c, const double* b, int ldb,
                            double* x, int ldx, double* rcond,
                            double* berr, int n_err_bnds,
                            double* err_bnds_norm, double* err_bnds_comp,
                            int nparams, double* params );
int LAPACKE_zgerfsx( int matrix_order, char trans, char equed,
                            int n, int nrhs,
                            const double _Complex* a, int lda,
                            const double _Complex* af, int ldaf,
                            const int* ipiv, const double* r,
                            const double* c, const double _Complex* b,
                            int ldb, double _Complex* x,
                            int ldx, double* rcond, double* berr,
                            int n_err_bnds, double* err_bnds_norm,
                            double* err_bnds_comp, int nparams,
                            double* params );
int LAPACKE_dgerqf( int matrix_order, int m, int n,
                           double* a, int lda, double* tau );
int LAPACKE_zgerqf( int matrix_order, int m, int n,
                           double _Complex* a, int lda,
                           double _Complex* tau );
int LAPACKE_dgesdd( int matrix_order, char jobz, int m,
                           int n, double* a, int lda, double* s,
                           double* u, int ldu, double* vt,
                           int ldvt );
int LAPACKE_zgesdd( int matrix_order, char jobz, int m,
                           int n, double _Complex* a,
                           int lda, double* s, double _Complex* u,
                           int ldu, double _Complex* vt,
                           int ldvt );
int LAPACKE_dgesv( int matrix_order, int n, int nrhs,
                          double* a, int lda, int* ipiv,
                          double* b, int ldb );
int LAPACKE_zgesv( int matrix_order, int n, int nrhs,
                          double _Complex* a, int lda,
                          int* ipiv, double _Complex* b,
                          int ldb );
int LAPACKE_dsgesv( int matrix_order, int n, int nrhs,
                           double* a, int lda, int* ipiv,
                           double* b, int ldb, double* x, int ldx,
                           int* iter );
int LAPACKE_zcgesv( int matrix_order, int n, int nrhs,
                           double _Complex* a, int lda,
                           int* ipiv, double _Complex* b,
                           int ldb, double _Complex* x,
                           int ldx, int* iter );
int LAPACKE_dgesvd( int matrix_order, char jobu, char jobvt,
                           int m, int n, double* a,
                           int lda, double* s, double* u, int ldu,
                           double* vt, int ldvt, double* superb );
int LAPACKE_zgesvd( int matrix_order, char jobu, char jobvt,
                           int m, int n, double _Complex* a,
                           int lda, double* s, double _Complex* u,
                           int ldu, double _Complex* vt,
                           int ldvt, double* superb );
int LAPACKE_dgesvj( int matrix_order, char joba, char jobu, char jobv,
                           int m, int n, double* a,
                           int lda, double* sva, int mv,
                           double* v, int ldv, double* stat );
int LAPACKE_dgesvx( int matrix_order, char fact, char trans,
                           int n, int nrhs, double* a,
                           int lda, double* af, int ldaf,
                           int* ipiv, char* equed, double* r, double* c,
                           double* b, int ldb, double* x, int ldx,
                           double* rcond, double* ferr, double* berr,
                           double* rpivot );
int LAPACKE_zgesvx( int matrix_order, char fact, char trans,
                           int n, int nrhs,
                           double _Complex* a, int lda,
                           double _Complex* af, int ldaf,
                           int* ipiv, char* equed, double* r, double* c,
                           double _Complex* b, int ldb,
                           double _Complex* x, int ldx,
                           double* rcond, double* ferr, double* berr,
                           double* rpivot );
int LAPACKE_dgesvxx( int matrix_order, char fact, char trans,
                            int n, int nrhs, double* a,
                            int lda, double* af, int ldaf,
                            int* ipiv, char* equed, double* r, double* c,
                            double* b, int ldb, double* x,
                            int ldx, double* rcond, double* rpvgrw,
                            double* berr, int n_err_bnds,
                            double* err_bnds_norm, double* err_bnds_comp,
                            int nparams, double* params );
int LAPACKE_zgesvxx( int matrix_order, char fact, char trans,
                            int n, int nrhs,
                            double _Complex* a, int lda,
                            double _Complex* af, int ldaf,
                            int* ipiv, char* equed, double* r, double* c,
                            double _Complex* b, int ldb,
                            double _Complex* x, int ldx,
                            double* rcond, double* rpvgrw, double* berr,
                            int n_err_bnds, double* err_bnds_norm,
                            double* err_bnds_comp, int nparams,
                            double* params );
int LAPACKE_dgetrf( int matrix_order, int m, int n,
                           double* a, int lda, int* ipiv );
int LAPACKE_zgetrf( int matrix_order, int m, int n,
                           double _Complex* a, int lda,
                           int* ipiv );
int LAPACKE_dgetri( int matrix_order, int n, double* a,
                           int lda, const int* ipiv );
int LAPACKE_zgetri( int matrix_order, int n,
                           double _Complex* a, int lda,
                           const int* ipiv );
int LAPACKE_dgetrs( int matrix_order, char trans, int n,
                           int nrhs, const double* a, int lda,
                           const int* ipiv, double* b, int ldb );
int LAPACKE_zgetrs( int matrix_order, char trans, int n,
                           int nrhs, const double _Complex* a,
                           int lda, const int* ipiv,
                           double _Complex* b, int ldb );
int LAPACKE_dggbak( int matrix_order, char job, char side, int n,
                           int ilo, int ihi, const double* lscale,
                           const double* rscale, int m, double* v,
                           int ldv );
int LAPACKE_zggbak( int matrix_order, char job, char side, int n,
                           int ilo, int ihi, const double* lscale,
                           const double* rscale, int m,
                           double _Complex* v, int ldv );
int LAPACKE_dggbal( int matrix_order, char job, int n, double* a,
                           int lda, double* b, int ldb,
                           int* ilo, int* ihi, double* lscale,
                           double* rscale );
int LAPACKE_zggbal( int matrix_order, char job, int n,
                           double _Complex* a, int lda,
                           double _Complex* b, int ldb,
                           int* ilo, int* ihi, double* lscale,
                           double* rscale );
int LAPACKE_dgges( int matrix_order, char jobvsl, char jobvsr, char sort,
                          LAPACK_D_SELECT3 selctg, int n, double* a,
                          int lda, double* b, int ldb,
                          int* sdim, double* alphar, double* alphai,
                          double* beta, double* vsl, int ldvsl,
                          double* vsr, int ldvsr );
int LAPACKE_zgges( int matrix_order, char jobvsl, char jobvsr, char sort,
                          LAPACK_Z_SELECT2 selctg, int n,
                          double _Complex* a, int lda,
                          double _Complex* b, int ldb,
                          int* sdim, double _Complex* alpha,
                          double _Complex* beta,
                          double _Complex* vsl, int ldvsl,
                          double _Complex* vsr, int ldvsr );
int LAPACKE_dggesx( int matrix_order, char jobvsl, char jobvsr,
                           char sort, LAPACK_D_SELECT3 selctg, char sense,
                           int n, double* a, int lda, double* b,
                           int ldb, int* sdim, double* alphar,
                           double* alphai, double* beta, double* vsl,
                           int ldvsl, double* vsr, int ldvsr,
                           double* rconde, double* rcondv );
int LAPACKE_zggesx( int matrix_order, char jobvsl, char jobvsr,
                           char sort, LAPACK_Z_SELECT2 selctg, char sense,
                           int n, double _Complex* a,
                           int lda, double _Complex* b,
                           int ldb, int* sdim,
                           double _Complex* alpha,
                           double _Complex* beta,
                           double _Complex* vsl, int ldvsl,
                           double _Complex* vsr, int ldvsr,
                           double* rconde, double* rcondv );
int LAPACKE_dggev( int matrix_order, char jobvl, char jobvr,
                          int n, double* a, int lda, double* b,
                          int ldb, double* alphar, double* alphai,
                          double* beta, double* vl, int ldvl, double* vr,
                          int ldvr );
int LAPACKE_zggev( int matrix_order, char jobvl, char jobvr,
                          int n, double _Complex* a,
                          int lda, double _Complex* b,
                          int ldb, double _Complex* alpha,
                          double _Complex* beta,
                          double _Complex* vl, int ldvl,
                          double _Complex* vr, int ldvr );
int LAPACKE_dggevx( int matrix_order, char balanc, char jobvl,
                           char jobvr, char sense, int n, double* a,
                           int lda, double* b, int ldb,
                           double* alphar, double* alphai, double* beta,
                           double* vl, int ldvl, double* vr,
                           int ldvr, int* ilo, int* ihi,
                           double* lscale, double* rscale, double* abnrm,
                           double* bbnrm, double* rconde, double* rcondv );
int LAPACKE_zggevx( int matrix_order, char balanc, char jobvl,
                           char jobvr, char sense, int n,
                           double _Complex* a, int lda,
                           double _Complex* b, int ldb,
                           double _Complex* alpha,
                           double _Complex* beta,
                           double _Complex* vl, int ldvl,
                           double _Complex* vr, int ldvr,
                           int* ilo, int* ihi, double* lscale,
                           double* rscale, double* abnrm, double* bbnrm,
                           double* rconde, double* rcondv );
int LAPACKE_dggglm( int matrix_order, int n, int m,
                           int p, double* a, int lda, double* b,
                           int ldb, double* d, double* x, double* y );
int LAPACKE_zggglm( int matrix_order, int n, int m,
                           int p, double _Complex* a,
                           int lda, double _Complex* b,
                           int ldb, double _Complex* d,
                           double _Complex* x, double _Complex* y );
int LAPACKE_dgghrd( int matrix_order, char compq, char compz,
                           int n, int ilo, int ihi,
                           double* a, int lda, double* b, int ldb,
                           double* q, int ldq, double* z,
                           int ldz );
int LAPACKE_zgghrd( int matrix_order, char compq, char compz,
                           int n, int ilo, int ihi,
                           double _Complex* a, int lda,
                           double _Complex* b, int ldb,
                           double _Complex* q, int ldq,
                           double _Complex* z, int ldz );
int LAPACKE_dgglse( int matrix_order, int m, int n,
                           int p, double* a, int lda, double* b,
                           int ldb, double* c, double* d, double* x );
int LAPACKE_zgglse( int matrix_order, int m, int n,
                           int p, double _Complex* a,
                           int lda, double _Complex* b,
                           int ldb, double _Complex* c,
                           double _Complex* d, double _Complex* x );
int LAPACKE_dggqrf( int matrix_order, int n, int m,
                           int p, double* a, int lda,
                           double* taua, double* b, int ldb,
                           double* taub );
int LAPACKE_zggqrf( int matrix_order, int n, int m,
                           int p, double _Complex* a,
                           int lda, double _Complex* taua,
                           double _Complex* b, int ldb,
                           double _Complex* taub );
int LAPACKE_dggrqf( int matrix_order, int m, int p,
                           int n, double* a, int lda,
                           double* taua, double* b, int ldb,
                           double* taub );
int LAPACKE_zggrqf( int matrix_order, int m, int p,
                           int n, double _Complex* a,
                           int lda, double _Complex* taua,
                           double _Complex* b, int ldb,
                           double _Complex* taub );
int LAPACKE_dggsvd( int matrix_order, char jobu, char jobv, char jobq,
                           int m, int n, int p,
                           int* k, int* l, double* a,
                           int lda, double* b, int ldb,
                           double* alpha, double* beta, double* u,
                           int ldu, double* v, int ldv, double* q,
                           int ldq, int* iwork );
int LAPACKE_zggsvd( int matrix_order, char jobu, char jobv, char jobq,
                           int m, int n, int p,
                           int* k, int* l,
                           double _Complex* a, int lda,
                           double _Complex* b, int ldb,
                           double* alpha, double* beta,
                           double _Complex* u, int ldu,
                           double _Complex* v, int ldv,
                           double _Complex* q, int ldq,
                           int* iwork );
int LAPACKE_dggsvp( int matrix_order, char jobu, char jobv, char jobq,
                           int m, int p, int n, double* a,
                           int lda, double* b, int ldb,
                           double tola, double tolb, int* k,
                           int* l, double* u, int ldu, double* v,
                           int ldv, double* q, int ldq );
int LAPACKE_zggsvp( int matrix_order, char jobu, char jobv, char jobq,
                           int m, int p, int n,
                           double _Complex* a, int lda,
                           double _Complex* b, int ldb,
                           double tola, double tolb, int* k,
                           int* l, double _Complex* u,
                           int ldu, double _Complex* v,
                           int ldv, double _Complex* q,
                           int ldq );
int LAPACKE_dgtcon( char norm, int n, const double* dl,
                           const double* d, const double* du, const double* du2,
                           const int* ipiv, double anorm,
                           double* rcond );
int LAPACKE_zgtcon( char norm, int n,
                           const double _Complex* dl,
                           const double _Complex* d,
                           const double _Complex* du,
                           const double _Complex* du2,
                           const int* ipiv, double anorm,
                           double* rcond );
int LAPACKE_dgtrfs( int matrix_order, char trans, int n,
                           int nrhs, const double* dl, const double* d,
                           const double* du, const double* dlf,
                           const double* df, const double* duf,
                           const double* du2, const int* ipiv,
                           const double* b, int ldb, double* x,
                           int ldx, double* ferr, double* berr );
int LAPACKE_zgtrfs( int matrix_order, char trans, int n,
                           int nrhs, const double _Complex* dl,
                           const double _Complex* d,
                           const double _Complex* du,
                           const double _Complex* dlf,
                           const double _Complex* df,
                           const double _Complex* duf,
                           const double _Complex* du2,
                           const int* ipiv,
                           const double _Complex* b, int ldb,
                           double _Complex* x, int ldx,
                           double* ferr, double* berr );
int LAPACKE_dgtsv( int matrix_order, int n, int nrhs,
                          double* dl, double* d, double* du, double* b,
                          int ldb );
int LAPACKE_zgtsv( int matrix_order, int n, int nrhs,
                          double _Complex* dl, double _Complex* d,
                          double _Complex* du, double _Complex* b,
                          int ldb );
int LAPACKE_dgtsvx( int matrix_order, char fact, char trans,
                           int n, int nrhs, const double* dl,
                           const double* d, const double* du, double* dlf,
                           double* df, double* duf, double* du2,
                           int* ipiv, const double* b, int ldb,
                           double* x, int ldx, double* rcond,
                           double* ferr, double* berr );
int LAPACKE_zgtsvx( int matrix_order, char fact, char trans,
                           int n, int nrhs,
                           const double _Complex* dl,
                           const double _Complex* d,
                           const double _Complex* du,
                           double _Complex* dlf,
                           double _Complex* df,
                           double _Complex* duf,
                           double _Complex* du2, int* ipiv,
                           const double _Complex* b, int ldb,
                           double _Complex* x, int ldx,
                           double* rcond, double* ferr, double* berr );
int LAPACKE_dgttrf( int n, double* dl, double* d, double* du,
                           double* du2, int* ipiv );
int LAPACKE_zgttrf( int n, double _Complex* dl,
                           double _Complex* d, double _Complex* du,
                           double _Complex* du2, int* ipiv );
int LAPACKE_dgttrs( int matrix_order, char trans, int n,
                           int nrhs, const double* dl, const double* d,
                           const double* du, const double* du2,
                           const int* ipiv, double* b, int ldb );
int LAPACKE_zgttrs( int matrix_order, char trans, int n,
                           int nrhs, const double _Complex* dl,
                           const double _Complex* d,
                           const double _Complex* du,
                           const double _Complex* du2,
                           const int* ipiv, double _Complex* b,
                           int ldb );
int LAPACKE_zhbev( int matrix_order, char jobz, char uplo, int n,
                          int kd, double _Complex* ab,
                          int ldab, double* w, double _Complex* z,
                          int ldz );
int LAPACKE_zhbevd( int matrix_order, char jobz, char uplo, int n,
                           int kd, double _Complex* ab,
                           int ldab, double* w, double _Complex* z,
                           int ldz );
int LAPACKE_zhbevx( int matrix_order, char jobz, char range, char uplo,
                           int n, int kd,
                           double _Complex* ab, int ldab,
                           double _Complex* q, int ldq, double vl,
                           double vu, int il, int iu,
                           double abstol, int* m, double* w,
                           double _Complex* z, int ldz,
                           int* ifail );
int LAPACKE_zhbgst( int matrix_order, char vect, char uplo, int n,
                           int ka, int kb,
                           double _Complex* ab, int ldab,
                           const double _Complex* bb, int ldbb,
                           double _Complex* x, int ldx );
int LAPACKE_zhbgv( int matrix_order, char jobz, char uplo, int n,
                          int ka, int kb,
                          double _Complex* ab, int ldab,
                          double _Complex* bb, int ldbb, double* w,
                          double _Complex* z, int ldz );
int LAPACKE_zhbgvd( int matrix_order, char jobz, char uplo, int n,
                           int ka, int kb,
                           double _Complex* ab, int ldab,
                           double _Complex* bb, int ldbb,
                           double* w, double _Complex* z,
                           int ldz );
int LAPACKE_zhbgvx( int matrix_order, char jobz, char range, char uplo,
                           int n, int ka, int kb,
                           double _Complex* ab, int ldab,
                           double _Complex* bb, int ldbb,
                           double _Complex* q, int ldq, double vl,
                           double vu, int il, int iu,
                           double abstol, int* m, double* w,
                           double _Complex* z, int ldz,
                           int* ifail );
int LAPACKE_zhbtrd( int matrix_order, char vect, char uplo, int n,
                           int kd, double _Complex* ab,
                           int ldab, double* d, double* e,
                           double _Complex* q, int ldq );
int LAPACKE_zhecon( int matrix_order, char uplo, int n,
                           const double _Complex* a, int lda,
                           const int* ipiv, double anorm,
                           double* rcond );
int LAPACKE_zheequb( int matrix_order, char uplo, int n,
                            const double _Complex* a, int lda,
                            double* s, double* scond, double* amax );
int LAPACKE_zheev( int matrix_order, char jobz, char uplo, int n,
                          double _Complex* a, int lda, double* w );
int LAPACKE_zheevd( int matrix_order, char jobz, char uplo, int n,
                           double _Complex* a, int lda,
                           double* w );
int LAPACKE_zheevr( int matrix_order, char jobz, char range, char uplo,
                           int n, double _Complex* a,
                           int lda, double vl, double vu, int il,
                           int iu, double abstol, int* m,
                           double* w, double _Complex* z, int ldz,
                           int* isuppz );
int LAPACKE_zheevx( int matrix_order, char jobz, char range, char uplo,
                           int n, double _Complex* a,
                           int lda, double vl, double vu, int il,
                           int iu, double abstol, int* m,
                           double* w, double _Complex* z, int ldz,
                           int* ifail );
int LAPACKE_zhegst( int matrix_order, int itype, char uplo,
                           int n, double _Complex* a,
                           int lda, const double _Complex* b,
                           int ldb );
int LAPACKE_zhegv( int matrix_order, int itype, char jobz,
                          char uplo, int n, double _Complex* a,
                          int lda, double _Complex* b,
                          int ldb, double* w );
int LAPACKE_zhegvd( int matrix_order, int itype, char jobz,
                           char uplo, int n, double _Complex* a,
                           int lda, double _Complex* b,
                           int ldb, double* w );
int LAPACKE_zhegvx( int matrix_order, int itype, char jobz,
                           char range, char uplo, int n,
                           double _Complex* a, int lda,
                           double _Complex* b, int ldb, double vl,
                           double vu, int il, int iu,
                           double abstol, int* m, double* w,
                           double _Complex* z, int ldz,
                           int* ifail );
int LAPACKE_zherfs( int matrix_order, char uplo, int n,
                           int nrhs, const double _Complex* a,
                           int lda, const double _Complex* af,
                           int ldaf, const int* ipiv,
                           const double _Complex* b, int ldb,
                           double _Complex* x, int ldx,
                           double* ferr, double* berr );
int LAPACKE_zherfsx( int matrix_order, char uplo, char equed,
                            int n, int nrhs,
                            const double _Complex* a, int lda,
                            const double _Complex* af, int ldaf,
                            const int* ipiv, const double* s,
                            const double _Complex* b, int ldb,
                            double _Complex* x, int ldx,
                            double* rcond, double* berr, int n_err_bnds,
                            double* err_bnds_norm, double* err_bnds_comp,
                            int nparams, double* params );
int LAPACKE_zhesv( int matrix_order, char uplo, int n,
                          int nrhs, double _Complex* a,
                          int lda, int* ipiv,
                          double _Complex* b, int ldb );
int LAPACKE_zhesvx( int matrix_order, char fact, char uplo, int n,
                           int nrhs, const double _Complex* a,
                           int lda, double _Complex* af,
                           int ldaf, int* ipiv,
                           const double _Complex* b, int ldb,
                           double _Complex* x, int ldx,
                           double* rcond, double* ferr, double* berr );
int LAPACKE_zhesvxx( int matrix_order, char fact, char uplo,
                            int n, int nrhs,
                            double _Complex* a, int lda,
                            double _Complex* af, int ldaf,
                            int* ipiv, char* equed, double* s,
                            double _Complex* b, int ldb,
                            double _Complex* x, int ldx,
                            double* rcond, double* rpvgrw, double* berr,
                            int n_err_bnds, double* err_bnds_norm,
                            double* err_bnds_comp, int nparams,
                            double* params );
int LAPACKE_zhetrd( int matrix_order, char uplo, int n,
                           double _Complex* a, int lda, double* d,
                           double* e, double _Complex* tau );
int LAPACKE_zhetrf( int matrix_order, char uplo, int n,
                           double _Complex* a, int lda,
                           int* ipiv );
int LAPACKE_zhetri( int matrix_order, char uplo, int n,
                           double _Complex* a, int lda,
                           const int* ipiv );
int LAPACKE_zhetrs( int matrix_order, char uplo, int n,
                           int nrhs, const double _Complex* a,
                           int lda, const int* ipiv,
                           double _Complex* b, int ldb );
int LAPACKE_zhfrk( int matrix_order, char transr, char uplo, char trans,
                          int n, int k, double alpha,
                          const double _Complex* a, int lda,
                          double beta, double _Complex* c );
int LAPACKE_dhgeqz( int matrix_order, char job, char compq, char compz,
                           int n, int ilo, int ihi,
                           double* h, int ldh, double* t, int ldt,
                           double* alphar, double* alphai, double* beta,
                           double* q, int ldq, double* z,
                           int ldz );
int LAPACKE_zhgeqz( int matrix_order, char job, char compq, char compz,
                           int n, int ilo, int ihi,
                           double _Complex* h, int ldh,
                           double _Complex* t, int ldt,
                           double _Complex* alpha,
                           double _Complex* beta,
                           double _Complex* q, int ldq,
                           double _Complex* z, int ldz );
int LAPACKE_zhpcon( int matrix_order, char uplo, int n,
                           const double _Complex* ap,
                           const int* ipiv, double anorm,
                           double* rcond );
int LAPACKE_zhpev( int matrix_order, char jobz, char uplo, int n,
                          double _Complex* ap, double* w,
                          double _Complex* z, int ldz );
int LAPACKE_zhpevd( int matrix_order, char jobz, char uplo, int n,
                           double _Complex* ap, double* w,
                           double _Complex* z, int ldz );
int LAPACKE_zhpevx( int matrix_order, char jobz, char range, char uplo,
                           int n, double _Complex* ap, double vl,
                           double vu, int il, int iu,
                           double abstol, int* m, double* w,
                           double _Complex* z, int ldz,
                           int* ifail );
int LAPACKE_zhpgst( int matrix_order, int itype, char uplo,
                           int n, double _Complex* ap,
                           const double _Complex* bp );
int LAPACKE_zhpgv( int matrix_order, int itype, char jobz,
                          char uplo, int n, double _Complex* ap,
                          double _Complex* bp, double* w,
                          double _Complex* z, int ldz );
int LAPACKE_zhpgvd( int matrix_order, int itype, char jobz,
                           char uplo, int n, double _Complex* ap,
                           double _Complex* bp, double* w,
                           double _Complex* z, int ldz );
int LAPACKE_zhpgvx( int matrix_order, int itype, char jobz,
                           char range, char uplo, int n,
                           double _Complex* ap, double _Complex* bp,
                           double vl, double vu, int il, int iu,
                           double abstol, int* m, double* w,
                           double _Complex* z, int ldz,
                           int* ifail );
int LAPACKE_zhprfs( int matrix_order, char uplo, int n,
                           int nrhs, const double _Complex* ap,
                           const double _Complex* afp,
                           const int* ipiv,
                           const double _Complex* b, int ldb,
                           double _Complex* x, int ldx,
                           double* ferr, double* berr );
int LAPACKE_zhpsv( int matrix_order, char uplo, int n,
                          int nrhs, double _Complex* ap,
                          int* ipiv, double _Complex* b,
                          int ldb );
int LAPACKE_zhpsvx( int matrix_order, char fact, char uplo, int n,
                           int nrhs, const double _Complex* ap,
                           double _Complex* afp, int* ipiv,
                           const double _Complex* b, int ldb,
                           double _Complex* x, int ldx,
                           double* rcond, double* ferr, double* berr );
int LAPACKE_zhptrd( int matrix_order, char uplo, int n,
                           double _Complex* ap, double* d, double* e,
                           double _Complex* tau );
int LAPACKE_zhptrf( int matrix_order, char uplo, int n,
                           double _Complex* ap, int* ipiv );
int LAPACKE_zhptri( int matrix_order, char uplo, int n,
                           double _Complex* ap, const int* ipiv );
int LAPACKE_zhptrs( int matrix_order, char uplo, int n,
                           int nrhs, const double _Complex* ap,
                           const int* ipiv, double _Complex* b,
                           int ldb );
int LAPACKE_dhsein( int matrix_order, char job, char eigsrc, char initv,
                           int* select, int n,
                           const double* h, int ldh, double* wr,
                           const double* wi, double* vl, int ldvl,
                           double* vr, int ldvr, int mm,
                           int* m, int* ifaill,
                           int* ifailr );
int LAPACKE_zhsein( int matrix_order, char job, char eigsrc, char initv,
                           const int* select, int n,
                           const double _Complex* h, int ldh,
                           double _Complex* w, double _Complex* vl,
                           int ldvl, double _Complex* vr,
                           int ldvr, int mm, int* m,
                           int* ifaill, int* ifailr );
int LAPACKE_dhseqr( int matrix_order, char job, char compz, int n,
                           int ilo, int ihi, double* h,
                           int ldh, double* wr, double* wi, double* z,
                           int ldz );
int LAPACKE_zhseqr( int matrix_order, char job, char compz, int n,
                           int ilo, int ihi,
                           double _Complex* h, int ldh,
                           double _Complex* w, double _Complex* z,
                           int ldz );
int LAPACKE_dopgtr( int matrix_order, char uplo, int n,
                           const double* ap, const double* tau, double* q,
                           int ldq );
int LAPACKE_dopmtr( int matrix_order, char side, char uplo, char trans,
                           int m, int n, const double* ap,
                           const double* tau, double* c, int ldc );
int LAPACKE_dorgbr( int matrix_order, char vect, int m,
                           int n, int k, double* a,
                           int lda, const double* tau );
int LAPACKE_dorghr( int matrix_order, int n, int ilo,
                           int ihi, double* a, int lda,
                           const double* tau );
int LAPACKE_dorglq( int matrix_order, int m, int n,
                           int k, double* a, int lda,
                           const double* tau );
int LAPACKE_dorgql( int matrix_order, int m, int n,
                           int k, double* a, int lda,
                           const double* tau );
int LAPACKE_dorgqr( int matrix_order, int m, int n,
                           int k, double* a, int lda,
                           const double* tau );
int LAPACKE_dorgrq( int matrix_order, int m, int n,
                           int k, double* a, int lda,
                           const double* tau );
int LAPACKE_dorgtr( int matrix_order, char uplo, int n, double* a,
                           int lda, const double* tau );
int LAPACKE_dormbr( int matrix_order, char vect, char side, char trans,
                           int m, int n, int k,
                           const double* a, int lda, const double* tau,
                           double* c, int ldc );
int LAPACKE_dormhr( int matrix_order, char side, char trans,
                           int m, int n, int ilo,
                           int ihi, const double* a, int lda,
                           const double* tau, double* c, int ldc );
int LAPACKE_dormlq( int matrix_order, char side, char trans,
                           int m, int n, int k,
                           const double* a, int lda, const double* tau,
                           double* c, int ldc );
int LAPACKE_dormql( int matrix_order, char side, char trans,
                           int m, int n, int k,
                           const double* a, int lda, const double* tau,
                           double* c, int ldc );
int LAPACKE_dormqr( int matrix_order, char side, char trans,
                           int m, int n, int k,
                           const double* a, int lda, const double* tau,
                           double* c, int ldc );
int LAPACKE_dormrq( int matrix_order, char side, char trans,
                           int m, int n, int k,
                           const double* a, int lda, const double* tau,
                           double* c, int ldc );
int LAPACKE_dormrz( int matrix_order, char side, char trans,
                           int m, int n, int k,
                           int l, const double* a, int lda,
                           const double* tau, double* c, int ldc );
int LAPACKE_dormtr( int matrix_order, char side, char uplo, char trans,
                           int m, int n, const double* a,
                           int lda, const double* tau, double* c,
                           int ldc );
int LAPACKE_dpbcon( int matrix_order, char uplo, int n,
                           int kd, const double* ab, int ldab,
                           double anorm, double* rcond );
int LAPACKE_zpbcon( int matrix_order, char uplo, int n,
                           int kd, const double _Complex* ab,
                           int ldab, double anorm, double* rcond );
int LAPACKE_dpbequ( int matrix_order, char uplo, int n,
                           int kd, const double* ab, int ldab,
                           double* s, double* scond, double* amax );
int LAPACKE_zpbequ( int matrix_order, char uplo, int n,
                           int kd, const double _Complex* ab,
                           int ldab, double* s, double* scond,
                           double* amax );
int LAPACKE_dpbrfs( int matrix_order, char uplo, int n,
                           int kd, int nrhs, const double* ab,
                           int ldab, const double* afb, int ldafb,
                           const double* b, int ldb, double* x,
                           int ldx, double* ferr, double* berr );
int LAPACKE_zpbrfs( int matrix_order, char uplo, int n,
                           int kd, int nrhs,
                           const double _Complex* ab, int ldab,
                           const double _Complex* afb, int ldafb,
                           const double _Complex* b, int ldb,
                           double _Complex* x, int ldx,
                           double* ferr, double* berr );
int LAPACKE_dpbstf( int matrix_order, char uplo, int n,
                           int kb, double* bb, int ldbb );
int LAPACKE_zpbstf( int matrix_order, char uplo, int n,
                           int kb, double _Complex* bb,
                           int ldbb );
int LAPACKE_dpbsv( int matrix_order, char uplo, int n,
                          int kd, int nrhs, double* ab,
                          int ldab, double* b, int ldb );
int LAPACKE_zpbsv( int matrix_order, char uplo, int n,
                          int kd, int nrhs,
                          double _Complex* ab, int ldab,
                          double _Complex* b, int ldb );
int LAPACKE_dpbsvx( int matrix_order, char fact, char uplo, int n,
                           int kd, int nrhs, double* ab,
                           int ldab, double* afb, int ldafb,
                           char* equed, double* s, double* b, int ldb,
                           double* x, int ldx, double* rcond,
                           double* ferr, double* berr );
int LAPACKE_zpbsvx( int matrix_order, char fact, char uplo, int n,
                           int kd, int nrhs,
                           double _Complex* ab, int ldab,
                           double _Complex* afb, int ldafb,
                           char* equed, double* s, double _Complex* b,
                           int ldb, double _Complex* x,
                           int ldx, double* rcond, double* ferr,
                           double* berr );
int LAPACKE_dpbtrf( int matrix_order, char uplo, int n,
                           int kd, double* ab, int ldab );
int LAPACKE_zpbtrf( int matrix_order, char uplo, int n,
                           int kd, double _Complex* ab,
                           int ldab );
int LAPACKE_dpbtrs( int matrix_order, char uplo, int n,
                           int kd, int nrhs, const double* ab,
                           int ldab, double* b, int ldb );
int LAPACKE_zpbtrs( int matrix_order, char uplo, int n,
                           int kd, int nrhs,
                           const double _Complex* ab, int ldab,
                           double _Complex* b, int ldb );
int LAPACKE_dpftrf( int matrix_order, char transr, char uplo,
                           int n, double* a );
int LAPACKE_zpftrf( int matrix_order, char transr, char uplo,
                           int n, double _Complex* a );
int LAPACKE_dpftri( int matrix_order, char transr, char uplo,
                           int n, double* a );
int LAPACKE_zpftri( int matrix_order, char transr, char uplo,
                           int n, double _Complex* a );
int LAPACKE_dpftrs( int matrix_order, char transr, char uplo,
                           int n, int nrhs, const double* a,
                           double* b, int ldb );
int LAPACKE_zpftrs( int matrix_order, char transr, char uplo,
                           int n, int nrhs,
                           const double _Complex* a,
                           double _Complex* b, int ldb );
int LAPACKE_dpocon( int matrix_order, char uplo, int n,
                           const double* a, int lda, double anorm,
                           double* rcond );
int LAPACKE_zpocon( int matrix_order, char uplo, int n,
                           const double _Complex* a, int lda,
                           double anorm, double* rcond );
int LAPACKE_dpoequ( int matrix_order, int n, const double* a,
                           int lda, double* s, double* scond,
                           double* amax );
int LAPACKE_zpoequ( int matrix_order, int n,
                           const double _Complex* a, int lda,
                           double* s, double* scond, double* amax );
int LAPACKE_dpoequb( int matrix_order, int n, const double* a,
                            int lda, double* s, double* scond,
                            double* amax );
int LAPACKE_zpoequb( int matrix_order, int n,
                            const double _Complex* a, int lda,
                            double* s, double* scond, double* amax );
int LAPACKE_dporfs( int matrix_order, char uplo, int n,
                           int nrhs, const double* a, int lda,
                           const double* af, int ldaf, const double* b,
                           int ldb, double* x, int ldx,
                           double* ferr, double* berr );
int LAPACKE_zporfs( int matrix_order, char uplo, int n,
                           int nrhs, const double _Complex* a,
                           int lda, const double _Complex* af,
                           int ldaf, const double _Complex* b,
                           int ldb, double _Complex* x,
                           int ldx, double* ferr, double* berr );
int LAPACKE_dporfsx( int matrix_order, char uplo, char equed,
                            int n, int nrhs, const double* a,
                            int lda, const double* af, int ldaf,
                            const double* s, const double* b, int ldb,
                            double* x, int ldx, double* rcond,
                            double* berr, int n_err_bnds,
                            double* err_bnds_norm, double* err_bnds_comp,
                            int nparams, double* params );
int LAPACKE_zporfsx( int matrix_order, char uplo, char equed,
                            int n, int nrhs,
                            const double _Complex* a, int lda,
                            const double _Complex* af, int ldaf,
                            const double* s, const double _Complex* b,
                            int ldb, double _Complex* x,
                            int ldx, double* rcond, double* berr,
                            int n_err_bnds, double* err_bnds_norm,
                            double* err_bnds_comp, int nparams,
                            double* params );
int LAPACKE_dposv( int matrix_order, char uplo, int n,
                          int nrhs, double* a, int lda, double* b,
                          int ldb );
int LAPACKE_zposv( int matrix_order, char uplo, int n,
                          int nrhs, double _Complex* a,
                          int lda, double _Complex* b,
                          int ldb );
int LAPACKE_dsposv( int matrix_order, char uplo, int n,
                           int nrhs, double* a, int lda,
                           double* b, int ldb, double* x, int ldx,
                           int* iter );
int LAPACKE_zcposv( int matrix_order, char uplo, int n,
                           int nrhs, double _Complex* a,
                           int lda, double _Complex* b,
                           int ldb, double _Complex* x,
                           int ldx, int* iter );
int LAPACKE_dposvx( int matrix_order, char fact, char uplo, int n,
                           int nrhs, double* a, int lda,
                           double* af, int ldaf, char* equed, double* s,
                           double* b, int ldb, double* x, int ldx,
                           double* rcond, double* ferr, double* berr );
int LAPACKE_zposvx( int matrix_order, char fact, char uplo, int n,
                           int nrhs, double _Complex* a,
                           int lda, double _Complex* af,
                           int ldaf, char* equed, double* s,
                           double _Complex* b, int ldb,
                           double _Complex* x, int ldx,
                           double* rcond, double* ferr, double* berr );
int LAPACKE_dposvxx( int matrix_order, char fact, char uplo,
                            int n, int nrhs, double* a,
                            int lda, double* af, int ldaf,
                            char* equed, double* s, double* b, int ldb,
                            double* x, int ldx, double* rcond,
                            double* rpvgrw, double* berr, int n_err_bnds,
                            double* err_bnds_norm, double* err_bnds_comp,
                            int nparams, double* params );
int LAPACKE_zposvxx( int matrix_order, char fact, char uplo,
                            int n, int nrhs,
                            double _Complex* a, int lda,
                            double _Complex* af, int ldaf,
                            char* equed, double* s, double _Complex* b,
                            int ldb, double _Complex* x,
                            int ldx, double* rcond, double* rpvgrw,
                            double* berr, int n_err_bnds,
                            double* err_bnds_norm, double* err_bnds_comp,
                            int nparams, double* params );
int LAPACKE_dpotrf( int matrix_order, char uplo, int n, double* a,
                           int lda );
int LAPACKE_zpotrf( int matrix_order, char uplo, int n,
                           double _Complex* a, int lda );
int LAPACKE_dpotri( int matrix_order, char uplo, int n, double* a,
                           int lda );
int LAPACKE_zpotri( int matrix_order, char uplo, int n,
                           double _Complex* a, int lda );
int LAPACKE_dpotrs( int matrix_order, char uplo, int n,
                           int nrhs, const double* a, int lda,
                           double* b, int ldb );
int LAPACKE_zpotrs( int matrix_order, char uplo, int n,
                           int nrhs, const double _Complex* a,
                           int lda, double _Complex* b,
                           int ldb );
int LAPACKE_dppcon( int matrix_order, char uplo, int n,
                           const double* ap, double anorm, double* rcond );
int LAPACKE_zppcon( int matrix_order, char uplo, int n,
                           const double _Complex* ap, double anorm,
                           double* rcond );
int LAPACKE_dppequ( int matrix_order, char uplo, int n,
                           const double* ap, double* s, double* scond,
                           double* amax );
int LAPACKE_zppequ( int matrix_order, char uplo, int n,
                           const double _Complex* ap, double* s,
                           double* scond, double* amax );
int LAPACKE_dpprfs( int matrix_order, char uplo, int n,
                           int nrhs, const double* ap, const double* afp,
                           const double* b, int ldb, double* x,
                           int ldx, double* ferr, double* berr );
int LAPACKE_zpprfs( int matrix_order, char uplo, int n,
                           int nrhs, const double _Complex* ap,
                           const double _Complex* afp,
                           const double _Complex* b, int ldb,
                           double _Complex* x, int ldx,
                           double* ferr, double* berr );
int LAPACKE_dppsv( int matrix_order, char uplo, int n,
                          int nrhs, double* ap, double* b,
                          int ldb );
int LAPACKE_zppsv( int matrix_order, char uplo, int n,
                          int nrhs, double _Complex* ap,
                          double _Complex* b, int ldb );
int LAPACKE_dppsvx( int matrix_order, char fact, char uplo, int n,
                           int nrhs, double* ap, double* afp,
                           char* equed, double* s, double* b, int ldb,
                           double* x, int ldx, double* rcond,
                           double* ferr, double* berr );
int LAPACKE_zppsvx( int matrix_order, char fact, char uplo, int n,
                           int nrhs, double _Complex* ap,
                           double _Complex* afp, char* equed, double* s,
                           double _Complex* b, int ldb,
                           double _Complex* x, int ldx,
                           double* rcond, double* ferr, double* berr );
int LAPACKE_dpptrf( int matrix_order, char uplo, int n,
                           double* ap );
int LAPACKE_zpptrf( int matrix_order, char uplo, int n,
                           double _Complex* ap );
int LAPACKE_dpptri( int matrix_order, char uplo, int n,
                           double* ap );
int LAPACKE_zpptri( int matrix_order, char uplo, int n,
                           double _Complex* ap );
int LAPACKE_dpptrs( int matrix_order, char uplo, int n,
                           int nrhs, const double* ap, double* b,
                           int ldb );
int LAPACKE_zpptrs( int matrix_order, char uplo, int n,
                           int nrhs, const double _Complex* ap,
                           double _Complex* b, int ldb );
int LAPACKE_dpstrf( int matrix_order, char uplo, int n, double* a,
                           int lda, int* piv, int* rank,
                           double tol );
int LAPACKE_zpstrf( int matrix_order, char uplo, int n,
                           double _Complex* a, int lda,
                           int* piv, int* rank, double tol );
int LAPACKE_dptcon( int n, const double* d, const double* e,
                           double anorm, double* rcond );
int LAPACKE_zptcon( int n, const double* d,
                           const double _Complex* e, double anorm,
                           double* rcond );
int LAPACKE_dpteqr( int matrix_order, char compz, int n,
                           double* d, double* e, double* z, int ldz );
int LAPACKE_zpteqr( int matrix_order, char compz, int n,
                           double* d, double* e, double _Complex* z,
                           int ldz );
int LAPACKE_dptrfs( int matrix_order, int n, int nrhs,
                           const double* d, const double* e, const double* df,
                           const double* ef, const double* b, int ldb,
                           double* x, int ldx, double* ferr,
                           double* berr );
int LAPACKE_zptrfs( int matrix_order, char uplo, int n,
                           int nrhs, const double* d,
                           const double _Complex* e, const double* df,
                           const double _Complex* ef,
                           const double _Complex* b, int ldb,
                           double _Complex* x, int ldx,
                           double* ferr, double* berr );
int LAPACKE_dptsv( int matrix_order, int n, int nrhs,
                          double* d, double* e, double* b, int ldb );
int LAPACKE_zptsv( int matrix_order, int n, int nrhs,
                          double* d, double _Complex* e,
                          double _Complex* b, int ldb );
int LAPACKE_dptsvx( int matrix_order, char fact, int n,
                           int nrhs, const double* d, const double* e,
                           double* df, double* ef, const double* b,
                           int ldb, double* x, int ldx,
                           double* rcond, double* ferr, double* berr );
int LAPACKE_zptsvx( int matrix_order, char fact, int n,
                           int nrhs, const double* d,
                           const double _Complex* e, double* df,
                           double _Complex* ef,
                           const double _Complex* b, int ldb,
                           double _Complex* x, int ldx,
                           double* rcond, double* ferr, double* berr );
int LAPACKE_dpttrf( int n, double* d, double* e );
int LAPACKE_zpttrf( int n, double* d, double _Complex* e );
int LAPACKE_dpttrs( int matrix_order, int n, int nrhs,
                           const double* d, const double* e, double* b,
                           int ldb );
int LAPACKE_zpttrs( int matrix_order, char uplo, int n,
                           int nrhs, const double* d,
                           const double _Complex* e,
                           double _Complex* b, int ldb );
int LAPACKE_dsbev( int matrix_order, char jobz, char uplo, int n,
                          int kd, double* ab, int ldab, double* w,
                          double* z, int ldz );
int LAPACKE_dsbevd( int matrix_order, char jobz, char uplo, int n,
                           int kd, double* ab, int ldab,
                           double* w, double* z, int ldz );
int LAPACKE_dsbevx( int matrix_order, char jobz, char range, char uplo,
                           int n, int kd, double* ab,
                           int ldab, double* q, int ldq,
                           double vl, double vu, int il, int iu,
                           double abstol, int* m, double* w, double* z,
                           int ldz, int* ifail );
int LAPACKE_dsbgst( int matrix_order, char vect, char uplo, int n,
                           int ka, int kb, double* ab,
                           int ldab, const double* bb, int ldbb,
                           double* x, int ldx );
int LAPACKE_dsbgv( int matrix_order, char jobz, char uplo, int n,
                          int ka, int kb, double* ab,
                          int ldab, double* bb, int ldbb,
                          double* w, double* z, int ldz );
int LAPACKE_dsbgvd( int matrix_order, char jobz, char uplo, int n,
                           int ka, int kb, double* ab,
                           int ldab, double* bb, int ldbb,
                           double* w, double* z, int ldz );
int LAPACKE_dsbgvx( int matrix_order, char jobz, char range, char uplo,
                           int n, int ka, int kb,
                           double* ab, int ldab, double* bb,
                           int ldbb, double* q, int ldq,
                           double vl, double vu, int il, int iu,
                           double abstol, int* m, double* w, double* z,
                           int ldz, int* ifail );
int LAPACKE_dsbtrd( int matrix_order, char vect, char uplo, int n,
                           int kd, double* ab, int ldab,
                           double* d, double* e, double* q, int ldq );
int LAPACKE_dsfrk( int matrix_order, char transr, char uplo, char trans,
                          int n, int k, double alpha,
                          const double* a, int lda, double beta,
                          double* c );
int LAPACKE_dspcon( int matrix_order, char uplo, int n,
                           const double* ap, const int* ipiv,
                           double anorm, double* rcond );
int LAPACKE_zspcon( int matrix_order, char uplo, int n,
                           const double _Complex* ap,
                           const int* ipiv, double anorm,
                           double* rcond );
int LAPACKE_dspev( int matrix_order, char jobz, char uplo, int n,
                          double* ap, double* w, double* z, int ldz );
int LAPACKE_dspevd( int matrix_order, char jobz, char uplo, int n,
                           double* ap, double* w, double* z, int ldz );
int LAPACKE_dspevx( int matrix_order, char jobz, char range, char uplo,
                           int n, double* ap, double vl, double vu,
                           int il, int iu, double abstol,
                           int* m, double* w, double* z, int ldz,
                           int* ifail );
int LAPACKE_dspgst( int matrix_order, int itype, char uplo,
                           int n, double* ap, const double* bp );
int LAPACKE_dspgv( int matrix_order, int itype, char jobz,
                          char uplo, int n, double* ap, double* bp,
                          double* w, double* z, int ldz );
int LAPACKE_dspgvd( int matrix_order, int itype, char jobz,
                           char uplo, int n, double* ap, double* bp,
                           double* w, double* z, int ldz );
int LAPACKE_dspgvx( int matrix_order, int itype, char jobz,
                           char range, char uplo, int n, double* ap,
                           double* bp, double vl, double vu, int il,
                           int iu, double abstol, int* m,
                           double* w, double* z, int ldz,
                           int* ifail );
int LAPACKE_dsprfs( int matrix_order, char uplo, int n,
                           int nrhs, const double* ap, const double* afp,
                           const int* ipiv, const double* b,
                           int ldb, double* x, int ldx,
                           double* ferr, double* berr );
int LAPACKE_zsprfs( int matrix_order, char uplo, int n,
                           int nrhs, const double _Complex* ap,
                           const double _Complex* afp,
                           const int* ipiv,
                           const double _Complex* b, int ldb,
                           double _Complex* x, int ldx,
                           double* ferr, double* berr );
int LAPACKE_dspsv( int matrix_order, char uplo, int n,
                          int nrhs, double* ap, int* ipiv,
                          double* b, int ldb );
int LAPACKE_zspsv( int matrix_order, char uplo, int n,
                          int nrhs, double _Complex* ap,
                          int* ipiv, double _Complex* b,
                          int ldb );
int LAPACKE_dspsvx( int matrix_order, char fact, char uplo, int n,
                           int nrhs, const double* ap, double* afp,
                           int* ipiv, const double* b, int ldb,
                           double* x, int ldx, double* rcond,
                           double* ferr, double* berr );
int LAPACKE_zspsvx( int matrix_order, char fact, char uplo, int n,
                           int nrhs, const double _Complex* ap,
                           double _Complex* afp, int* ipiv,
                           const double _Complex* b, int ldb,
                           double _Complex* x, int ldx,
                           double* rcond, double* ferr, double* berr );
int LAPACKE_dsptrd( int matrix_order, char uplo, int n,
                           double* ap, double* d, double* e, double* tau );
int LAPACKE_dsptrf( int matrix_order, char uplo, int n,
                           double* ap, int* ipiv );
int LAPACKE_zsptrf( int matrix_order, char uplo, int n,
                           double _Complex* ap, int* ipiv );
int LAPACKE_dsptri( int matrix_order, char uplo, int n,
                           double* ap, const int* ipiv );
int LAPACKE_zsptri( int matrix_order, char uplo, int n,
                           double _Complex* ap, const int* ipiv );
int LAPACKE_dsptrs( int matrix_order, char uplo, int n,
                           int nrhs, const double* ap,
                           const int* ipiv, double* b, int ldb );
int LAPACKE_zsptrs( int matrix_order, char uplo, int n,
                           int nrhs, const double _Complex* ap,
                           const int* ipiv, double _Complex* b,
                           int ldb );
int LAPACKE_dstebz( char range, char order, int n, double vl,
                           double vu, int il, int iu,
                           double abstol, const double* d, const double* e,
                           int* m, int* nsplit, double* w,
                           int* iblock, int* isplit );
int LAPACKE_dstedc( int matrix_order, char compz, int n,
                           double* d, double* e, double* z, int ldz );
int LAPACKE_zstedc( int matrix_order, char compz, int n,
                           double* d, double* e, double _Complex* z,
                           int ldz );
int LAPACKE_dstegr( int matrix_order, char jobz, char range,
                           int n, double* d, double* e, double vl,
                           double vu, int il, int iu,
                           double abstol, int* m, double* w, double* z,
                           int ldz, int* isuppz );
int LAPACKE_zstegr( int matrix_order, char jobz, char range,
                           int n, double* d, double* e, double vl,
                           double vu, int il, int iu,
                           double abstol, int* m, double* w,
                           double _Complex* z, int ldz,
                           int* isuppz );
int LAPACKE_dstein( int matrix_order, int n, const double* d,
                           const double* e, int m, const double* w,
                           const int* iblock, const int* isplit,
                           double* z, int ldz, int* ifailv );
int LAPACKE_zstein( int matrix_order, int n, const double* d,
                           const double* e, int m, const double* w,
                           const int* iblock, const int* isplit,
                           double _Complex* z, int ldz,
                           int* ifailv );
int LAPACKE_dstemr( int matrix_order, char jobz, char range,
                           int n, double* d, double* e, double vl,
                           double vu, int il, int iu,
                           int* m, double* w, double* z, int ldz,
                           int nzc, int* isuppz,
                           int* tryrac );
int LAPACKE_zstemr( int matrix_order, char jobz, char range,
                           int n, double* d, double* e, double vl,
                           double vu, int il, int iu,
                           int* m, double* w, double _Complex* z,
                           int ldz, int nzc, int* isuppz,
                           int* tryrac );
int LAPACKE_dsteqr( int matrix_order, char compz, int n,
                           double* d, double* e, double* z, int ldz );
int LAPACKE_zsteqr( int matrix_order, char compz, int n,
                           double* d, double* e, double _Complex* z,
                           int ldz );
int LAPACKE_dsterf( int n, double* d, double* e );
int LAPACKE_dstev( int matrix_order, char jobz, int n, double* d,
                          double* e, double* z, int ldz );
int LAPACKE_dstevd( int matrix_order, char jobz, int n, double* d,
                           double* e, double* z, int ldz );
int LAPACKE_dstevr( int matrix_order, char jobz, char range,
                           int n, double* d, double* e, double vl,
                           double vu, int il, int iu,
                           double abstol, int* m, double* w, double* z,
                           int ldz, int* isuppz );
int LAPACKE_dstevx( int matrix_order, char jobz, char range,
                           int n, double* d, double* e, double vl,
                           double vu, int il, int iu,
                           double abstol, int* m, double* w, double* z,
                           int ldz, int* ifail );
int LAPACKE_dsycon( int matrix_order, char uplo, int n,
                           const double* a, int lda,
                           const int* ipiv, double anorm,
                           double* rcond );
int LAPACKE_zsycon( int matrix_order, char uplo, int n,
                           const double _Complex* a, int lda,
                           const int* ipiv, double anorm,
                           double* rcond );
int LAPACKE_dsyequb( int matrix_order, char uplo, int n,
                            const double* a, int lda, double* s,
                            double* scond, double* amax );
int LAPACKE_zsyequb( int matrix_order, char uplo, int n,
                            const double _Complex* a, int lda,
                            double* s, double* scond, double* amax );
int LAPACKE_dsyev( int matrix_order, char jobz, char uplo, int n,
                          double* a, int lda, double* w );
int LAPACKE_dsyevd( int matrix_order, char jobz, char uplo, int n,
                           double* a, int lda, double* w );
int LAPACKE_dsyevr( int matrix_order, char jobz, char range, char uplo,
                           int n, double* a, int lda, double vl,
                           double vu, int il, int iu,
                           double abstol, int* m, double* w, double* z,
                           int ldz, int* isuppz );
int LAPACKE_dsyevx( int matrix_order, char jobz, char range, char uplo,
                           int n, double* a, int lda, double vl,
                           double vu, int il, int iu,
                           double abstol, int* m, double* w, double* z,
                           int ldz, int* ifail );
int LAPACKE_dsygst( int matrix_order, int itype, char uplo,
                           int n, double* a, int lda,
                           const double* b, int ldb );
int LAPACKE_dsygv( int matrix_order, int itype, char jobz,
                          char uplo, int n, double* a, int lda,
                          double* b, int ldb, double* w );
int LAPACKE_dsygvd( int matrix_order, int itype, char jobz,
                           char uplo, int n, double* a, int lda,
                           double* b, int ldb, double* w );
int LAPACKE_dsygvx( int matrix_order, int itype, char jobz,
                           char range, char uplo, int n, double* a,
                           int lda, double* b, int ldb, double vl,
                           double vu, int il, int iu,
                           double abstol, int* m, double* w, double* z,
                           int ldz, int* ifail );
int LAPACKE_dsyrfs( int matrix_order, char uplo, int n,
                           int nrhs, const double* a, int lda,
                           const double* af, int ldaf,
                           const int* ipiv, const double* b,
                           int ldb, double* x, int ldx,
                           double* ferr, double* berr );
int LAPACKE_zsyrfs( int matrix_order, char uplo, int n,
                           int nrhs, const double _Complex* a,
                           int lda, const double _Complex* af,
                           int ldaf, const int* ipiv,
                           const double _Complex* b, int ldb,
                           double _Complex* x, int ldx,
                           double* ferr, double* berr );
int LAPACKE_dsyrfsx( int matrix_order, char uplo, char equed,
                            int n, int nrhs, const double* a,
                            int lda, const double* af, int ldaf,
                            const int* ipiv, const double* s,
                            const double* b, int ldb, double* x,
                            int ldx, double* rcond, double* berr,
                            int n_err_bnds, double* err_bnds_norm,
                            double* err_bnds_comp, int nparams,
                            double* params );
int LAPACKE_zsyrfsx( int matrix_order, char uplo, char equed,
                            int n, int nrhs,
                            const double _Complex* a, int lda,
                            const double _Complex* af, int ldaf,
                            const int* ipiv, const double* s,
                            const double _Complex* b, int ldb,
                            double _Complex* x, int ldx,
                            double* rcond, double* berr, int n_err_bnds,
                            double* err_bnds_norm, double* err_bnds_comp,
                            int nparams, double* params );
int LAPACKE_dsysv( int matrix_order, char uplo, int n,
                          int nrhs, double* a, int lda,
                          int* ipiv, double* b, int ldb );
int LAPACKE_zsysv( int matrix_order, char uplo, int n,
                          int nrhs, double _Complex* a,
                          int lda, int* ipiv,
                          double _Complex* b, int ldb );
int LAPACKE_dsysvx( int matrix_order, char fact, char uplo, int n,
                           int nrhs, const double* a, int lda,
                           double* af, int ldaf, int* ipiv,
                           const double* b, int ldb, double* x,
                           int ldx, double* rcond, double* ferr,
                           double* berr );
int LAPACKE_zsysvx( int matrix_order, char fact, char uplo, int n,
                           int nrhs, const double _Complex* a,
                           int lda, double _Complex* af,
                           int ldaf, int* ipiv,
                           const double _Complex* b, int ldb,
                           double _Complex* x, int ldx,
                           double* rcond, double* ferr, double* berr );
int LAPACKE_dsysvxx( int matrix_order, char fact, char uplo,
                            int n, int nrhs, double* a,
                            int lda, double* af, int ldaf,
                            int* ipiv, char* equed, double* s, double* b,
                            int ldb, double* x, int ldx,
                            double* rcond, double* rpvgrw, double* berr,
                            int n_err_bnds, double* err_bnds_norm,
                            double* err_bnds_comp, int nparams,
                            double* params );
int LAPACKE_zsysvxx( int matrix_order, char fact, char uplo,
                            int n, int nrhs,
                            double _Complex* a, int lda,
                            double _Complex* af, int ldaf,
                            int* ipiv, char* equed, double* s,
                            double _Complex* b, int ldb,
                            double _Complex* x, int ldx,
                            double* rcond, double* rpvgrw, double* berr,
                            int n_err_bnds, double* err_bnds_norm,
                            double* err_bnds_comp, int nparams,
                            double* params );
int LAPACKE_dsytrd( int matrix_order, char uplo, int n, double* a,
                           int lda, double* d, double* e, double* tau );
int LAPACKE_dsytrf( int matrix_order, char uplo, int n, double* a,
                           int lda, int* ipiv );
int LAPACKE_zsytrf( int matrix_order, char uplo, int n,
                           double _Complex* a, int lda,
                           int* ipiv );
int LAPACKE_dsytri( int matrix_order, char uplo, int n, double* a,
                           int lda, const int* ipiv );
int LAPACKE_zsytri( int matrix_order, char uplo, int n,
                           double _Complex* a, int lda,
                           const int* ipiv );
int LAPACKE_dsytrs( int matrix_order, char uplo, int n,
                           int nrhs, const double* a, int lda,
                           const int* ipiv, double* b, int ldb );
int LAPACKE_zsytrs( int matrix_order, char uplo, int n,
                           int nrhs, const double _Complex* a,
                           int lda, const int* ipiv,
                           double _Complex* b, int ldb );
int LAPACKE_dtbcon( int matrix_order, char norm, char uplo, char diag,
                           int n, int kd, const double* ab,
                           int ldab, double* rcond );
int LAPACKE_ztbcon( int matrix_order, char norm, char uplo, char diag,
                           int n, int kd,
                           const double _Complex* ab, int ldab,
                           double* rcond );
int LAPACKE_dtbrfs( int matrix_order, char uplo, char trans, char diag,
                           int n, int kd, int nrhs,
                           const double* ab, int ldab, const double* b,
                           int ldb, const double* x, int ldx,
                           double* ferr, double* berr );
int LAPACKE_ztbrfs( int matrix_order, char uplo, char trans, char diag,
                           int n, int kd, int nrhs,
                           const double _Complex* ab, int ldab,
                           const double _Complex* b, int ldb,
                           const double _Complex* x, int ldx,
                           double* ferr, double* berr );
int LAPACKE_dtbtrs( int matrix_order, char uplo, char trans, char diag,
                           int n, int kd, int nrhs,
                           const double* ab, int ldab, double* b,
                           int ldb );
int LAPACKE_ztbtrs( int matrix_order, char uplo, char trans, char diag,
                           int n, int kd, int nrhs,
                           const double _Complex* ab, int ldab,
                           double _Complex* b, int ldb );
int LAPACKE_dtfsm( int matrix_order, char transr, char side, char uplo,
                          char trans, char diag, int m, int n,
                          double alpha, const double* a, double* b,
                          int ldb );
int LAPACKE_ztfsm( int matrix_order, char transr, char side, char uplo,
                          char trans, char diag, int m, int n,
                          double _Complex alpha,
                          const double _Complex* a,
                          double _Complex* b, int ldb );
int LAPACKE_dtftri( int matrix_order, char transr, char uplo, char diag,
                           int n, double* a );
int LAPACKE_ztftri( int matrix_order, char transr, char uplo, char diag,
                           int n, double _Complex* a );
int LAPACKE_dtfttp( int matrix_order, char transr, char uplo,
                           int n, const double* arf, double* ap );
int LAPACKE_ztfttp( int matrix_order, char transr, char uplo,
                           int n, const double _Complex* arf,
                           double _Complex* ap );
int LAPACKE_dtfttr( int matrix_order, char transr, char uplo,
                           int n, const double* arf, double* a,
                           int lda );
int LAPACKE_ztfttr( int matrix_order, char transr, char uplo,
                           int n, const double _Complex* arf,
                           double _Complex* a, int lda );
int LAPACKE_dtgevc( int matrix_order, char side, char howmny,
                           const int* select, int n,
                           const double* s, int lds, const double* p,
                           int ldp, double* vl, int ldvl,
                           double* vr, int ldvr, int mm,
                           int* m );
int LAPACKE_ztgevc( int matrix_order, char side, char howmny,
                           const int* select, int n,
                           const double _Complex* s, int lds,
                           const double _Complex* p, int ldp,
                           double _Complex* vl, int ldvl,
                           double _Complex* vr, int ldvr,
                           int mm, int* m );
int LAPACKE_dtgexc( int matrix_order, int wantq,
                           int wantz, int n, double* a,
                           int lda, double* b, int ldb, double* q,
                           int ldq, double* z, int ldz,
                           int* ifst, int* ilst );
int LAPACKE_ztgexc( int matrix_order, int wantq,
                           int wantz, int n,
                           double _Complex* a, int lda,
                           double _Complex* b, int ldb,
                           double _Complex* q, int ldq,
                           double _Complex* z, int ldz,
                           int ifst, int ilst );
int LAPACKE_dtgsen( int matrix_order, int ijob,
                           int wantq, int wantz,
                           const int* select, int n,
                           double* a, int lda, double* b, int ldb,
                           double* alphar, double* alphai, double* beta,
                           double* q, int ldq, double* z, int ldz,
                           int* m, double* pl, double* pr, double* dif );
int LAPACKE_ztgsen( int matrix_order, int ijob,
                           int wantq, int wantz,
                           const int* select, int n,
                           double _Complex* a, int lda,
                           double _Complex* b, int ldb,
                           double _Complex* alpha,
                           double _Complex* beta,
                           double _Complex* q, int ldq,
                           double _Complex* z, int ldz,
                           int* m, double* pl, double* pr, double* dif );
int LAPACKE_dtgsja( int matrix_order, char jobu, char jobv, char jobq,
                           int m, int p, int n,
                           int k, int l, double* a,
                           int lda, double* b, int ldb,
                           double tola, double tolb, double* alpha,
                           double* beta, double* u, int ldu, double* v,
                           int ldv, double* q, int ldq,
                           int* ncycle );
int LAPACKE_ztgsja( int matrix_order, char jobu, char jobv, char jobq,
                           int m, int p, int n,
                           int k, int l, double _Complex* a,
                           int lda, double _Complex* b,
                           int ldb, double tola, double tolb,
                           double* alpha, double* beta,
                           double _Complex* u, int ldu,
                           double _Complex* v, int ldv,
                           double _Complex* q, int ldq,
                           int* ncycle );
int LAPACKE_dtgsna( int matrix_order, char job, char howmny,
                           const int* select, int n,
                           const double* a, int lda, const double* b,
                           int ldb, const double* vl, int ldvl,
                           const double* vr, int ldvr, double* s,
                           double* dif, int mm, int* m );
int LAPACKE_ztgsna( int matrix_order, char job, char howmny,
                           const int* select, int n,
                           const double _Complex* a, int lda,
                           const double _Complex* b, int ldb,
                           const double _Complex* vl, int ldvl,
                           const double _Complex* vr, int ldvr,
                           double* s, double* dif, int mm,
                           int* m );
int LAPACKE_dtgsyl( int matrix_order, char trans, int ijob,
                           int m, int n, const double* a,
                           int lda, const double* b, int ldb,
                           double* c, int ldc, const double* d,
                           int ldd, const double* e, int lde,
                           double* f, int ldf, double* scale,
                           double* dif );
int LAPACKE_ztgsyl( int matrix_order, char trans, int ijob,
                           int m, int n,
                           const double _Complex* a, int lda,
                           const double _Complex* b, int ldb,
                           double _Complex* c, int ldc,
                           const double _Complex* d, int ldd,
                           const double _Complex* e, int lde,
                           double _Complex* f, int ldf,
                           double* scale, double* dif );
int LAPACKE_dtpcon( int matrix_order, char norm, char uplo, char diag,
                           int n, const double* ap, double* rcond );
int LAPACKE_ztpcon( int matrix_order, char norm, char uplo, char diag,
                           int n, const double _Complex* ap,
                           double* rcond );
int LAPACKE_dtprfs( int matrix_order, char uplo, char trans, char diag,
                           int n, int nrhs, const double* ap,
                           const double* b, int ldb, const double* x,
                           int ldx, double* ferr, double* berr );
int LAPACKE_ztprfs( int matrix_order, char uplo, char trans, char diag,
                           int n, int nrhs,
                           const double _Complex* ap,
                           const double _Complex* b, int ldb,
                           const double _Complex* x, int ldx,
                           double* ferr, double* berr );
int LAPACKE_dtptri( int matrix_order, char uplo, char diag, int n,
                           double* ap );
int LAPACKE_ztptri( int matrix_order, char uplo, char diag, int n,
                           double _Complex* ap );
int LAPACKE_dtptrs( int matrix_order, char uplo, char trans, char diag,
                           int n, int nrhs, const double* ap,
                           double* b, int ldb );
int LAPACKE_ztptrs( int matrix_order, char uplo, char trans, char diag,
                           int n, int nrhs,
                           const double _Complex* ap,
                           double _Complex* b, int ldb );
int LAPACKE_dtpttf( int matrix_order, char transr, char uplo,
                           int n, const double* ap, double* arf );
int LAPACKE_ztpttf( int matrix_order, char transr, char uplo,
                           int n, const double _Complex* ap,
                           double _Complex* arf );
int LAPACKE_dtpttr( int matrix_order, char uplo, int n,
                           const double* ap, double* a, int lda );
int LAPACKE_ztpttr( int matrix_order, char uplo, int n,
                           const double _Complex* ap,
                           double _Complex* a, int lda );
int LAPACKE_dtrcon( int matrix_order, char norm, char uplo, char diag,
                           int n, const double* a, int lda,
                           double* rcond );
int LAPACKE_ztrcon( int matrix_order, char norm, char uplo, char diag,
                           int n, const double _Complex* a,
                           int lda, double* rcond );
int LAPACKE_dtrevc( int matrix_order, char side, char howmny,
                           int* select, int n,
                           const double* t, int ldt, double* vl,
                           int ldvl, double* vr, int ldvr,
                           int mm, int* m );
int LAPACKE_ztrevc( int matrix_order, char side, char howmny,
                           const int* select, int n,
                           double _Complex* t, int ldt,
                           double _Complex* vl, int ldvl,
                           double _Complex* vr, int ldvr,
                           int mm, int* m );
int LAPACKE_dtrexc( int matrix_order, char compq, int n,
                           double* t, int ldt, double* q, int ldq,
                           int* ifst, int* ilst );
int LAPACKE_ztrexc( int matrix_order, char compq, int n,
                           double _Complex* t, int ldt,
                           double _Complex* q, int ldq,
                           int ifst, int ilst );
int LAPACKE_dtrrfs( int matrix_order, char uplo, char trans, char diag,
                           int n, int nrhs, const double* a,
                           int lda, const double* b, int ldb,
                           const double* x, int ldx, double* ferr,
                           double* berr );
int LAPACKE_ztrrfs( int matrix_order, char uplo, char trans, char diag,
                           int n, int nrhs,
                           const double _Complex* a, int lda,
                           const double _Complex* b, int ldb,
                           const double _Complex* x, int ldx,
                           double* ferr, double* berr );
int LAPACKE_dtrsen( int matrix_order, char job, char compq,
                           const int* select, int n,
                           double* t, int ldt, double* q, int ldq,
                           double* wr, double* wi, int* m, double* s,
                           double* sep );
int LAPACKE_ztrsen( int matrix_order, char job, char compq,
                           const int* select, int n,
                           double _Complex* t, int ldt,
                           double _Complex* q, int ldq,
                           double _Complex* w, int* m, double* s,
                           double* sep );
int LAPACKE_dtrsna( int matrix_order, char job, char howmny,
                           const int* select, int n,
                           const double* t, int ldt, const double* vl,
                           int ldvl, const double* vr, int ldvr,
                           double* s, double* sep, int mm,
                           int* m );
int LAPACKE_ztrsna( int matrix_order, char job, char howmny,
                           const int* select, int n,
                           const double _Complex* t, int ldt,
                           const double _Complex* vl, int ldvl,
                           const double _Complex* vr, int ldvr,
                           double* s, double* sep, int mm,
                           int* m );
int LAPACKE_dtrsyl( int matrix_order, char trana, char tranb,
                           int isgn, int m, int n,
                           const double* a, int lda, const double* b,
                           int ldb, double* c, int ldc,
                           double* scale );
int LAPACKE_ztrsyl( int matrix_order, char trana, char tranb,
                           int isgn, int m, int n,
                           const double _Complex* a, int lda,
                           const double _Complex* b, int ldb,
                           double _Complex* c, int ldc,
                           double* scale );
int LAPACKE_dtrtri( int matrix_order, char uplo, char diag, int n,
                           double* a, int lda );
int LAPACKE_ztrtri( int matrix_order, char uplo, char diag, int n,
                           double _Complex* a, int lda );
int LAPACKE_dtrtrs( int matrix_order, char uplo, char trans, char diag,
                           int n, int nrhs, const double* a,
                           int lda, double* b, int ldb );
int LAPACKE_ztrtrs( int matrix_order, char uplo, char trans, char diag,
                           int n, int nrhs,
                           const double _Complex* a, int lda,
                           double _Complex* b, int ldb );
int LAPACKE_dtrttf( int matrix_order, char transr, char uplo,
                           int n, const double* a, int lda,
                           double* arf );
int LAPACKE_ztrttf( int matrix_order, char transr, char uplo,
                           int n, const double _Complex* a,
                           int lda, double _Complex* arf );
int LAPACKE_dtrttp( int matrix_order, char uplo, int n,
                           const double* a, int lda, double* ap );
int LAPACKE_ztrttp( int matrix_order, char uplo, int n,
                           const double _Complex* a, int lda,
                           double _Complex* ap );
int LAPACKE_dtzrzf( int matrix_order, int m, int n,
                           double* a, int lda, double* tau );
int LAPACKE_ztzrzf( int matrix_order, int m, int n,
                           double _Complex* a, int lda,
                           double _Complex* tau );
int LAPACKE_zungbr( int matrix_order, char vect, int m,
                           int n, int k, double _Complex* a,
                           int lda, const double _Complex* tau );
int LAPACKE_zunghr( int matrix_order, int n, int ilo,
                           int ihi, double _Complex* a,
                           int lda, const double _Complex* tau );
int LAPACKE_zunglq( int matrix_order, int m, int n,
                           int k, double _Complex* a,
                           int lda, const double _Complex* tau );
int LAPACKE_zungql( int matrix_order, int m, int n,
                           int k, double _Complex* a,
                           int lda, const double _Complex* tau );
int LAPACKE_zungqr( int matrix_order, int m, int n,
                           int k, double _Complex* a,
                           int lda, double _Complex* tau );
int LAPACKE_zungrq( int matrix_order, int m, int n,
                           int k, double _Complex* a,
                           int lda, const double _Complex* tau );
int LAPACKE_zungtr( int matrix_order, char uplo, int n,
                           double _Complex* a, int lda,
                           const double _Complex* tau );
int LAPACKE_zunmbr( int matrix_order, char vect, char side, char trans,
                           int m, int n, int k,
                           const double _Complex* a, int lda,
                           const double _Complex* tau,
                           double _Complex* c, int ldc );
int LAPACKE_zunmhr( int matrix_order, char side, char trans,
                           int m, int n, int ilo,
                           int ihi, const double _Complex* a,
                           int lda, const double _Complex* tau,
                           double _Complex* c, int ldc );
int LAPACKE_zunmlq( int matrix_order, char side, char trans,
                           int m, int n, int k,
                           const double _Complex* a, int lda,
                           const double _Complex* tau,
                           double _Complex* c, int ldc );
int LAPACKE_zunmql( int matrix_order, char side, char trans,
                           int m, int n, int k,
                           const double _Complex* a, int lda,
                           const double _Complex* tau,
                           double _Complex* c, int ldc );
int LAPACKE_zunmqr( int matrix_order, char side, char trans,
                           int m, int n, int k,
                           const double _Complex* a, int lda,
                           const double _Complex* tau,
                           double _Complex* c, int ldc );
int LAPACKE_zunmrq( int matrix_order, char side, char trans,
                           int m, int n, int k,
                           const double _Complex* a, int lda,
                           const double _Complex* tau,
                           double _Complex* c, int ldc );
int LAPACKE_zunmrz( int matrix_order, char side, char trans,
                           int m, int n, int k,
                           int l, const double _Complex* a,
                           int lda, const double _Complex* tau,
                           double _Complex* c, int ldc );
int LAPACKE_zunmtr( int matrix_order, char side, char uplo, char trans,
                           int m, int n,
                           const double _Complex* a, int lda,
                           const double _Complex* tau,
                           double _Complex* c, int ldc );
int LAPACKE_zupgtr( int matrix_order, char uplo, int n,
                           const double _Complex* ap,
                           const double _Complex* tau,
                           double _Complex* q, int ldq );
int LAPACKE_zupmtr( int matrix_order, char side, char uplo, char trans,
                           int m, int n,
                           const double _Complex* ap,
                           const double _Complex* tau,
                           double _Complex* c, int ldc );
enum CBLAS_ORDER {CblasRowMajor=101, CblasColMajor=102 };
enum CBLAS_TRANSPOSE {CblasNoTrans=111, CblasTrans=112, CblasConjTrans=113,
                      AtlasConj=114};
enum CBLAS_UPLO {CblasUpper=121, CblasLower=122};
enum CBLAS_DIAG {CblasNonUnit=131, CblasUnit=132};
enum CBLAS_SIDE {CblasLeft=141, CblasRight=142};
int cblas_errprn(int ierr, int info, char *form, ...);
void cblas_xerbla(int p, const char *rout, const char *form, ...);
double cblas_dsdot(const int N, const float *X, const int incX, const float *Y,
                   const int incY);
double cblas_ddot(const int N, const double *X, const int incX,
                  const double *Y, const int incY);
void cblas_zdotu_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu);
void cblas_zdotc_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc);
double cblas_dnrm2(const int N, const double *X, const int incX);
double cblas_dasum(const int N, const double *X, const int incX);
double cblas_dznrm2(const int N, const void *X, const int incX);
double cblas_dzasum(const int N, const void *X, const int incX);
int cblas_isamax(const int N, const float *X, const int incX);
int cblas_idamax(const int N, const double *X, const int incX);
int cblas_icamax(const int N, const void *X, const int incX);
int cblas_izamax(const int N, const void *X, const int incX);
void cblas_dswap(const int N, double *X, const int incX,
                 double *Y, const int incY);
void cblas_dcopy(const int N, const double *X, const int incX,
                 double *Y, const int incY);
void cblas_daxpy(const int N, const double alpha, const double *X,
                 const int incX, double *Y, const int incY);
void cblas_zswap(const int N, void *X, const int incX,
                 void *Y, const int incY);
void cblas_zcopy(const int N, const void *X, const int incX,
                 void *Y, const int incY);
void cblas_zaxpy(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY);
void cblas_drotg(double *a, double *b, double *c, double *s);
void cblas_drotmg(double *d1, double *d2, double *b1, const double b2, double *P);
void cblas_drot(const int N, double *X, const int incX,
                double *Y, const int incY, const double c, const double s);
void cblas_drotm(const int N, double *X, const int incX,
                double *Y, const int incY, const double *P);
void cblas_dscal(const int N, const double alpha, double *X, const int incX);
void cblas_zscal(const int N, const void *alpha, void *X, const int incX);
void cblas_zdscal(const int N, const double alpha, void *X, const int incX);
void cblas_zrotg(void *a, void *b, void *c, void *s);
void cblas_zdrot(const int N, void *X, const int incX, void *Y, const int incY,
                 const double c, const double s);
void cblas_dgemv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_TRANSPOSE TransA, const int M, const int N,
                 const double alpha, const double *A, const int lda,
                 const double *X, const int incX, const double beta,
                 double *Y, const int incY);
void cblas_dgbmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_TRANSPOSE TransA, const int M, const int N,
                 const int KL, const int KU, const double alpha,
                 const double *A, const int lda, const double *X,
                 const int incX, const double beta, double *Y, const int incY);
void cblas_dtrmv(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag,
                 const int N, const double *A, const int lda,
                 double *X, const int incX);
void cblas_dtbmv(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag,
                 const int N, const int K, const double *A, const int lda,
                 double *X, const int incX);
void cblas_dtpmv(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag,
                 const int N, const double *Ap, double *X, const int incX);
void cblas_dtrsv(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag,
                 const int N, const double *A, const int lda, double *X,
                 const int incX);
void cblas_dtbsv(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag,
                 const int N, const int K, const double *A, const int lda,
                 double *X, const int incX);
void cblas_dtpsv(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag,
                 const int N, const double *Ap, double *X, const int incX);
void cblas_zgemv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_TRANSPOSE TransA, const int M, const int N,
                 const void *alpha, const void *A, const int lda,
                 const void *X, const int incX, const void *beta,
                 void *Y, const int incY);
void cblas_zgbmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_TRANSPOSE TransA, const int M, const int N,
                 const int KL, const int KU, const void *alpha,
                 const void *A, const int lda, const void *X,
                 const int incX, const void *beta, void *Y, const int incY);
void cblas_ztrmv(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag,
                 const int N, const void *A, const int lda,
                 void *X, const int incX);
void cblas_ztbmv(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag,
                 const int N, const int K, const void *A, const int lda,
                 void *X, const int incX);
void cblas_ztpmv(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag,
                 const int N, const void *Ap, void *X, const int incX);
void cblas_ztrsv(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag,
                 const int N, const void *A, const int lda, void *X,
                 const int incX);
void cblas_ztbsv(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag,
                 const int N, const int K, const void *A, const int lda,
                 void *X, const int incX);
void cblas_ztpsv(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_DIAG Diag,
                 const int N, const void *Ap, void *X, const int incX);
void cblas_dsymv(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const int N, const double alpha, const double *A,
                 const int lda, const double *X, const int incX,
                 const double beta, double *Y, const int incY);
void cblas_dsbmv(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const int N, const int K, const double alpha, const double *A,
                 const int lda, const double *X, const int incX,
                 const double beta, double *Y, const int incY);
void cblas_dspmv(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const int N, const double alpha, const double *Ap,
                 const double *X, const int incX,
                 const double beta, double *Y, const int incY);
void cblas_dger(const enum CBLAS_ORDER Order, const int M, const int N,
                const double alpha, const double *X, const int incX,
                const double *Y, const int incY, double *A, const int lda);
void cblas_dsyr(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                const int N, const double alpha, const double *X,
                const int incX, double *A, const int lda);
void cblas_dspr(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                const int N, const double alpha, const double *X,
                const int incX, double *Ap);
void cblas_dsyr2(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                const int N, const double alpha, const double *X,
                const int incX, const double *Y, const int incY, double *A,
                const int lda);
void cblas_dspr2(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                const int N, const double alpha, const double *X,
                const int incX, const double *Y, const int incY, double *A);
void cblas_zhemv(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const int N, const void *alpha, const void *A,
                 const int lda, const void *X, const int incX,
                 const void *beta, void *Y, const int incY);
void cblas_zhbmv(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const int N, const int K, const void *alpha, const void *A,
                 const int lda, const void *X, const int incX,
                 const void *beta, void *Y, const int incY);
void cblas_zhpmv(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const int N, const void *alpha, const void *Ap,
                 const void *X, const int incX,
                 const void *beta, void *Y, const int incY);
void cblas_zgeru(const enum CBLAS_ORDER Order, const int M, const int N,
                 const void *alpha, const void *X, const int incX,
                 const void *Y, const int incY, void *A, const int lda);
void cblas_zgerc(const enum CBLAS_ORDER Order, const int M, const int N,
                 const void *alpha, const void *X, const int incX,
                 const void *Y, const int incY, void *A, const int lda);
void cblas_zher(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                const int N, const double alpha, const void *X, const int incX,
                void *A, const int lda);
void cblas_zhpr(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                const int N, const double alpha, const void *X,
                const int incX, void *A);
void cblas_zher2(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo, const int N,
                const void *alpha, const void *X, const int incX,
                const void *Y, const int incY, void *A, const int lda);
void cblas_zhpr2(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo, const int N,
                const void *alpha, const void *X, const int incX,
                const void *Y, const int incY, void *Ap);
void cblas_dgemm(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double alpha, const double *A,
                 const int lda, const double *B, const int ldb,
                 const double beta, double *C, const int ldc);
void cblas_dsymm(const enum CBLAS_ORDER Order, const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo, const int M, const int N,
                 const double alpha, const double *A, const int lda,
                 const double *B, const int ldb, const double beta,
                 double *C, const int ldc);
void cblas_dsyrk(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE Trans, const int N, const int K,
                 const double alpha, const double *A, const int lda,
                 const double beta, double *C, const int ldc);
void cblas_dsyr2k(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                  const enum CBLAS_TRANSPOSE Trans, const int N, const int K,
                  const double alpha, const double *A, const int lda,
                  const double *B, const int ldb, const double beta,
                  double *C, const int ldc);
void cblas_dtrmm(const enum CBLAS_ORDER Order, const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo, const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag, const int M, const int N,
                 const double alpha, const double *A, const int lda,
                 double *B, const int ldb);
void cblas_dtrsm(const enum CBLAS_ORDER Order, const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo, const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag, const int M, const int N,
                 const double alpha, const double *A, const int lda,
                 double *B, const int ldb);
void cblas_zgemm(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc);
void cblas_zsymm(const enum CBLAS_ORDER Order, const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo, const int M, const int N,
                 const void *alpha, const void *A, const int lda,
                 const void *B, const int ldb, const void *beta,
                 void *C, const int ldc);
void cblas_zsyrk(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE Trans, const int N, const int K,
                 const void *alpha, const void *A, const int lda,
                 const void *beta, void *C, const int ldc);
void cblas_zsyr2k(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                  const enum CBLAS_TRANSPOSE Trans, const int N, const int K,
                  const void *alpha, const void *A, const int lda,
                  const void *B, const int ldb, const void *beta,
                  void *C, const int ldc);
void cblas_ztrmm(const enum CBLAS_ORDER Order, const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo, const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag, const int M, const int N,
                 const void *alpha, const void *A, const int lda,
                 void *B, const int ldb);
void cblas_ztrsm(const enum CBLAS_ORDER Order, const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo, const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag, const int M, const int N,
                 const void *alpha, const void *A, const int lda,
                 void *B, const int ldb);
void cblas_zhemm(const enum CBLAS_ORDER Order, const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo, const int M, const int N,
                 const void *alpha, const void *A, const int lda,
                 const void *B, const int ldb, const void *beta,
                 void *C, const int ldc);
void cblas_zherk(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE Trans, const int N, const int K,
                 const double alpha, const void *A, const int lda,
                 const double beta, void *C, const int ldc);
void cblas_zher2k(const enum CBLAS_ORDER Order, const enum CBLAS_UPLO Uplo,
                  const enum CBLAS_TRANSPOSE Trans, const int N, const int K,
                  const void *alpha, const void *A, const int lda,
                  const void *B, const int ldb, const double beta,
                  void *C, const int ldc);
int cblas_errprn(int ierr, int info, char *form, ...);
]]
