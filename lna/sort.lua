--[[
    Inplace heapsort and reordering / arranging functions.
--]]

-- swaps two itens in an array
local function aux_swap(d, i, j)
    local aux = d[i]
    d[i] = d[j]
    d[j] = aux
end

-- swaps the element and the element order
local function swap(A, i, j)
    aux_swap(A.data       , i, j)
    aux_swap(A.sorder.data, i, j)
end

local function siftdown(A, i, lt)
    local son = 2*i+1 -- son
    
    -- a leaf node does not have sons, return
    if son >= A.ssize then return end
    
    -- verify the left and right sons
    local h  = son
    if (son+1) < A.ssize and not lt(A.data[son+1], A.data[h]) then
        h = son+1
    end
    
    -- If the highest son is higher than the parent, exchange
    if not lt(A.data[h],A.data[i]) then
        swap(A, h, i)
        siftdown(A, h, lt)
    end
end

local sort = {}

local function lessthan(a, b)
    return a < b
end

function sort.heapsort(A, lt)
    local total = A:len()
    lt = lt or lessthan
    
    A.ssize  = total
    A.sorder = A.linspace(1, total, total)
    
    -- step 1: heapify - O(n)
    for i = math.floor(total / 2)-1, 0, -1 do
        siftdown(A, i, lt)
    end
    
    -- step 2: perform removals until sorted - O(n * log n)
    for i = 1, total do
        A.ssize = A.ssize - 1
        swap(A, 0, A.ssize)
        siftdown(A, 0, lt)
    end
    
    local order = A.sorder
    A.ssize  = nil
    A.sorder = nil
    
    return A, order
end

-- inplace. Thanks S.O.
function sort.reorder(A, o) -- O(n), not O(n²)
    local total = A:len()
    
    for i = 1, total do
        local x = A.data[i-1]
        local j = i
        
        while true do
            local k     = o.data[j-1]
            o.data[j-1] = j
            if k == i then break end
            A.data[j-1] = A.data[k-1]
            j           = k
        end
        
        A.data[j-1] = x
    end
    
    return A
end

-- not inplace.
function sort.arrangerows(A, o)
    local B = A.new(o.m, A.n)
    for i = 1, o.m do
        B:row(i):fill(A:row( o(i) ))
    end
    
    return B
end

-- not inplace.
function sort.arrangecols(A, o)
    local B = A.new(A.m, o.m)
    for i = 1, o.m do
        B:col(i):fill(A:col( o(i) ))
    end
    
    return B
end

return sort
