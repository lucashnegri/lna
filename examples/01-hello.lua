--[[
    Hello world.
--]]

local matrix = require('lna.matrix')

-- create a matrix from a Lua table
local A = matrix.fromtable{
    {1, 1, 3},
    {4, 8, 2},
    {5, 8, 2}
}

-- create a matrix filled with ones
local B = matrix.new(3,3):fill(1)

-- basic operations
local C = (A + B):t()
local D = C:inv() * C

-- matrix D should be equal to the identity matrix:
local I = matrix.eye(3,3)
assert(D:equal(I, 1e-9))

print(D)
