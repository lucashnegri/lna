--- Numeric routines.

-- Numeric routines like function / least squares minimization and numerical
-- integration.
--
-- @module lna.numeric
local matrix = require('lna.matrix')
local stats  = require('lna.stats')

local numeric = {}

local amoeba_coefs = {
    1,      -- reflection
    2,      -- expansion
    0.5,    -- contraction
    0.5     -- shrink
}

local function runif(a, b)
    return (b-a)*math.random()+a
end

---
-- Downhill simplex function minimization. Nelder and Mead, 1965.
--
-- Searches for the parameter Xr (row vector) that minimizes the value of _func(Xr)_. The initial search space is delimited by _range_, a Lua
-- table with one entry per dimension, where each entry is a table {min,max} that limits the given
-- dimension to the [min,max] range.
--
-- At most _iters_ iterations will be performed. The search also
-- stops when there is no enhancement to the solution (stays within _eps_ from the previous best)
-- after an iteration. Custom reflection, expansion, contraction and shrink coefficients can be
-- used by passing then in a Lua table, in this order, to _coefs_ (default {1,2,0.5,0.5}).
function numeric.fmin(func, range, iters, eps, coefs)
    -- TODO respect the specified range.
    -- preparation
    iters = iters or 1e3
    eps   = eps   or 1e-6
    coefs = coefs or amoeba_coefs
    local n    = #range
    local p    = n+1
    local eval = matrix.new(p,1) -- holds the evaluations
    local x0   = matrix.new(1,n) -- centroid, excluding the worst point
    local f_h, h, f_lm, l        -- holds the lowest and highest points
    
    -- generate the amoeba
    local x = matrix.new(p, n)
    x:foreach(function(x, a, b)
        local val = runif(range[b][1], range[b][2])
        x:set(a,b, val)
    end)
    
    local it = 0
    while true do
        it = it + 1
        
        -- evaluate each point / solution
        for i = 1, p do eval:set(i, 1, func(x:row(i))) end
        
        -- select the highest (h) and lowest (l) points
        f_h, h = eval:max()
        f_l, l = eval:min()
        
        -- iteration limit reached
        if it > iters then break end
    
        -- compute the centroid
        x0:fill(0)
    
        for i = 1, p do if i ~= h then
            x0:add(x:row(i), true)
        end end
        x0:div(p-1, true)
    
        local xp   = (1 + coefs[1])*x0 - coefs[1]*x:row(h)
    
        local f_xp = func(xp)
       
        -- if it is lower than the lowest
        if f_xp < f_l then
            local xpp = (1 + coefs[2])*xp - coefs[2]*x0
            if func(xpp) < f_l then
                x:row(h):fill(xpp) -- expansion
            else
                x:row(h):fill(xp)  -- reflection
            end
        else
            eval:set(h, 1, -math.huge) -- trick
            local f_h2, h2 = eval:max()
            
            -- if the reflected solution is higher than all but the highest
            if f_xp > f_h2 then
                if f_xp < f_h then
                    x:row(h):fill(xp)  -- reflection
                end
                
                xpp = coefs[3]*x:row(h) + (1 - coefs[3])*x0
                
                if func(xpp) > f_h then
                    -- multiple contraction
                    for i = 1, p do
                        x:row(i):fill( (x:row(i) + x:row(l)) / 2)
                    end
                else
                    x:row(h):fill(xpp) -- contraction
                end
            else
                x:row(h):fill(xp)      -- reflection
            end
        end
    
        -- verify the convergence
        if math.sqrt(f_h - f_l) < eps then break end
    end
    
    return x:row(l):copy(), it
end

---
-- Fourth-order Runge-Kutta. Integrates the ordinary differential equation system described by
-- function _odesys_, of order _order_, using _values_ as the initial values.
--
-- _odesys(y,aux)_ must be a function that receives the current values as the array _y_ (Lua table), replacing the values
-- with the evaluation of each differential equation (_y_ is an in/out parameters). _aux_ is additional
-- userdata that must be passed as parameter to _odesys_ if needed by the user. This function returns
-- a function _iter(step)_ that, when called with a step of _step_, will integrate the system for
-- _step_ interval and return the current values.
--
-- Does not uses matrix operations (just Lua tables, faster).
-- @see 05-ode.lua
function numeric.rk4(odesys, order, values, aux)
    local k1, k2, k3, k4 = {}, {}, {}, {}
    
    return function(step)
        for i = 1, order do k1[i] = values[i] end
        odesys(k1, aux)
        
        for i = 1, order do k2[i] = 0.5*step*k1[i]+values[i] end
        odesys(k2, aux)
        
        for i = 1, order do k3[i] = 0.5*step*k2[i]+values[i] end
        odesys(k3, aux)
        
        for i = 1, order do k4[i] = step*k3[i]+values[i] end
        odesys(k4, aux)
        
        for i = 1, order do
            values[i] = values[i] + (step/6)*(k1[i] + 2*k2[i] + 2*k3[i] + k4[i])
        end
        
        return values
    end
end

-- finite difference: df/dx = ( f(x+eps)-f(x) ) / eps
local function diff(f, a, Xr, p, eps)
    local orig = a(p)
    
    local y1 = f(a, Xr)
    a:set(p, 1, orig + eps)
    local y2 = f(a, Xr)
    a:set(p, 1, orig)
    
    return (y2 - y1) / eps
end

---
-- Computes the mean squared error of the estimator function _f_.
-- Function _f_ must have the signature _o = f(a, x)_, where _o_ is a number, and
-- _a_ and _x_ are vectors.
-- 
-- Returns the mean squared error as a number.
local function fmse(f, a, X, y)
    local acc = 0
    local n_rows = X.m
    
    for r = 1, n_rows do
        local o   = f(a, X:row(r))
        local err = y(r) - o
        acc       = acc + err * err
    end
    
    return acc / n_rows
end

---
-- Computes the mean squared error between the elements of vectors _a_ and _b_.
-- _p_ can be used to compute an unbiased error estimation, using _v_ (default _a.m_)
-- degrees of freedom.
function numeric.mse(a, b, v)
    v = v or a.m
    local acc = 0
    
    for i = 1, a.m do
        local err = a:get(i,1) - b:get(i,1)
        acc = acc + err*err
    end
    
    return acc / v
end

---
-- Estimates a _goodness of fit_ by using the reduced χ²value.
--
-- The reduced χ² between the data on _a_ and _b_ vectors is returned, where
-- _s_ is a vector containing the standard uncertainty (not the variance)
-- of the measurements and _v_ (default _a.m_) degrees of freedom
function numeric.chisq(a, b, s, v)
    v = v or a.m
    local acc = 0
    
    for i = 1, a.m do
        local err = a:get(i,1) - b:get(i,1)
        acc = acc + err^2 / s:get(i,1)^2
    end
    
    return acc / v 
end
---
-- Levenberg-Marquardt implementation for non-linear least squares fitting. Optimizes the parameters
-- _a_ of function _f(X[r],a)_ in order to minimize the squares of the deviations between the response
-- of _f(Xr,a)_ and the expected values in the column vector _y_.
--
-- _a_ should be a column vector with the initial parameters, and will be modified inplace during the
-- optimization procedure. _X_ is the data to evaluate the fitting, with _X.m_ entries of _X.n_ dimensions,
-- where each row _X[r]_ corresponds to an expected output stores in _y[r]_. _X.n_ should be equal to
-- _a.m_.
--
-- The algorithm will run for at most _n\_iters_ (default 100) iterations, or when it converged (difference smaller
-- than _eps_ (default 1e-9) between the mean squared error of the current and previous iterations. The parameters
-- _u_ (default 0.1) and _v_ (default 10) controls the balance between gradient descent and the
-- Gauss-Newton method.
--
-- Returns _a_, the mean squared error of the resulting fit, and the number of iterations performed.
-- @see 03-nlfit.lua
function numeric.nlfit(f, a, X, y, n_iters, eps, u, v)
    -- TODO provide estimates of the parameter's standard error, and add support for weighting
    if a.isview then a = a:copy() end
    if y.isview then y = y:copy() end

    -- default parameters
    n_iters = n_iters or 1e2
    eps = eps or 1e-9
    u   = u   or 0.1
    v   = v   or 10
    
    local n_rows, n_params = X.m, a.m
    local J = matrix.new(n_rows, n_params)
    local E = matrix.new(n_rows, 1)
    local I = matrix.eye(n_params)
    
    local it = 0
    while it < n_iters do -- for every iteration
        it = it + 1
    
        -- compute the Jacobian matrix
        for r = 1, n_rows do
            local Xr  = X:row(r)
            for p = 1, n_params do
                local val = diff(f, a, Xr, p, eps)
                J:set(r, p, val)
            end
            
            E:set(r,1,y(r) - f(a, Xr))
        end
        
        -- compute the quasi-Hessian matrix
        local H     = J:t() * J;
        local G     = J:t() * E;
        local err_m = E:t() * E;
        local l_err = err_m:get(1,1) / n_rows
        
        -- update the parameters
        local alast = a:copy()
        local sol   = matrix.solve(H + I*u, G, false, true)
        a:add(sol, true)
        local n_err = fmse(f, a, X, y)
        
        -- adjust the damping factor accordingly
        if n_err < l_err then
            u = u / v
            -- check for convergence
            if math.abs(l_err - n_err) < eps then break end
        else
            u = u * v
            a = alast
        end
    end
    
    return a, fmse(f, a, X, y), it
end

---
-- Fits a polynomial of a certain _order_ to the data points _x_ and _y_, where both _x_
-- and _y_ are column vectors with the same number of rows. The fitting is performed by
-- building a Vandermonde matrix and performing a least squares fitting. The number of rows
-- of _x_ should be greater than _order_. An optional matrix _W_ can be used to inform the
-- covariance between the data (or the weights for a weighted fitting when only the diagonal elements
-- are different than zero).
--
-- Returns the fitted coefficients (highest order first), a variance matrix for the fitted coefficients,
-- the resulting squared error and the coefficient of determination
function numeric.polyfit(x, y, order, W)
    local A = matrix.new(y.m, order + 1)
    
    for i = 1, y.m do
        for j = 1, order+1 do
            local val = x:get(i, 1)^(order-j+1)
            A:set(i, j, val)
        end
    end
    
    if W then
        A = W*A
        y = W*y
    end
    
    local a = A:ls(y)
    if not a then return end
    
    local e      = A*a-y
    local se     = (e:t(true)*e)(1)
    local sigma2 = se / (A.m - (order+1))
    local var    = (A:t(true)*A):inv() * sigma2
    local r2     = 1 - e:sqnorm() / (y - stats.mean(y)):sqnorm()
    
    return a, var, se, r2
end

---
-- Evaluates a 1D polynomial with coefficients given by the column vector _p_
-- (given in decreasing order) at _x_ (_x_ can be a scalar or a column vector).
-- Uses Horner's method.
function numeric.polyval(p, x)
    if matrix.is(x) then
        local acc = matrix.zeros(x.m)
    
        for i = 1, p.m do
            acc:mul(x, true)
            acc:add(p:get(i,1), true)
        end
        return acc
    else
        local acc = 0
        
        for i = 1, p.m do
            acc = acc * x + p:get(i,1)
        end
        
        return acc
    end
end

return numeric
