module(..., package.seeall)

local matrix = require('lna.matrix')
local stats  = require('lna.stats')

function setup()
    tol = 1e-5
end

function test_mean()
    local v1 = matrix.fromtable{1,2,3,4,5,6,7,8,9}
    assert(5, stats.mean(v1) )
    
    local v2 = matrix.fromtable{ {1,2,3}, {4,8,6} }
    assert(4, stats.mean(v2) )
end

function test_meanm()
    local A = matrix.fromtable{ {1, 5}, {2,8}, {3,17} }
    local M = stats.meanm(A)
    assert_equal(2 , M(1), tol)
    assert_equal(10, M(2), tol)
    assert_equal(1 , M.m)
    assert_equal(2 , M.n)
end

function test_cumsum()
    local v1 = matrix.fromtable{1,1,1,1,1}
    local c  = stats.cumsum(v1)
    assert_true( matrix.fromtable{1,2,3,4,5}:equal(c, tol) )
end

function test_cov()
    local v1 = matrix.fromtable{1,2,3,4,5}
    local v2 = matrix.fromtable{-1,-2,-3,-4,-5}
    local v3 = v1*2
    
    assert_equal (2.5 , stats.cov(v1,v1), tol)
    assert_equal (2.5 , stats.cov(v2,v2), tol)
    assert_equal (-2.5, stats.cov(v1,v2), tol)
    assert_equal (5   , stats.cov(v1,v3), tol)
end

function test_var()
    local v = matrix.fromtable{1,2,3,4,5}
    assert_equal (2.5 , stats.var(v), tol)
    assert_equal (2   , stats.var(v,0), tol)
end

function test_std()
    local v1 = matrix.fromtable{4,2,3.5,2.5,3}
    local v2 = matrix.fromtable{3+2i, 2-2i, 4+2i,5-2i, 3-2i}
    
    assert_equal(0.79057, stats.std(v1), 1e-3 )
    assert_equal(2.4698 , stats.std(v2), 1e-3 )
end

function test_covm()
    local A = matrix.fromtable{ {1,2}, {2,3}, {-5,10}, {5,6} }
    local M = stats.covm(A)
    
    assert_true(M:equal(matrix.fromtable{
        {17.5833, -9.2500},
        {-9.2500, 12.9167}
    }, 1e-3))
end

function test_corrm()
    local A = matrix.fromtable{ {1,2}, {2,3}, {-5,10}, {5,6} }
    local M = stats.corrm(A)
    
    assert_true(M:equal(matrix.fromtable{
        {1, -0.61378},
        {-0.61378, 1}
    }, tol))
end

function test_zscore()
    local v = matrix.fromtable{5,5,5}
    assert_true(stats.zscore(v):equal(matrix.fromtable{0,0,0}, tol))
    
    local v = matrix.fromtable{2,2,3,3,3,4,4,4,4,5,5,5,5,5,4,4,4,4,3,3,3,2,2}
    local res = stats.zscore(v)
    assert_true(res:equal(matrix.fromtable{
        -1.55723,  -1.55723,  -0.58922,  -0.58922,  -0.58922,   0.37879,   0.37879,   0.37879,
        0.37879,   1.34679,   1.34679,   1.34679,   1.34679,   1.34679,   0.37879,   0.37879,
        0.37879,   0.37879,  -0.58922,  -0.58922,  -0.58922,  -1.55723,  -1.55723
    }, tol))
    assert_equal(1, stats.var(res), tol)
end

function test_zscorem()
    local m = matrix.fromtable{ {0,1,5}, {0,2,6}, {0,-1,5} }
    
    assert_true(stats.zscorem(m):equal(matrix.fromtable{
        {0.00000,   0.21822,  -0.57735},
        {0.00000,   0.87287,   1.15470},
        {0.00000,  -1.09109,  -0.57735}
    }, 1e-5))
end

function test_pca()
    local L = matrix.fromtable{ {2, 50}, {3, 10}, {2, 40}, {3, 80}, {2, 30}, {3, 100}, {2, 20} }
    local w, V, mu = stats.pca(L)
    assert_equal(V.m, 2)
    assert_equal(V.n, 2)
    assert_equal(2, w:len())
    assert_true( (V:get(2,1) / V:get(1,1) > 100 ) )
    
    local L2 = stats.project(L, V, mu);
    local L3 = stats.reconstruct(L2, V, mu)
    assert_true(L:equal(L3, tol))
end
