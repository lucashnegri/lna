module(..., package.seeall)

local matrix  = require('lna.matrix')
local numeric = require('lna.numeric')

function setup()
    tol = 1e-3
end

function test_fmin()
    math.randomseed( os.time() )
    
    local function f(x)
        return (1- x(1) )^2 + 100*( x(2) - x(1)^2)^2
    end

    local x, iters = numeric.fmin(f, { {-10,10}, {-10,10} } )
    assert_true(x:equal(matrix.fromtable{{1,1}}, tol))
end

function test_rk()
    local function ode(y)
        y[1], y[2], y[3] = 0, y[1], y[2]
    end

    local s1 = numeric.rk4(ode, 3, {0.2, 1, 10}) 
    local s2 = numeric.rk4(ode, 3, {0.2, 1, 10}) 
    local s3 = numeric.rk4(ode, 3, {0.2, 1, 10}) 
    
    local r1, r2, r3
    r1 = s1(1e1)
    for i = 1, 10   do r2 = s2(1e0)  end
    for i = 1, 1000 do r3 = s3(1e-2) end
    
    assert_equal(30, r1[3], tol)
    assert_equal(30, r2[3], tol)
    assert_equal(30, r3[3], tol)
end

function test_nlfit1()
    -- one independent variable
    local function poly(a, x)
        return a(1)*x(1)*x(1) + a(2)*x(1) + a(3)
    end
    
    for t = 1, 100 do
        local X = matrix.fromtable{1,2,3,4,5,6,7,8,9}
        local y = X:mul(X) + X*3 + 5
    
        local a, mse, it = numeric.nlfit(poly, matrix.fromtable{0.1, 0.1, 0.1}, X, y)
        assert_true ( a:equal(matrix.fromtable{1, 3, 5}, tol) )
        assert_equal( 0, mse, tol)
    end
end

function test_nlfit2()
    -- two independent variable
    local function mysum(a, x)
        return a(1)*x(1) + a(2)*x(2)
    end
    
    local X = matrix.fromtable{ {2,5}, {3,1}, {-2, 9}, {20, -3} }
    local y = -3*X:col(1) + 5*X:col(2)
    
    local a, mse, it = numeric.nlfit(mysum, matrix.fromtable{0.1, 0.1}, X, y)
    assert_true ( a:equal(matrix.fromtable{-3, 5}, tol) )
    assert_equal(0, mse, tol)
end

function test_nlfit3()
    -- sine function
    local function mysin(a, x)
        return math.sin(2*math.pi*a(1)*x(1) + a(2)) + a(3)
    end
    
    local X = matrix.linspace(0, 1, 30) 
    local y = (2*math.pi*5*X + 6):map(math.sin) - 4
    
    local a, mse, it = numeric.nlfit(mysin, matrix.fromtable{4.5, 3, 1}, X, y)
    assert_equal( 5, a(1), tol)
    assert_equal(-4, a(3), tol)
    assert_equal( 0, mse , tol)
end

function test_polyfit()
    local function poly(a, x)
        return a(1)*x(1)*x(1) + a(2)*x(1) + a(3)
    end
    
    for t = 1, 100 do
        local X      = matrix.fromtable{1,3,4,6,7,9}
        local y      = X:mul(X) + X*3 + 5
        local a, v, mse = numeric.polyfit(X, y, 2)
        assert_true ( a:equal(matrix.fromtable{1, 3, 5}, tol) )
        assert_true ( v:diag():equal(matrix.fromtable{0, 0, 0}, tol) )
        assert_equal( 0, mse, tol )
    end
    
    for o = 1, 10 do
        local X      = matrix.fromtable{1,2,3,4,5,6,7,8,9,10,11}
        local y      = X:mul(X) + X*3 + 5
        local a, s, mse = numeric.polyfit(X, y, o)
        
        if o < 2 then
            assert_true(mse > 5)
        else
            assert_equal(0, mse, tol)
        end
    end
end

function test_polyfit_weighted()
    local a, b = 2.5, -3
    
    local x = matrix.linspace(1,5, 5)
    local y = a*x + b
    local w = matrix.zeros(x.m, x.m)
    w:diag():fill(1)
    
    local c = numeric.polyfit(x, y, 1, w)
    assert_true( c:equal(matrix.fromtable{a, b}, tol) )

    y:set(y.m, 1, 12)
    local c = numeric.polyfit(x, y, 1, w)
    assert_false( c:equal(matrix.fromtable{a, b}, tol) )

    w:set(y.m, y.m, 0)
    local c = numeric.polyfit(x, y, 1, w)
    assert_true( c:equal(matrix.fromtable{a, b}, tol) )

    w:diag():fill(1e6)
    w:set(5,5, 1)
    local c = numeric.polyfit(x, y, 1, w)
    assert_true( c:equal(matrix.fromtable{a, b}, tol) )
end

function test_polyval()
    for t = 1, 100 do
        local x       = math.random(-100, 100)
        local a, b, c = math.random(-100,100), math.random(-100,100), math.random(-100,100)
        local p = matrix.fromtable{a,b,c}
        local y = a*x^2+b*x+c
        assert_equal(y, numeric.polyval(p, x), tol)
    end
    
    local x = matrix.fromtable{1, 2, 5}
    local p = matrix.fromtable{1, 2, -3}
    assert_true( numeric.polyval(p, x):equal(matrix.fromtable{0, 5, 32}, tol) )
end

function test_mse()
    local a = matrix.fromtable{1,2,3,4,5,6}
    local b = matrix.fromtable{4,3,2,1,2,3}
    assert_equal(9.5, numeric.mse(a, b, 4), tol)
end

function test_chisq()
    local x = matrix.fromtable{0.071, 0.156, 0.236, 0.300, 0.363, 0.448, 0.568, 0.701}
    local y = matrix.fromtable{120, 280, 340, 540, 700, 750, 970, 980}
    local s = matrix.fromtable{35, 53, 58, 73, 84, 87, 98, 99}
    
    local W = matrix.zeros(x.m, x.m)
    W:diag():fill(1/s)
    
    do
        local p   = numeric.polyfit(x, y, 1, W)
        local est = numeric.polyval(p, x)
        assert_equal(0.93, numeric.chisq(y, est, s, y.m - 2), 0.01)
    end
    
    do
        local p   = numeric.polyfit(x, y, 2, W)
        local est = numeric.polyval(p, x)
        assert_equal(0.73, numeric.chisq(y, est, s, y.m - 3), 0.01)
    end    
end
