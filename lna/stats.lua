--- Statistical routines implemented for matrices.
-- Implements basic statistics for matrices / vectors (var, cov, std), normalization (zscore),
-- and dimensionality reduction algorithms.
--
-- @module lna.stats
local matrix  = require('lna.matrix')
local complex = require('lna.complex')

local stats = {}

---
-- Returns the mean (real or complex number) of the entire matrix _A_.
function stats.mean(A)
    return A:sum() / A:len()
end

---
-- Returns a row vector where the value of each column _c_ is the mean of the respective column
-- of matrix _A_.
-- @usage stats.meanm(matrix.fromtable{ {1,2,3}, {3,2,5} ) -- equal to { {2,2,4} }
function stats.meanm(A)
    local B = matrix.new(1, A.n)
    
    for c = 1, A.n do
        B:set(1 ,c, stats.mean( A:col(c) ))
    end
    
    return B
end

---
-- Returns the cummulative sum of matrix _A_ as a flat array (row1 .. row2 .. rowN).
function stats.cumsum(A)
    local acc = 0
    local cum = matrix.new(A:len(), 1)
    local idx = 1
    
    A:foreach(function(A,i,j)
        acc = acc + A:get(i,j)
        cum:set(idx, 1, acc)
        idx = idx + 1
    end)
    
    return cum
end

---
-- Returns the covariance between matrices _A_ and _B_ as flat arrays, using
-- _ddof_ degrees of freedom (default 1).
function stats.cov(A, B, ddof)
    assert(A.m == B.m and A.n == B.n, "dimensions do not match")

    local ma, mb = stats.mean(A), stats.mean(B)
    local acc    = 0
    local cpx    = complex.is(mb)
    
    A:foreach(function(A,i,j)
        local a = A:get(i,j) - ma
        local b = B:get(i,j) - mb
        if cpx then b = b:conj() end
        acc = acc + a*b
    end)
    
    return acc / (A:len() - (ddof or 1))
end

---
-- Returns the variance of matrix _A_ as a flat array, using
-- _ddof_ degrees of freedom (default 1).
-- @see cov
function stats.var(A, ddof)
    local val = stats.cov(A, A, ddof)
    return type(val) == 'number' and val or val:real()
end

---
-- Returns the standard deviation of matrix _A_ as a flat array, using
-- _ddof_ degrees of freedom (default 1).
-- @see var
function stats.std(A, ddof)
    return math.sqrt( stats.var(A, ddof) )
end

---
-- Returns the pearson product-moment correlation coefficient of matrices _A_ and _B_ as flat arrays.
-- @see var
function stats.corr(A, B)
    local val = stats.std(A) * stats.std(B)
    if val == 0 then val = 1/0 end
    return stats.cov(A,B) / (val)
end

---
-- Returns a matrix where each cell (i,j) is the covariance between column i and j of matrix _A_, using
-- _ddof_ degrees of freedom.
-- @see cov
function stats.covm(A, ddof)
    return matrix.new(A.n, A.n):foreach(function(B,i,j)
        local val = stats.cov(A:col(i), A:col(j), ddof)
        B:set(i,j,val)
    end)
end

---
-- Returns a matrix where each cell (i,j) is the correlation between column i and j of matrix _A_.
-- @see corr
function stats.corrm(A)
    return matrix.new(A.n, A.n):foreach(function(B,i,j)
        local val = stats.corr(A:col(i), A:col(j))
        B:set(i,j,val)
    end)
end

---
-- Normalizes the values in matrix _A_ (zero mean and unitary variance). Returns a matrix with
-- the same shape as matrix _A_ where, for every index (i,j), A[i][j] = ( A[i][j] - mean(A) ) / std(A).
function stats.zscore(A)
    local std = stats.std(A)
    if math.abs(std) < 1e-12 then std = 1/0 end
    return (A - stats.mean(A)):div(std, true)
end

---
-- Returns a new matrix with the normalized values of matrix _A_ (zero mean and unitary variance),
-- handling each column as a different population.
-- @see zscore
function stats.zscorem(A)
    local B = matrix.new(A.m, A.n)
    
    for c = 1, A.n do
        B:col(c):fill( stats.zscore( A:col(c)) )
    end
    
    return B
end

local function eig_reorder(A)
    local v, V     = A:eig()
    local w, order = v:sort(false, function(a,b) return a > b end)
    V = V:arrange(order, true)
    
    return w, V
end

---
-- Computes the components of matrix _A_ (A.m data entries in A.n dimensions). It is the first step
-- for a principal component analysis. Returns a column matrix with the eigenvalues, a matrix with
-- the eigenvectors and a column with original mean of every column (the eigenvalues/vectors are
-- sorted in order of decreasing contribution to the variance of the data).
-- @see matrix.eig
-- @see project
-- @see 06-wine.lua
function stats.pca(A)
    local mu   = stats.meanm(A)
    A = A:copy():bsx(mu, matrix.sub, true)
    local w, V = eig_reorder( stats.covm(A) )
    return w, V, mu
end

---
-- Projects matrix _X_ (X.m entries, X.n dimensions) into the space given by the eigenvectors
-- _V_, using a mean of _mu_.
-- @see matrix.eig
-- @see pca
-- @see lda
function stats.project(X, V, mu)
    if mu then X = X:copy():bsx(mu, matrix.sub) end
    return X * V
end

---
-- Reconstructs matrix _X_ to its original dimensions by using eigenvectors _V_ and mean _mu_.
-- @see matrix.eig
-- @see pca
-- @see lda
function stats.reconstruct(X, V, mu)
    local out = X*V:t()
    if mu then out:bsx(mu, matrix.add) end
    return out
end

---
-- Vomputes the components of matrix _A_ (A.m data entries in A.n dimensions) to perform a
-- linear discriminant analysis. Returns a column matrix with the eigenvalues, a matrix with
-- the eigenvectors and a column with original mean of every column (the eigenvalues/vectors are
-- sorted in order of decreasing contribution to the variance of the data).
-- @see matrix.eig
-- @see project
-- @see 06-wine.lua
function stats.lda(X, y)
    local dims   = X.n
    local labels = y:unique()
    local C      = labels.m
    local Sw     = matrix.new(dims, dims):fill(0)
    local Sb     = matrix.new(dims, dims):fill(0)
    local mu     = stats.meanm(X)
    
    for i = 1, C do
        local Xi   = X:filter(function(X,p) return y:get(p) == labels:get(i) end)
        local mu_i = stats.meanm(Xi)
        Xi:bsx(mu_i, matrix.sub)
        Sw:add  (Xi:t() * Xi, true)
        mu_i:sub(mu, true)
        Sb:add((mu_i:t() * Xi.m) * mu_i, true)
    end
    
    local w, V = eig_reorder( matrix.solve(Sw, Sb, false, true) )
    return w, V, mu
end

return stats
