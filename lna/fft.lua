--- Fast Fourier transform.
-- Provides the Fast Fourier Transform, and related algorithms, for matrices.
--
-- @module lna.fft
local kiss   = require('kiss')
local matrix = require('lna.matrix')

local fft    = {}

---
-- Computes the one-dimensional fast Fourier transform of samples in _matrix_ _A_. _A_ is used as a
-- flat array (row1 .. row2 .. row3 .. rowN). Returns a matrix with the same shape as _A_ with the
-- frequency components.
-- @see 04-fourier.lua
function fft.fft(A)
    A = A:copy(true)
    local n = A:len()
    kiss.luakiss_fft(n, false, A.data, A.data)
    return A
end

---
-- Computes the one-dimensional inverse fast Fourier transform of the components in _matrix_ _A_. _A_ is used as a
-- flat array (row1 .. row2 .. row3 .. rowN). Returns a matrix with the same shape as _A_ with the
-- samples; _A_ must be a complex matrix.
function fft.ifft(A)
    assert( matrix.is(A) and A:iscomplex(), 'complex matrix expected' )
    A = A:copy()
    local n = A:len()
    kiss.luakiss_fft(n, true, A.data, A.data)
    return A / n
end

---
-- Returns the next fast size. A fast size is the lowest value higher than _n_ in which the
-- [i]fft computation will be as fast as possible.
function fft.nextfastsize(n)
    while true do
        local m = n
        while m % 2 == 0 do m = m / 2 end
        while m % 3 == 0 do m = m / 3 end
        while m % 5 == 0 do m = m / 5 end
        if m <= 1 then return n end
        n = n + 1
    end
end

---
-- Swaps the halves of _A_. _A_ is used as a flat array (row1 .. row2 .. rowN). Returns a matrix
-- with the same shape of _A_.
-- @see 04-fourier.lua
function fft.shift(A)
    if A.isview then A = A:copy() end
    local B   = matrix.new(A.m, A.n, A:iscomplex() )
    local pos = 0
    local h   = math.ceil(A:len() / 2)+1
    
    -- middle to the second half
    for i = h, A:len() do
        B.data[pos] = A(i)
        pos = pos + 1
    end
    
    -- first half
    for i = 1, h-1 do
        B.data[pos] = A(i)
        pos = pos + 1
    end
    
    return B
end

---
-- Removes discontinuites in _A_ greater than _tol_. _A_ should be a real matrix representing
-- phases in radians, and will be used as a flat array (row1 .. row2 .. rowN). By default, tol is
-- equal to pi. Returns a new matrix with the same shape as _A_.
-- @see 04-fourier.lua
function fft.unwrap(A, tol)
    assert( not A:iscomplex() )
    A         = A:copy()
    tol       = tol or math.pi
    local acc = 0
    local inc = matrix.new(A.m, A.n)
    
    for i = 2, A:len() do
        local dif = A(i) - A(i-1)
        if dif > tol then
            acc = acc - math.pi*2
        elseif dif < -tol then
            acc = acc + math.pi*2
        end
        
        inc.data[i-1] = acc
    end
    
    return A:add(inc, true)
end

return fft
