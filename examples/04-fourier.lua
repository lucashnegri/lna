--[[
    Fourier analysis by using the fast Fourier transform.
--]]

-- lna.lab imports all modules to the global table and provides a simple
-- plot(x,y) function, intended for quick tests
require('lna.lab')

local sin, w = matrix.sin, math.pi*2

-- sampled 2 seconds of a signal, with a 100Hz sampling rate
local t = matrix.linspace(0,2,200)

-- the signal has components in 15, 25 and 40Hz, plus random noise
local x = sin(w*15*t) + 3*sin(w*25*t) + 2*sin(w*40*t)

-- add some noise
x = x + 0.2*matrix.rand(t.m, t.n)

-- perform the transform
local X = fft.shift( fft.fft(x) )

-- plot
local f = matrix.linspace(-50, 49.5, 200)
plot(t, x)
plot(f, X:abs() )
plot(f, fft.unwrap(X:arg()) )
