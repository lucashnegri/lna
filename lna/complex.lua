--- Complex number implementation.
-- A pure Lua implementation of complex numbers that hooks into the complex type via LuaJIT FFI.
-- Can be used alone or with complex matrices.
--
-- Currently implemented in Lua, instead of bindings to the C99 complex number functions, due
-- to performance. Note that you must use the accessors real(), imag() or both() instead of indexing
-- a complex number manually, because it could be a Lua _table_ or a _cdata_ type (operations with
-- complex numbers returns 'tables' instead of 'cdata' for performance reasons).
--
-- Allows one to write 5+2i anywere in a LuaJIT program.
--
-- @module lna.complex

local ffi = require('ffi')

local complex = {}

---
-- Returns both real and imaginary components of _c_.
-- @usage r, i = (5+2i):both()
function complex.both(c)
    if type(c) == "number" then
        return c, 0
    elseif type(c) == "table" then
        return c[1], c[2]
    else
        return c[0], c[1]
    end
end
local both = complex.both

--- 
-- Returns a new complex number with real part _re_ (default 0) and imaginary part _im_ (default 0).
function complex.new(re, im)
    return setmetatable({re or 0, im or 0}, complex)
end
local new = complex.new
local i   = new(0,1)

---
-- Returns a new complex number with real part _re_ (default 0) and imaginary part _im_ (default 0).
-- Differs from _complex.new_ by forcing the return of a 'cdata'.
function complex.cnew(re, im)
    return ffi.new("complex", re or 0, im or 0)
end

---
-- Returns the complex argument (phase angle) of _c_.
function complex.arg(c)
    local r, i = both(c)
    return math.atan2(i, r)
end
local arg = complex.arg

---
-- Returns _c1_ + _c2_.
function complex.__add(c1, c2)
    local r1, i1 = both(c1)
    local r2, i2 = both(c2)
    return new(r1 + r2, i1 + i2)
end

---
-- Returns _c1_ - _c2_.    
function complex.__sub(c1, c2)
    local r1, i1 = both(c1)
    local r2, i2 = both(c2)
    return new(r1 - r2, i1 - i2)
end
    
---
-- Returns _c1_ \* _c2_.
function complex.__mul(c1,c2)
    local r1, i1 = both(c1)
    local r2, i2 = both(c2)
    return new(r1*r2 - i1*i2, r1*i2 + r2*i1)
end

---
-- Returns _c1_ / _c2_.
function complex.__div(c1,c2)
    local r1,i1 = both(c1)
    local r2,i2 = both(c2)
    local den   = r2*r2 + i2*i2
    return new( (r1*r2 + i1*i2) / den, (r2*i1 - r1*i2) / den)
end
    
---
-- Returns -c.
function complex.__unm(c)
    local r, i = both(c)
    return new(-r, -i)
end

---
-- Returns a string representation of the complex number _c_. Values lower than 1e-15 are
-- rounded to zero.
function complex.__tostring(c)
    local r, i = both(c)
    if(math.abs(r) < 1e-15) then r = 0 end
    if(math.abs(i) < 1e-15) then i = 0 end
    return r .. (i >= 0 and '+' or '') .. i .."i"
end

local type_complex = ffi.metatype(ffi.typeof("complex") , complex)

--- 
-- Returns true if _c_ is a complex number, false otherwise.
function complex.is(c)
    local type = type(c)
    
    if type == 'table' then
        return getmetatable(c) == complex
    elseif type == 'cdata' then
        return ffi.istype(c, type_complex)
    else
        return false
    end
end

--- 
-- Returns the complex conjugate of _c_ (r,-i).
function complex.conj(c)
    local r, i = both(c)
    return new(r, -i)
end

---
-- Returns the absolute value of _c_.
function complex.abs(c)
    local r, i = both(c)
    return math.sqrt(r*r + i*i)
end

--- 
-- Returns the complex exponential of _c_.
function complex.exp(c)
    local r, i = both(c)
    local er   = math.exp(r)
    return new(er*math.cos(i), er*math.sin(i))
end

--- 
-- Returns the natural logarithm of _c_.
function complex.log(c)
    return new(math.log(c:abs()), c:arg())
end

--- 
-- Returns the base-10 logarithm of _c_.
function complex.log10(c)
    return complex.log(c) / math.log(10)
end

---
-- Returns the sine of _c_.
function complex.sin(c)
    local r, i = both(c)
    return new(math.sin(r) * math.cosh(i), math.cos(r) * math.sinh(i))
end

---
-- Returns the inverse sine of _c_.
function complex.asin(c)
    return -i * (i*c + (1-c*c):sqrt()):log()
end

---
-- Returns the hyperbolic sine of _c_.
function complex.sinh(c)
    return (c*i):sin() / i
end

---
-- Returns the inverse hyperbolic sine of _c_.
function complex.asinh(c)
    return (c+(c*c+1):sqrt()):log()
end

---
-- Returns the cossine of _c_.
function complex.cos(c)
    local r, i = both(c)
    return new(math.cos(r) * math.cosh(i), -math.sin(r) * math.sinh(i))
end

---
-- Returns the inverse cossine of _c_.
function complex.acos(c)
    return math.pi/2 - c:asin()
end

---
-- Returns the hyperbolic cossine of _c_.
function complex.cosh(c)
    return (c*i):cos()
end

---
-- Returns the inverse hyperbolic cossine of _c_.
function complex.acosh(c)
    return (c+(c+1):sqrt()*(c-1):sqrt()):log()
end

---
-- Returns the tangent of _c_.
function complex.tan(c)
    return c:sin() / c:cos()
end

---
-- Returns the hyperbolic tangent of _c_.
function complex.tanh(c)
    return c:sinh() / c:cosh()
end

---
-- Returns the inverse tangent of _c_.
function complex.atan(c)
    local ic = i*c
    return (complex.log(1-ic) - complex.log(1+ic)) * (0.5*i)
end

---
-- Returns the inverse hyperbolic tangent of _c_.
function complex.atanh(c)
    return (complex.log(1+c) - complex.log(1-c)) * 0.5
end

---
-- Returns the _c1_ raised to the power of _c2_.
function complex.pow(c1, c2)
    return (c1:log() * c2):exp()
end

---
-- Returns the square root of _c_.
function complex.sqrt(c)
    local a = arg(c) * 0.5
    local m = math.sqrt(c:abs())
    return new( m*math.cos(a), m*math.sin(a) )
end

---
-- Returns the real component of _c_.
function complex.real(c)
    local r, i = both(c)
    return r
end

---
-- Returns the imaginary component of _c_.
function complex.imag(c)
    local r, i = both(c)
    return i
end

---
-- Returns true if each component of _c1_ is within a absolute difference of _tol_ (default 1e-12)
-- from the respective component of _c2_, false otherwise.
function complex.equal(c1, c2, tol)
    tol = tol or 1e-12
    local r1, i1 = both(c1)
    local r2, i2 = both(c2)
    return math.abs(r1-r2) <= tol and math.abs(i1-i2) <= tol
end

---
-- Returns true if the absolute value of _c1_ is smaller less than the absolute valor of _c2_.
function complex.__lt(c1,c2)
    local r1, i1 = both(c1)
    local r2, i2 = both(c2)
    
     -- no need for square root
    return (r1*r1+i1*i1) < (r2*r2+i2*i2)
end

--- Equal to complex.new(0,1), for compatibility.
complex.i         = i
complex.__index   = complex
complex.__pow     = complex.pow

return complex
