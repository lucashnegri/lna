--[[
    Numerical simulation of a mass attached to a spring, where
    the attractive force on the mass is equal to the compression
    of the spring. The simulation is performed with a 4º order
    Runge-Kutta method implemented in numeric.rk4.
--]]

local numeric = require('lna.numeric')
local gp      = require('gnuplot')

-- m d²x/dt² + c dx/dt + kx = 0
-- => d²x/dt² = -k/m x -c dx/dt
function ode(y, a)
    -- get the current values
    local x  = y[1] -- x
    local xp = y[2] -- dx/dt

    -- evaluate the differential equations
    y[2] = -a[1]/a[2]*x -a[3]*xp -- d²x/dt² = -k/m x - c dx/dt
    y[1] = xp                    -- dx /dt  = x'
end

-- initial values: x(0) = 5 [m], x(1) = 0 [m/s].
local initial = {5, 0}

-- using k = 5 [N/m], m = 10 [Kg], and c = 0.1 [Kg/s]
local sim1 = numeric.rk4(ode, 2, {5, 0}, {5, 10, 0.1})

-- using k = 1 [N/m], m = 1 [Kg], and c = 1 [Kg/s]
local sim2 = numeric.rk4(ode, 2, {5, 0}, {1, 1, 1})

-- simulate for 20s with steps of 0.1s, generating a plot
local step = 0.1
local out1  = {
    {}, -- time
    {}, -- position
}
local out2  = { {}, {} }

for t = 0, 20, step do
    -- sim1
    local y1 = sim1(step)
    table.insert(out1[1], t    )
    table.insert(out1[2], y1[1])
    
    -- sim2
    local y2 = sim2(step)
    table.insert(out2[1], t    )
    table.insert(out2[2], y2[1])
end

gp{
    grid   = 'back',
    xlabel = 'Time [s]',
    ylabel = 'Displacement [m]',
    width  = 600,
    height = 400,
    data = {
        gp.array{
            out1,
            title = 'm = 10 [Kg], k = 5 [N/m], c = 0.1 [Kg/s]',
            with  = 'lines',
            width = 2,
        },
        gp.array{
            out2,
            title = 'm =  1 [Kg], k = 1 [N/m],  c =   1 [Kg/s]',
            with  = 'lines',
            width = 2,
        }
    },
}:plot('ode.qt')
