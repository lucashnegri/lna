--[[
    Solving a linear system with matrix.solve.
 
     a + b +  c = 6
    3a - b + 2c = 7
    -a - b + 4c = 9
--]]

local matrix = require('lna.matrix')

local A = matrix.fromtable{
    { 1,  1, 1},
    { 3, -1, 2},
    {-1, -1, 4},
}
local b = matrix.fromtable{6,7,9} -- column vector

-- Ax = b
local x =  matrix.solve(A,b)
print(x)
