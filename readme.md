Lua - Numerical Algorithms
==========================

About
-----

Lua - Numerical Algorithms (lna) is a collection of Lua modules that provides:

* Bi-dimensional real/complex matrices;
* Fast linear algebra operations via lapacke bindings;
* Complex number implementation;
* Optimization algorithms (least squares fitting, function maximization);
* Other numerical algorithms (ODE solver, PCA / LDA, ...);

All modules are pure LuaJIT (requires FFI) modules, so redistribution is easy.
One can also use only the desired modules, without polluting the global table.

A note about performance: since all the basic operations are performed in Lua,
(and because LuaJIT is awesome), the performance is quite good (almost C level),
even for manually iterating over all elements of a matrix while doing some
computation.

License
-------

*lna* is licensed under the MIT license and can be freely used
for academic and commercial purposes. All required dependencies are
under a MIT or BSD-like license too.

This project uses Kiss FFT and any implementation of blas/lapack, and lapacke.

Dependencies
------------

*lna* depends on an atlas/lapack implementation with lapacke and cblas interfaces, such as [OpenBLAS][1]
[luakiss][2] (optional, only for FFT), [lua-gnuplot][3] (optional, only for plots),
and LuaJIT 2.

For testing, *lna* requires lunatest and lcomplex.

Install
-------

Numeric extra is a pure LuaJIT + FFI library. To install it, copy the *lna*
folder into your Lua shared path.

With some modifications, a good subset of this library may work with
Lua 5.1/5.2 by using luaffi. Keep in mind that the good performance comes from
LuaJIT.

Documentation
-------------

The documentation is located at doc/index.html (generated by [ldoc][7]).

Source code and Contact
-----------------------

Repository on [bitbucket][4].
Contact me at lucashnegri@gmail.com

Known issues
------------

luajit 2.0.3 crashes on exit when OpenBLAS is loaded. This issue shows only
when OpenBLAS is compiled with USE_OPENMP=1 and multiple threads are used. This
issue occurs independently of *lna*.

## Related Projects

[gsl-shell][5] provides a very nice environment for scientific coding in Lua.
[numlua][6] also provides matrices, linear algebra, complex number support and
FFT operations for Lua.

[1]: http://www.openblas.net
[2]: https://bitbucket.org/lucashnegri/lua-kiss
[3]: https://bitbucket.org/lucashnegri/lua-gnuplot
[4]: https://bitbucket.org/lucashnegri/lna
[5]: http://www.nongnu.org/gsl-shell
[6]: https://github.com/carvalho/numlua
[7]: http://stevedonovan.github.io/ldoc
