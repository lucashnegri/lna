-- required libraries
require('lunatest')

-- test suits
lunatest.suite('tests.matrix' )
lunatest.suite('tests.complex')
lunatest.suite('tests.fft'    )
lunatest.suite('tests.numeric')
lunatest.suite('tests.stats'  )

-- run
lunatest.run()
