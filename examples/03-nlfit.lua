--[[
    Using numeric.nlfit to fit the parameters of a
    model to a dataset.
--]]

-- require libraries
local matrix  = require('lna.matrix')
local numeric = require('lna.numeric')

-- model: f(x) = 3*x(1) + 5*log(12*x(2))
function model(a, x)
    return a(1)*x(1) + a(2)*math.log(a(3)*x(2))
end

 -- two dimensions, 5 data points
local X = matrix.fromtable{
    {-1, 10},
    {5 , 2},
    {-2, 3},
    {3 , 5},
    {4 , 8},
}

-- expected values (3, 5, 12)
local y = 3*X:col(1) + X:col(2):map(function(a)
    return 5*math.log(12*a)
end)

-- determine the coefficients
local initial_guess = matrix.fromtable{1, 1, 1}
local a, mse, it = numeric.nlfit(model, initial_guess, X, y)

-- print the output
print('Mean squared error  : ', mse)
print('Number of iterations: ', it )
print('Fitted parameters   :')
print(a)

